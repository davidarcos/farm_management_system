import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountingDataRecordComponent } from './components/accounting-data-record/accounting-data-record.component';
import { GeneralDataComponent } from './components/general-data/general-data.component';
import { HomeComponent } from './components/home/home.component';
import { MachineriesComponent } from './components/machineries/machineries.component';
import { PlotsComponent } from './components/plots/plots.component';
import { TreatmentsRecordComponent } from './components/treatments-record/treatments-record.component';

const routes: Routes = [
  {
    path: '',
    component: GeneralDataComponent
  },
  {
    path: 'generalData',
    component: GeneralDataComponent
  },
  {
    path: 'machineries',
    component: MachineriesComponent
  },
  {
    path: 'plots',
    component: PlotsComponent
  },
  {
    path: 'treatmentsRecord',
    component: TreatmentsRecordComponent
  },
  {
    path: 'accountingDataRecord',
    component: AccountingDataRecordComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
