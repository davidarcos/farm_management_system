import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { OwnerService } from 'src/app/services/owner.service';
import { MatDialog } from '@angular/material/dialog';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { Owner } from 'src/app/models/owner';
import { DialogOwnersComponent } from './dialogs/dialog-owners/dialog-owners.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { timer } from 'rxjs';
import { take } from 'rxjs/operators';
import { Relative } from 'src/app/models/relative';
import { RelativeService } from 'src/app/services/relative.service';
import { DialogRelativesComponent } from './dialogs/dialog-relatives/dialog-relatives.component';
import { OutsiderService } from 'src/app/services/outsider.service';
import { Outsider } from 'src/app/models/outsider';
import { DialogOutsidersComponent } from './dialogs/dialog-outsiders/dialog-outsiders.component';
import { AdviserService } from 'src/app/services/adviser.service';
import { Adviser } from 'src/app/models/adviser';
import { DialogAdviserComponent } from './dialogs/dialog-adviser/dialog-adviser.component';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-general-data',
  templateUrl: './general-data.component.html',
  styleUrls: ['./general-data.component.scss']
})
export class GeneralDataComponent implements OnInit {
  
  displayedColumnsOwners: string[] = ['registrationID','campaign', 'regepaCode', 'nif', 'fullName', 
    'province', 'city', 'zipCode', 'address', 'phoneNumber', 'mobileNumber', 'email', 'options'];
  displayedColumnsRelatives: string[] = ['orderNumber', 'nif', 'fullName', 'carnetType', 
    'ropoNumber', 'relationship', 'options'];
  displayedColumnsOutsiders: string[] = ['orderNumber', 'nif', 'fullName', 'carnetType', 'ropoNumber', 'options'];
  displayedColumnsAdvisers: string[] = ['orderNumber', 'nif', 'fullName', 'ropoNumber', 'fungusInstAdvice', 'options'];

  dataSourceOwners = new MatTableDataSource<Owner>();
  dataSourceRelatives = new MatTableDataSource<Relative>();
  dataSourceOutsiders = new MatTableDataSource<Outsider>();
  dataSourceAdvisers = new MatTableDataSource<Adviser>();

  noLoading = false;

  public formFilter!: FormGroup;
  showFilter = false;

  @ViewChild(MatPaginator, {static: true}) paginator!: MatPaginator;
  constructor(
    private ownerService: OwnerService,
    private adviserService: AdviserService,
    private relativeService: RelativeService,
    private outsiderService: OutsiderService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private formBuilder: FormBuilder
    ) {}

  ngOnInit(){
    this.refreshItems('owners',1);
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    switch(tabChangeEvent.index) {
      case 0:
        this.refreshItems('owners',0);
        break;      
      case 1:
        this.refreshItems('advisers',0);
        break;
      case 2:
        this.refreshItems('relatives',0);
        break;
      case 3:
        this.refreshItems('outsiders',0);
        break;
    }
  }

  openDialog(option: string, param: any) {
    switch(option) {
      case 'new-owner':
        const dialogRef_newOwner = this.dialog.open(DialogOwnersComponent,
          {data: {owner: null, optionDialog: option}});
        dialogRef_newOwner.disableClose = true;
        dialogRef_newOwner.afterClosed().subscribe(result => {
          this.refreshItems('owners',1500);
        });
        break;
      case 'edit-owner':
        const updatedOwner: Owner = param;
        const dialogRef_editOwner = this.dialog.open(DialogOwnersComponent, 
          {data: {owner: updatedOwner, optionDialog: option}});
        dialogRef_editOwner.afterClosed().subscribe(result => {
          this.refreshItems('owners',1500);
        });
        break;
      case 'delete-owner':
        const deletedOwner: Owner = param;
        const dialogRef_deleteOwner = this.dialog.open(DialogOwnersComponent, 
          {data: {owner: deletedOwner, optionDialog: option}});
        dialogRef_deleteOwner.afterClosed().subscribe(result => {
          this.refreshItems('owners',1500);
        });
        break;
      case 'edit-manager':
        const managedOwner: Owner = param;
        const dialogRef_editManager = this.dialog.open(DialogOwnersComponent, 
          {data: {owner: managedOwner, optionDialog: option}});
        dialogRef_editManager.afterClosed().subscribe(result => {
          this.refreshItems('owners',1500);
        });
        break;
      case 'no-manager':
        const addManager: Owner = param;
        const dialogRef_newManager = this.dialog.open(DialogOwnersComponent, 
          {data: {owner: addManager, optionDialog: option}});
        dialogRef_newManager.afterClosed().subscribe(result => {
          this.refreshItems('owners',1500);
        });
        break;
      case 'new-adviser':
        const dialogRef_newAdviser = this.dialog.open(DialogAdviserComponent,
          {data: {owner: null, optionDialog: option}});
        dialogRef_newAdviser.disableClose = true;
        dialogRef_newAdviser.afterClosed().subscribe(result => {
          this.refreshItems('advisers',1500);
        });
        break;
      case 'edit-adviser':
        const updatedAdviser: Adviser = param;
        const dialogRef_editAdviser = this.dialog.open(DialogAdviserComponent, 
          {data: {adviser: updatedAdviser, optionDialog: option}});
        dialogRef_editAdviser.afterClosed().subscribe(result => {
          this.refreshItems('advisers',1500);
        });
        break;
      case 'delete-adviser':
        const deletedAdviser: Adviser = param;
        const dialogRef_deleteAdviser = this.dialog.open(DialogAdviserComponent, 
          {data: {adviser: deletedAdviser, optionDialog: option}});
        dialogRef_deleteAdviser.afterClosed().subscribe(result => {
          this.refreshItems('advisers',1500);
        });
        break;
      case 'show-advisoryCrops':
        const adviser: Adviser = param;
        const dialogRef_showAdvisoryCrops = this.dialog.open(DialogAdviserComponent, 
          {data: {adviser: adviser, optionDialog: option}});
        dialogRef_showAdvisoryCrops.afterClosed().subscribe(result => {
          this.refreshItems('advisers',1500);
        });
        break;
      case 'new-relative':
        const dialogRef_newRelative = this.dialog.open(DialogRelativesComponent,
          {data: {relative: null, optionDialog: option}});
        dialogRef_newRelative.afterClosed().subscribe(result => {
          this.refreshItems('relatives',1500);
        });
        break;
      case 'edit-relative':
        const updatedRelative: Relative = param;
        const dialogRef_editRelative = this.dialog.open(DialogRelativesComponent,
          {data: {relative: updatedRelative, optionDialog: option}});
        dialogRef_editRelative.afterClosed().subscribe(result => {
          this.refreshItems('relatives',1500);
        });
        break;
      case 'delete-relative':
        const deletedRelative: Relative = param;
        const dialogRef_deleteRelative = this.dialog.open(DialogRelativesComponent,
          {data: {relative: deletedRelative, optionDialog: option}});
        dialogRef_deleteRelative.afterClosed().subscribe(result => {
          this.refreshItems('relatives',1500);
        });
        break;
      case 'new-outsider':
        const dialogRef_newOutsider = this.dialog.open(DialogOutsidersComponent,
          {data: {relative: null, optionDialog: option}});
        dialogRef_newOutsider.afterClosed().subscribe(result => {
          this.refreshItems('outsiders',1500);
        });
        break;
      case 'edit-outsider':
        const updatedOutsider: Outsider = param;
        const dialogRef_editOutsider = this.dialog.open(DialogOutsidersComponent,
          {data: {outsider: updatedOutsider, optionDialog: option}});
        dialogRef_editOutsider.afterClosed().subscribe(result => {
          this.refreshItems('outsiders',1500);
        });
        break;
      case 'delete-outsider':
        const deletedOutsider: Outsider = param;
        const dialogRef_deleteOutsider = this.dialog.open(DialogOutsidersComponent,
          {data: {outsider: deletedOutsider, optionDialog: option}});
        dialogRef_deleteOutsider.afterClosed().subscribe(result => {
          this.refreshItems('outsiders',1500);
        });
        break;
      default:
        this.snackBar.open('Error en la operación.', 'OK', 
        { duration: 4000, horizontalPosition: 'end' } );
        break;
    }
  }

  async refreshItems(table: string, time: number, showMsg: boolean = false) {
    await timer(time).pipe(take(1)).toPromise();
    switch(table) {
      case 'owners':
        this.noLoading = false;
        this.findAllOwners();
        break;
      case 'advisers':
        this.noLoading = false;
        this.findAllAdvisers();
        break;
      case 'relatives':
        this.noLoading = false;
        this.findAllRelatives();
        break;
      case 'outsiders':
        this.noLoading = false;
        this.findAllOutsiders();
        break;
      case 'filter':
        this.noLoading = false;
        this.search();
        break;
      default: 
        break;
    }

    if(showMsg && this.noLoading == false) {
      this.snackBar.open('¡Datos actualizados correctamente!', 'OK', 
      { duration: 2000, horizontalPosition: 'end' } );
    }

  }

  async findAllOwners(){
    try {
      this.dataSourceOwners.data = await this.ownerService.findAll().toPromise();
      this.dataSourceOwners.paginator = this.paginator;
    } catch {
      this.noLoading = true;
    }
  }

  async findAllAdvisers(){
    try {
      this.dataSourceAdvisers.data = await this.adviserService.findAll().toPromise();
      this.dataSourceAdvisers.paginator = this.paginator;
    } catch {
      this.noLoading = true;
    }
  }

  async findAllRelatives(){
    try {
      this.dataSourceRelatives.data = await this.relativeService.findAll().toPromise();
      this.dataSourceRelatives.paginator = this.paginator;
    } catch {
      this.noLoading = true;
    }
  }

  async findAllOutsiders(){
    try {
      this.dataSourceOutsiders.data = await this.outsiderService.findAll().toPromise();
      this.dataSourceOutsiders.paginator = this.paginator;    
    } catch {
      this.noLoading = true;
    }
  }

  showFilterOnOff(){
    this.showFilter = !this.showFilter;
    this.formFilter = this.formBuilder.group({
      registrationIDFilter: new FormControl(''),
      campaignFilter: new FormControl(''),
      regepaNumberFilter: new FormControl(''),
      nifFilter: new FormControl('')
    });
  }

  async search() {
    const registrationID = this.formFilter.value.registrationIDFilter;
    const campaign = this.formFilter.value.campaignFilter;
    const regepaNumber = this.formFilter.value.regepaNumberFilter;
    const nif = this.formFilter.value.nifFilter;

    try {
      if(registrationID != '') {
        this.dataSourceOwners.data = await this.ownerService.findByRegistrationID(registrationID).toPromise();
        if(this.dataSourceOwners.data.length > 0) return;
      }
      if(campaign != '') {
        this.dataSourceOwners.data = await this.ownerService.findByCampaign(campaign).toPromise();
        if(this.dataSourceOwners.data.length > 0) return;
      }
      if(regepaNumber != '') {
        this.dataSourceOwners.data = await this.ownerService.findByRegepaCode(regepaNumber).toPromise();
        if(this.dataSourceOwners.data.length > 0) return;
      }
      if(nif != '') {
        this.dataSourceOwners.data = await this.ownerService.findByNif(nif).toPromise();
        if(this.dataSourceOwners.data.length > 0) return;
      }
      this.refreshItems('owners',0);
      this.snackBar.open('¡No se han encontrado datos con estos parámetros de búsqueda!', 'OK', 
      { duration: 5000, horizontalPosition: 'end' } );
    } catch {
      this.noLoading = true;
    }
  }

}