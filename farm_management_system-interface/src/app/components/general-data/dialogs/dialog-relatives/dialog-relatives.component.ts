import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Relative } from 'src/app/models/relative';
import { RelativeService } from 'src/app/services/relative.service';

@Component({
  selector: 'app-dialog-relatives',
  templateUrl: './dialog-relatives.component.html',
  styleUrls: ['./dialog-relatives.component.scss']
})
export class DialogRelativesComponent implements OnInit {

  addRelative = false;
  editRelative = false;
  deleteRelative = false;

  public formNewRelative!: FormGroup;
  public formEditRelative!: FormGroup;
  public formDeleteRelative!: FormGroup;

  formNewRelativeError = false;
  formEditRelativeError = false;

  constructor(
    private relativeService: RelativeService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: {relative: Relative, optionDialog: string}
  ) { }

  ngOnInit(): void {
    this.initForms(this.data.optionDialog);
  }

  async initForms(option: string) {
    switch(option) {
      case 'new-relative':
        this.addRelative = true;
        this.formNewRelative = this.formBuilder.group({
          orderNumber: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(5)]),
          name: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(20)]),
          lastName: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(40)]),
          nif: new FormControl('', [Validators.required, Validators.minLength(9), Validators.maxLength(9), Validators.pattern(/^[0-9]{8}[ABCDEFGHIJKLMNOPQRSTUVWXYZ]$/i)]),
          carnetType: new FormControl('', [Validators.required]),
          ropoNumber: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(15)]),
          relationship: new FormControl('', [Validators.required])
        });
        break;
      case 'edit-relative':
        this.editRelative = true;
        const updatedRelative = this.data.relative;
        this.formEditRelative = this.formBuilder.group({
          orderNumber: new FormControl(updatedRelative.orderNumber, [Validators.required, Validators.minLength(1), Validators.maxLength(5)]),
          name: new FormControl(updatedRelative.name, [Validators.required, Validators.minLength(2), Validators.maxLength(20)]),
          lastName: new FormControl(updatedRelative.lastName, [Validators.required, Validators.minLength(2), Validators.maxLength(40)]),
          nif: new FormControl(updatedRelative.nif, [Validators.required, Validators.minLength(9), Validators.maxLength(9), Validators.pattern(/^[0-9]{8}[ABCDEFGHIJKLMNOPQRSTUVWXYZ]$/i)]),
          carnetType: new FormControl(updatedRelative.carnetType, [Validators.required]),
          ropoNumber: new FormControl(updatedRelative.ropoNumber, [Validators.required, Validators.minLength(1), Validators.maxLength(15)]),
          relationship: new FormControl(updatedRelative.relationship, [Validators.required])
        });
        break;
      case 'delete-relative':
        this.deleteRelative = true;
        const deletedRelative = this.data.relative;
        this.formDeleteRelative = this.formBuilder.group({
          orderNumber: new FormControl(deletedRelative.orderNumber),
          name: new FormControl(deletedRelative.name),
          lastName: new FormControl(deletedRelative.lastName),
          nif: new FormControl(deletedRelative.nif),
          carnetType: new FormControl(deletedRelative.carnetType),
          ropoNumber: new FormControl(deletedRelative.ropoNumber),
          relationship: new FormControl(deletedRelative.relationship)
        });
        break;
      default:
        break;
    }
  }

  async saveRelativeDB() {
    this.formNewRelative.markAllAsTouched();
    try {
      if(this.formNewRelative.invalid) {
        this.formNewRelativeError = true;
      } else {
        const relative: Relative = this.formNewRelative.value;
        await this.relativeService.insert(relative).toPromise();
        this.snackBar.open('AÑADIDO: Persona de la explotación o relacionada con el titular mediante grado de parentesco hasta primer grado que dispone de carné de aplicador de productos fitosanitarios.', 'OK', 
        { duration: 4000, horizontalPosition: 'end' } );
        this.dialog.closeAll();
      }
    } catch {
      this.snackBar.open('ERROR al añadir.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
    }

  }
  
  async updateRelativeDB() {
    this.formEditRelative.markAllAsTouched();
    try {
      if (this.formEditRelative.invalid) {
        this.formEditRelativeError = true;
      } else {
        let relative: Relative = this.formEditRelative.value;
        await this.relativeService.update(relative.nif, relative).toPromise();
        this.snackBar.open('ACTUALIZADO: Persona de la explotación o relacionada con el titular mediante grado de parentesco hasta primer grado que dispone de carné de aplicador de productos fitosanitarios.', 'OK', 
          { duration: 4000, horizontalPosition: 'end' } );
      }
    } catch {
      this.snackBar.open('ERROR al actualizar.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
    } finally {
      this.dialog.closeAll();
    }
    
  }

  async deleteRelativeDB() {
    try {
      await this.relativeService.delete(this.data.relative.nif).toPromise();
      this.snackBar.open('ELIMINADO: Persona de la explotación o relacionada con el titular mediante grado de parentesco hasta primer grado que dispone de carné de aplicador de productos fitosanitarios.', 'OK', 
      { duration: 4000, horizontalPosition: 'end' } );
    } catch {
      this.snackBar.open('ERROR al eliminar.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
    } finally {
      this.dialog.closeAll(); 
    }
  }

}
