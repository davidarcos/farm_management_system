import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Manager } from 'src/app/models/manager';
import { Owner } from 'src/app/models/owner';
import { ManagerService } from 'src/app/services/manager.service';
import { OwnerService } from 'src/app/services/owner.service';

@Component({
  selector: 'app-dialog-owners',
  templateUrl: './dialog-owners.component.html',
  styleUrls: ['./dialog-owners.component.scss']
})
export class DialogOwnersComponent implements OnInit {

  auxOwner: any;
  auxManager: any;

  addOwner = false;
  editOwner = false;
  deleteOwner = false;
  addManager = false;
  editManager = false;
  noManager = false;

  public formNewOwner!: FormGroup;
  public formEditOwner!: FormGroup;
  public formDeleteOwner!: FormGroup;
  public formNewManager!: FormGroup;
  public formEditManager!: FormGroup;
  public formNoManager!: FormGroup; 

  formNewOwnerError = false;
  formEditOwnerError = false;
  formNewManagerError = false;
  formEditManagerError = false;
  formNoManagerError = false;

  constructor(
    private ownerService: OwnerService,
    private managerService: ManagerService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    @ Inject(MAT_DIALOG_DATA) public data: {owner: Owner, optionDialog: string},
    private formBuilder: FormBuilder
    ) {}

  ngOnInit(): void {
    this.initForms(this.data.optionDialog);
  }

  async initForms(option: string) {
    switch(option) {
      case 'new-owner':
        this.addOwner = true;
        this.formNewOwner = this.formBuilder.group({
          registrationID: new FormControl('', [Validators.required]),
          campaign: new FormControl('', [Validators.required]),
          regepaCode: new FormControl('', [Validators.required]),
          name: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(20)]),
          lastName: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(40)]),
          nif: new FormControl('', [Validators.required, Validators.minLength(9), Validators.maxLength(9), Validators.pattern(/^[0-9]{8}[ABCDEFGHIJKLMNOPQRSTUVWXYZ]$/i)]),
          province: new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(20)]),
          city: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(20)]),
          zipCode: new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(5), Validators.pattern(/^[0-9]{5}/)]),
          address: new FormControl('', [Validators.required, Validators.minLength(7)]),
          phoneNumber: new FormControl('', [Validators.required, Validators.minLength(9), Validators.maxLength(9), Validators.pattern(/^[9]{1}[0-9]{8}/)]),
          mobileNumber: new FormControl('', [Validators.required, Validators.minLength(9), Validators.maxLength(9), Validators.pattern(/^[6-7]{1}[0-9]{8}/)]),
          email: new FormControl('', [Validators.required, Validators.email]),
          manager: new FormControl(true, [])
        });
        break;
      case 'edit-owner':
        this.editOwner = true;
        const updatedowner = this.data.owner;
        this.formEditOwner = this.formBuilder.group({
          registrationID: new FormControl(updatedowner.registrationID, [Validators.required]),
          campaign: new FormControl(updatedowner.campaign, [Validators.required]),
          regepaCode: new FormControl(updatedowner.regepaCode, [Validators.required]),
          name: new FormControl(updatedowner.name, [Validators.required, Validators.minLength(2), Validators.maxLength(20)]),
          lastName: new FormControl(updatedowner.lastName, [Validators.required, Validators.minLength(2), Validators.maxLength(40)]),
          nif: new FormControl(updatedowner.nif, [Validators.required, Validators.minLength(9), Validators.maxLength(9), Validators.pattern(/^[0-9]{8}[ABCDEFGHIJKLMNOPQRSTUVWXYZ]$/i)]),
          province: new FormControl(updatedowner.province, [Validators.required, Validators.minLength(4), Validators.maxLength(20)]),
          city: new FormControl(updatedowner.city, [Validators.required, Validators.minLength(2), Validators.maxLength(20)]),
          zipCode: new FormControl(updatedowner.zipCode, [Validators.required, Validators.minLength(5), Validators.maxLength(5), Validators.pattern(/^[0-9]{5}/)]),
          address: new FormControl(updatedowner.address, [Validators.required, Validators.minLength(7)]),
          phoneNumber: new FormControl(updatedowner.phoneNumber, [Validators.required, Validators.minLength(9), Validators.maxLength(9), Validators.pattern(/^[9]{1}[0-9]{8}/)]),
          mobileNumber: new FormControl(updatedowner.mobileNumber, [Validators.required, Validators.minLength(9), Validators.maxLength(9), Validators.pattern(/^[6-7]{1}[0-9]{8}/)]),
          email: new FormControl(updatedowner.email, [Validators.required, Validators.email])
        });
        break;
      case 'delete-owner':
        this.deleteOwner = true;
        const deletedOwner = this.data.owner;
        this.formDeleteOwner = this.formBuilder.group({
          registrationID: new FormControl(deletedOwner.registrationID),
          campaign: new FormControl(deletedOwner.campaign),
          regepaCode: new FormControl(deletedOwner.regepaCode),
          name: new FormControl(deletedOwner.name),
          lastName: new FormControl(deletedOwner.lastName),
          nif: new FormControl(deletedOwner.nif)
        });
        break;
      case 'new-manager':
        this.addManager = true;
        const owner: Owner = this.auxOwner;
        this.formNewManager = this.formBuilder.group({
          registrationID: new FormControl(owner.registrationID, [Validators.required]),
          campaign: new FormControl(owner.campaign, [Validators.required]),
          regepaCode: new FormControl(owner.regepaCode, [Validators.required]),
          name: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(20)]),
          lastName: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(40)]),
          nif: new FormControl('', [Validators.required, Validators.minLength(9), Validators.maxLength(9), Validators.pattern(/^[0-9]{8}[ABCDEFGHIJKLMNOPQRSTUVWXYZ]$/i)]),
          province: new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(20)]),
          city: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(20)]),
          zipCode: new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(5), Validators.pattern(/^[0-9]{5}/)]),
          address: new FormControl('', [Validators.required, Validators.minLength(7)]),
          phoneNumber: new FormControl('', [Validators.required, Validators.minLength(9), Validators.maxLength(9), Validators.pattern(/^[9]{1}[0-9]{8}/)]),
          mobileNumber: new FormControl('', [Validators.required, Validators.minLength(9), Validators.maxLength(9), Validators.pattern(/^[6-7]{1}[0-9]{8}/)]),
          email: new FormControl('', [Validators.required, Validators.email]),
        });
        break;
      case 'edit-manager':
        this.editManager = true;
        this.auxOwner = this.data.owner;
        await this.getManagerById();
        this.formEditManager = this.formBuilder.group({
          registrationID: new FormControl(this.auxManager.registrationID, [Validators.required]),
          campaign: new FormControl(this.auxManager.campaign, [Validators.required]),
          regepaCode: new FormControl(this.auxManager.regepaCode, [Validators.required]),
          name: new FormControl(this.auxManager.name, [Validators.required, Validators.minLength(2), Validators.maxLength(20)]),
          lastName: new FormControl(this.auxManager.lastName, [Validators.required, Validators.minLength(2), Validators.maxLength(40)]),
          nif: new FormControl(this.auxManager.nif, [Validators.required, Validators.minLength(9), Validators.maxLength(9), Validators.pattern(/^[0-9]{8}[ABCDEFGHIJKLMNOPQRSTUVWXYZ]$/i)]),
          province: new FormControl(this.auxManager.province, [Validators.required, Validators.minLength(4), Validators.maxLength(20)]),
          city: new FormControl(this.auxManager.city, [Validators.required, Validators.minLength(2), Validators.maxLength(20)]),
          zipCode: new FormControl(this.auxManager.zipCode, [Validators.required, Validators.minLength(5), Validators.maxLength(5), Validators.pattern(/^[0-9]{5}/)]),
          address: new FormControl(this.auxManager.address, [Validators.required, Validators.minLength(7)]),
          phoneNumber: new FormControl(this.auxManager.phoneNumber, [Validators.required, Validators.minLength(9), Validators.maxLength(9), Validators.pattern(/^[9]{1}[0-9]{8}/)]),
          mobileNumber: new FormControl(this.auxManager.mobileNumber, [Validators.required, Validators.minLength(9), Validators.maxLength(9), Validators.pattern(/^[6-7]{1}[0-9]{8}/)]),
          email: new FormControl(this.auxManager.email, [Validators.required, Validators.email])
        });           
        break;
      case 'no-manager':
        this.noManager = true;
        this.auxOwner = this.data.owner;
        this.formNoManager = this.formBuilder.group({
          registrationID: new FormControl(this.auxOwner.registrationID, [Validators.required]),
          campaign: new FormControl(this.auxOwner.campaign, [Validators.required]),
          regepaCode: new FormControl(this.auxOwner.regepaCode, [Validators.required]),
          name: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(20)]),
          lastName: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(40)]),
          nif: new FormControl('', [Validators.required, Validators.minLength(9), Validators.maxLength(9), Validators.pattern(/^[0-9]{8}[ABCDEFGHIJKLMNOPQRSTUVWXYZ]$/i)]),
          province: new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(20)]),
          city: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(20)]),
          zipCode: new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(5), Validators.pattern(/^[0-9]{5}/)]),
          address: new FormControl('', [Validators.required, Validators.minLength(7)]),
          phoneNumber: new FormControl('', [Validators.required, Validators.minLength(9), Validators.maxLength(9), Validators.pattern(/^[9]{1}[0-9]{8}/)]),
          mobileNumber: new FormControl('', [Validators.required, Validators.minLength(9), Validators.maxLength(9), Validators.pattern(/^[6-7]{1}[0-9]{8}/)]),
          email: new FormControl('', [Validators.required, Validators.email]),
        });        
        break;     
      default:
        break;
    }
  }

  async saveOwnerDB() {
    this.formNewOwner.markAllAsTouched();
    if (this.formNewOwner.invalid) {
      this.formNewOwnerError = true;
    } else {
      try {
        const owner: Owner = this.formNewOwner.value;
        await this.ownerService.insert(owner).toPromise();
        if (owner.manager) {
          this.auxOwner = owner;
          this.addOwner = false;
          this.initForms('new-manager');
        } else {
          this.snackBar.open('AÑADIDO: Registro de datos generales de la explotación.', 'OK', 
          { duration: 4000, horizontalPosition: 'end' } );
          this.dialog.closeAll();
        }
      } catch {
        this.snackBar.open('ERROR al añadir.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
      }
    }
  }

  async updateOwnerDB() {
    this.formEditOwner.markAllAsTouched();
    if (this.formEditOwner.invalid) {
      this.formEditOwnerError = true;
    } else {
      try {
        let owner: Owner = this.formEditOwner.value;
        await this.ownerService.update(owner.registrationID, owner).toPromise();
        this.snackBar.open('ACTUALIZADO: Registro de datos generales de la explotación.', 'OK', 
          { duration: 4000, horizontalPosition: 'end' } );
      } catch {
        this.snackBar.open('ERROR al actualizar.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
      } finally {
        this.dialog.closeAll();
      }
    }
  }

  async deleteOwnerDB() {
    try {
      if (this.data.owner.manager == true) {
        await this.managerService.delete(this.data.owner.registrationID).toPromise();
      }
      await this.ownerService.delete(this.data.owner.registrationID).toPromise();
      this.snackBar.open('ELIMINADO: Registro de datos generales de la explotación.', 'OK', 
        { duration: 4000, horizontalPosition: 'end' } );
    } catch {
      this.snackBar.open('ERROR al eliminar.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
    } finally {
      this.dialog.closeAll();
    }
  }

  async saveManagerDB() {
    this.formNewManager.markAllAsTouched();
    try {
      if (this.formNewManager.invalid) {
        this.formNewManagerError = true;
      } else {
        const manager: Manager = this.formNewManager.value;
        await this.managerService.insert(manager).toPromise();
        this.snackBar.open('AÑADIDO: Registro de representante.', 'OK', 
          { duration: 4000, horizontalPosition: 'end' } );
      }
    } catch {
      this.snackBar.open('ERROR al añadir.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
    } finally {
      this.dialog.closeAll();
    }
  }

  async saveNoManagerDB() {
    this.formNoManager.markAllAsTouched();
    try {
      if (this.formNoManager.invalid) {
        this.formNoManagerError = true;
      } else {
        const manager: Manager = this.formNoManager.value;
        await this.managerService.insert(manager).toPromise();
        const updatedOwner: Owner = this.auxOwner;
        updatedOwner.manager = true;
        await this.ownerService.update(updatedOwner.registrationID, updatedOwner).toPromise();
        this.snackBar.open('ACTUALIZADO: Registro de datos generales de la explotación. AÑADIDO: Registro de representante.', 'OK', 
        { duration: 4000, horizontalPosition: 'end' } );
      }
    } catch {
      this.snackBar.open('ERROR al añadir.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
    } finally {
      this.dialog.closeAll();
    }

  }
  
  async notSaveManagerDB() {
    try {
      const owner: Owner = this.auxOwner;
      owner.manager = false;
      await this.ownerService.update(owner.registrationID, owner).toPromise();
      if(this.data.optionDialog == 'new-manager') {
        this.snackBar.open('AÑADIDO: Registro de datos generales de la explotación.', 'OK', 
        { duration: 4000, horizontalPosition: 'end' } );
      }
    } catch {
      this.snackBar.open('ERROR al añadir.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
    } finally {
      this.dialog.closeAll();
    }
  }

  async getManagerById() {
    this.auxManager = await this.managerService.findById(this.auxOwner.registrationID).toPromise();
  }

  async updateManagerDB() {
    this.formEditManager.markAllAsTouched();
    try {
      if (this.formEditManager.invalid) {
        this.formEditManagerError = true;
      } else {
        let manager: Manager = this.formEditManager.value;
        await this.managerService.update(manager.registrationID, manager).toPromise();
        this.snackBar.open('ACTUALIZADO: Registro de representante.', 'OK', 
          { duration: 4000, horizontalPosition: 'end' } );
      }
    } catch {
      this.snackBar.open('ERROR al actualizar.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
    } finally {
      this.dialog.closeAll();
    }
  }

  async deleteManagerDB() {
    try {
      this.managerService.delete(this.auxManager.registrationID).toPromise();
      const owner: Owner = this.auxOwner;
      owner.manager = false;
      await this.ownerService.update(owner.registrationID, owner).toPromise();
      this.snackBar.open('ACTUALIZADO: Registro de datos generales de la explotación. ELIMINADO: Registro de representante.', 'OK', 
      { duration: 2000, horizontalPosition: 'end' } );
    } catch {
      this.snackBar.open('ERROR al eliminar.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
    } finally {
      this.dialog.closeAll();
    }
  }

}
