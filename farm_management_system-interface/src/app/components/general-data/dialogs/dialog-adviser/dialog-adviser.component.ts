import { SelectionModel } from '@angular/cdk/collections';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Adviser } from 'src/app/models/adviser';
import { AdvisoryCrop } from 'src/app/models/advisoryCrop';
import { AdviserService } from 'src/app/services/adviser.service';
import { AdvisoryCropService } from 'src/app/services/advisory-crop.service';

@Component({
  selector: 'app-dialog-adviser',
  templateUrl: './dialog-adviser.component.html',
  styleUrls: ['./dialog-adviser.component.scss']
})
export class DialogAdviserComponent implements OnInit {
  
  optionCrop = '';

  addAdviser = false;
  editAdviser = false;
  deleteAdviser = false;
  editAdvisoryCrops = false;
  showCrops = false;

  public formNewAdviser!: FormGroup;
  public formEditAdviser!: FormGroup;
  public formDeleteAdviser!: FormGroup;
  public formEditAdvisoryCrops!: FormGroup;

  formNewAdviserError = false;
  formEditAdviserError = false;
  formEditAdvisoryCropsError = false;
  cropSelectedError = false;

  displayedColumnsCrops: string[] = ['select', 'section', 'type', 'crop'];
  dataSourceAllCrops = new MatTableDataSource<AdvisoryCrop>();
  selection = new SelectionModel<AdvisoryCrop>(true, []);

  @ViewChild(MatPaginator, {static: true}) paginator!: MatPaginator;
  constructor(
    private adviserService: AdviserService,
    private advisoryCropsService: AdvisoryCropService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    @ Inject(MAT_DIALOG_DATA) public data: {adviser: Adviser, optionDialog: string},
    private formBuilder: FormBuilder
    ) {}

  ngOnInit(): void {
    this.initForms(this.data.optionDialog);
  }

  async initForms(option: string) {
    switch(option) {
      case 'new-adviser':
        this.addAdviser = true;
        this.formNewAdviser = this.formBuilder.group({
          orderNumber: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(5)]),
          name: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(20)]),
          lastName: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(40)]),
          nif: new FormControl('', [Validators.required, Validators.minLength(9), Validators.maxLength(9), Validators.pattern(/^[0-9]{8}[ABCDEFGHIJKLMNOPQRSTUVWXYZ]$/i)]),
          ropoNumber: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(15)]),
          fungusInstAdvice: new FormControl(true, [])
        });
        this.dataSourceAllCrops.data = await this.advisoryCropsService.findAll().toPromise();
        break;
      case 'edit-adviser':
        this.editAdviser = true;
        const updatedAdviser = this.data.adviser;
        let isFungusInstAdviceView = false;
        if (updatedAdviser.fungusInstAdvice == "Afirmativo") isFungusInstAdviceView = true;
        this.formEditAdviser = this.formBuilder.group({
          orderNumber: new FormControl(updatedAdviser.orderNumber, [Validators.required, Validators.minLength(1), Validators.maxLength(5)]),
          name: new FormControl(updatedAdviser.name, [Validators.required, Validators.minLength(2), Validators.maxLength(20)]),
          lastName: new FormControl(updatedAdviser.lastName, [Validators.required, Validators.minLength(2), Validators.maxLength(40)]),
          nif: new FormControl(updatedAdviser.nif, [Validators.required, Validators.minLength(9), Validators.maxLength(9), Validators.pattern(/^[0-9]{8}[ABCDEFGHIJKLMNOPQRSTUVWXYZ]$/i)]),
          ropoNumber: new FormControl(updatedAdviser.ropoNumber, [Validators.required, Validators.minLength(1), Validators.maxLength(15)]),
          fungusInstAdvice: new FormControl(isFungusInstAdviceView, [])
        });
        break;
      case 'delete-adviser':
        this.deleteAdviser = true;
        const deletedAdviser = this.data.adviser;
        let isFungusInstAdviceDel = false;
        if (deletedAdviser.fungusInstAdvice == "Afirmativo") isFungusInstAdviceDel = true;
        this.formDeleteAdviser = this.formBuilder.group({
          orderNumber: new FormControl(deletedAdviser.orderNumber),
          name: new FormControl(deletedAdviser.name),
          lastName: new FormControl(deletedAdviser.lastName),
          nif: new FormControl(deletedAdviser.nif),
          ropoNumber: new FormControl(deletedAdviser.ropoNumber),
          fungusInstAdvice: new FormControl(isFungusInstAdviceDel, [])
        });
        break;
      case 'show-advisoryCrops':
        this.showCrops = true;
        break;
      default:
        break;
    }
  }

  async saveAdviserDB() {
    this.formNewAdviser.markAllAsTouched();
    if(this.formNewAdviser.invalid) {
      this.formNewAdviserError = true;
    } else {
      
      try {
        if(this.selection.isEmpty()) {
          this.cropSelectedError = true;
        } else {
          const adviser: Adviser = this.formNewAdviser.value;
          adviser.crops = this.selection.selected;
          await this.adviserService.insert(adviser).toPromise();
          this.snackBar.open('AÑADIDO: Asesor de la explotación.', 'OK', 
          { duration: 4000, horizontalPosition: 'end' } );
          this.dialog.closeAll();
        }
      } catch {
        this.snackBar.open('ERROR al añadir.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
      }
    }
  }

  async updateAdviserDB() {
    this.formEditAdviser.markAllAsTouched();
    if (this.formEditAdviser.invalid) {
      this.formEditAdviserError = true;
    } else {
      try {
        let adviser: Adviser = this.formEditAdviser.value;
        await this.adviserService.update(adviser.nif, adviser).toPromise();
        this.snackBar.open('ACTUALIZADO: Asesor de la explotación.', 'OK', 
          { duration: 4000, horizontalPosition: 'end' } );
      } catch {
        this.snackBar.open('ERROR al actualizar.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
      } finally {
        this.dialog.closeAll();
      }
    }
  }

  async deleteAdviserDB() {
    try {
      await this.adviserService.delete(this.data.adviser.nif).toPromise();
      this.snackBar.open('ELIMINADO: Asesor de la explotación.', 'OK', 
        { duration: 4000, horizontalPosition: 'end' } );
    } catch {
      this.snackBar.open('ERROR al eliminar.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
    } finally {
      this.dialog.closeAll(); 
    }
  }

  goCrops(section: string) {
    this.optionCrop = section;
    this.initForms('add-advisoryCrops');
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSourceAllCrops.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSourceAllCrops.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: AdvisoryCrop): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id}`;
  }

}
