import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Outsider } from 'src/app/models/outsider';
import { OutsiderService } from 'src/app/services/outsider.service';

@Component({
  selector: 'app-dialog-outsiders',
  templateUrl: './dialog-outsiders.component.html',
  styleUrls: ['./dialog-outsiders.component.scss']
})
export class DialogOutsidersComponent implements OnInit {

  addOutsider = false;
  editOutsider = false;
  deleteOutsider = false;

  public formNewOutsider!: FormGroup;
  public formEditOutsider!: FormGroup;
  public formDeleteOutsider!: FormGroup;

  formNewOutsiderError = false;
  formEditOutsiderError = false;

  constructor(
    private outsiderService: OutsiderService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: {outsider: Outsider, optionDialog: string}
  ) { }

  ngOnInit(): void {
    this.initForms(this.data.optionDialog);
  }

  async initForms(option: string) {
    switch(option) {
      case 'new-outsider':
        this.addOutsider = true;
        this.formNewOutsider = this.formBuilder.group({
          orderNumber: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(5)]),
          name: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(20)]),
          lastName: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(40)]),
          nif: new FormControl('', [Validators.required, Validators.minLength(9), Validators.maxLength(9), Validators.pattern(/^[0-9]{8}[ABCDEFGHIJKLMNOPQRSTUVWXYZ]$/i)]),
          carnetType: new FormControl('', [Validators.required]),
          ropoNumber: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(15)])
        });
        break;
      case 'edit-outsider':
        this.editOutsider = true;
        const updatedOutsider = this.data.outsider;
        this.formEditOutsider = this.formBuilder.group({
          orderNumber: new FormControl(updatedOutsider.orderNumber, [Validators.required, Validators.minLength(1), Validators.maxLength(5)]),
          name: new FormControl(updatedOutsider.name, [Validators.required, Validators.minLength(2), Validators.maxLength(20)]),
          lastName: new FormControl(updatedOutsider.lastName, [Validators.required, Validators.minLength(2), Validators.maxLength(40)]),
          nif: new FormControl(updatedOutsider.nif, [Validators.required, Validators.minLength(9), Validators.maxLength(9), Validators.pattern(/^[0-9]{8}[ABCDEFGHIJKLMNOPQRSTUVWXYZ]$/i)]),
          carnetType: new FormControl(updatedOutsider.carnetType, [Validators.required]),
          ropoNumber: new FormControl(updatedOutsider.ropoNumber, [Validators.required, Validators.minLength(1), Validators.maxLength(15)])
        });
        break;
      case 'delete-outsider':
        this.deleteOutsider = true;
        const deletedOutsider = this.data.outsider;
        this.formDeleteOutsider = this.formBuilder.group({
          orderNumber: new FormControl(deletedOutsider.orderNumber),
          name: new FormControl(deletedOutsider.name),
          lastName: new FormControl(deletedOutsider.lastName),
          nif: new FormControl(deletedOutsider.nif),
          carnetType: new FormControl(deletedOutsider.carnetType),
          ropoNumber: new FormControl(deletedOutsider.ropoNumber)
        });
        break;
      default:
        break;
    }   
  }

  async saveOutsiderDB() {
    this.formNewOutsider.markAllAsTouched();
    if(this.formNewOutsider.invalid) {
      this.formNewOutsiderError = true;
    } else {
      const outsider: Outsider = this.formNewOutsider.value;
      try {
        await this.outsiderService.insert(outsider).toPromise();
        this.snackBar.open('AÑADIDO: Persona o empresa ajena a la explotación que interviene directamente en los tratamientos con productos fitosanitarios.', 'OK', 
          { duration: 4000, horizontalPosition: 'end' } );
          this.dialog.closeAll();
      } catch {
        this.snackBar.open('ERROR al añadir.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
      } 
    }
  }
  
  async updateOutsiderDB() {
    this.formEditOutsider.markAllAsTouched();
    if (this.formEditOutsider.invalid) {
      this.formEditOutsiderError = true;
    } else {
      try {
        let outsider: Outsider = this.formEditOutsider.value;
        await this.outsiderService.update(outsider.nif, outsider).toPromise();
        this.snackBar.open('ACTUALIZADO: Persona o empresa ajena a la explotación que interviene directamente en los tratamientos con productos fitosanitarios.', 'OK', 
          { duration: 4000, horizontalPosition: 'end' } );
      } catch {
        this.snackBar.open('ERROR al actualizar.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
      } finally {
        this.dialog.closeAll();
      }
    }
  }

  async deleteOutsiderDB() {
    try {
      await this.outsiderService.delete(this.data.outsider.nif).toPromise();
      this.snackBar.open('ELIMINADO: Persona o empresa ajena a la explotación que interviene directamente en los tratamientos con productos fitosanitarios.', 'OK', 
        { duration: 4000, horizontalPosition: 'end' } );
    } catch {
      this.snackBar.open('ERROR al eliminar.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
    } finally {
      this.dialog.closeAll(); 
    }
  }

}
