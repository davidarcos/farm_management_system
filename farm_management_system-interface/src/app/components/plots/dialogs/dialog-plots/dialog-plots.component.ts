import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Plot } from 'src/app/models/plot';
import { PlotService } from 'src/app/services/plot.service';

@Component({
  selector: 'app-dialog-plots',
  templateUrl: './dialog-plots.component.html',
  styleUrls: ['./dialog-plots.component.scss']
})
export class DialogPlotsComponent implements OnInit {

  editHerbPlot = false;
  editWoodyPlot = false;

  public formEditHerbPlot!: FormGroup;
  public formEditWoodyPlot!: FormGroup;

  formEditHerbPlotError = false;
  formEditWoodyPlotError = false;

  @ViewChild(MatPaginator, {static: true}) paginator!: MatPaginator;
  constructor(
    private plotService: PlotService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    @ Inject(MAT_DIALOG_DATA) public data: {plot: Plot, optionDialog: string},
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.initForms(this.data.optionDialog);
  }

  async initForms(option: string) {
    switch(option) {
      case 'edit-herbaceousPlot':
        this.editHerbPlot = true;
        const updatedHerbPlot = this.data.plot;
        this.formEditHerbPlot = this.formBuilder.group({
          orderNumber: new FormControl(updatedHerbPlot.orderNumber),
          category: new FormControl(updatedHerbPlot.category),
          cropType: new FormControl(updatedHerbPlot.cropType),
          cropTypeNumber: new FormControl(updatedHerbPlot.cropTypeNumber),
          crop: new FormControl(updatedHerbPlot.crop),
          cropNumber: new FormControl(updatedHerbPlot.cropNumber),
          variety: new FormControl(updatedHerbPlot.variety),
          varietyNumber: new FormControl(updatedHerbPlot.varietyNumber),
          varietyComments: new FormControl(updatedHerbPlot.varietyComments),
          isGenModified: new FormControl(updatedHerbPlot.isGenModified, Validators.required),
          varietyType: new FormControl(''),
          mainPattern: new FormControl(''),
          sizeX: new FormControl(''),
          sizeY: new FormControl(''),
          year: new FormControl(''),

          province: new FormControl(updatedHerbPlot.province),
          provinceNumber: new FormControl(updatedHerbPlot.provinceNumber),
          city: new FormControl(updatedHerbPlot.city),
          cityNumber: new FormControl(updatedHerbPlot.cityNumber),
          addition: new FormControl(updatedHerbPlot.addition),
          zone: new FormControl(updatedHerbPlot.zone),
          polygon: new FormControl(updatedHerbPlot.polygon),
          plot: new FormControl(updatedHerbPlot.plot),
          enclosure: new FormControl(updatedHerbPlot.enclosure),
          cultivatedArea: new FormControl(updatedHerbPlot.cultivatedArea, [Validators.required, Validators.minLength(1)]),
          area: new FormControl(updatedHerbPlot.area),
          sigpacUse: new FormControl(updatedHerbPlot.sigpacUse),
          slope: new FormControl(updatedHerbPlot.slope),

          isVulnerableArea: new FormControl(updatedHerbPlot.isVulnerableArea, Validators.required),
          isRedNatura: new FormControl(updatedHerbPlot.isRedNatura, Validators.required),
          isSecurityDistance: new FormControl(updatedHerbPlot.isSecurityDistance, Validators.required),
          securityDistance: new FormControl(updatedHerbPlot.securityDistance, Validators.required),
          waterExtPointsType: new FormControl(updatedHerbPlot.waterExtPointsType, Validators.required),
          measures: new FormControl(updatedHerbPlot.measures),

          exploitationType: new FormControl(updatedHerbPlot.exploitationType),
          irrigationSystem: new FormControl(updatedHerbPlot.irrigationSystem),
          protectionType: new FormControl(updatedHerbPlot.protectionType),
          comments: new FormControl(updatedHerbPlot.comments),

          certificationType: new FormControl(updatedHerbPlot.certificationType),
          gipAdviceSystem: new FormControl(updatedHerbPlot.gipAdviceSystem)

        });
        break;
      case 'edit-woodyPlot':
        this.editWoodyPlot = true;
        const updatedWoodyPlot = this.data.plot;
        this.formEditWoodyPlot = this.formBuilder.group({
          orderNumber: new FormControl(updatedWoodyPlot.orderNumber),
          category: new FormControl(updatedWoodyPlot.category),
          cropType: new FormControl(updatedWoodyPlot.cropType),
          cropTypeNumber: new FormControl(updatedWoodyPlot.cropTypeNumber),
          crop: new FormControl(updatedWoodyPlot.crop),
          cropNumber: new FormControl(updatedWoodyPlot.cropNumber),
          variety: new FormControl(updatedWoodyPlot.variety),
          varietyNumber: new FormControl(updatedWoodyPlot.varietyNumber),
          varietyComments: new FormControl(updatedWoodyPlot.varietyComments),
          isGenModified: new FormControl(''),
          varietyType: new FormControl(updatedWoodyPlot.varietyType, Validators.required),
          mainPattern: new FormControl(updatedWoodyPlot.mainPattern, [Validators.required, Validators.minLength(1)]),
          sizeX: new FormControl(updatedWoodyPlot.sizeX, Validators.required),
          sizeY: new FormControl(updatedWoodyPlot.sizeY, Validators.required),
          year: new FormControl(updatedWoodyPlot.year),

          province: new FormControl(updatedWoodyPlot.province),
          provinceNumber: new FormControl(updatedWoodyPlot.provinceNumber),
          city: new FormControl(updatedWoodyPlot.city),
          cityNumber: new FormControl(updatedWoodyPlot.cityNumber),
          addition: new FormControl(updatedWoodyPlot.addition),
          zone: new FormControl(updatedWoodyPlot.zone),
          polygon: new FormControl(updatedWoodyPlot.polygon),
          plot: new FormControl(updatedWoodyPlot.plot),
          enclosure: new FormControl(updatedWoodyPlot.enclosure),
          cultivatedArea: new FormControl(updatedWoodyPlot.cultivatedArea, [Validators.required, Validators.minLength(1)]),
          area: new FormControl(updatedWoodyPlot.area),
          sigpacUse: new FormControl(updatedWoodyPlot.sigpacUse),
          slope: new FormControl(updatedWoodyPlot.slope),

          isVulnerableArea: new FormControl(updatedWoodyPlot.isVulnerableArea, Validators.required),
          isRedNatura: new FormControl(updatedWoodyPlot.isRedNatura, Validators.required),
          isSecurityDistance: new FormControl(updatedWoodyPlot.isSecurityDistance, Validators.required),
          securityDistance: new FormControl(updatedWoodyPlot.securityDistance, Validators.required),
          waterExtPointsType: new FormControl(updatedWoodyPlot.waterExtPointsType, Validators.required),
          measures: new FormControl(updatedWoodyPlot.measures),

          exploitationType: new FormControl(updatedWoodyPlot.exploitationType),
          irrigationSystem: new FormControl(updatedWoodyPlot.irrigationSystem),
          protectionType: new FormControl(updatedWoodyPlot.protectionType),
          comments: new FormControl(updatedWoodyPlot.comments),

          certificationType: new FormControl(updatedWoodyPlot.certificationType),
          gipAdviceSystem: new FormControl(updatedWoodyPlot.gipAdviceSystem)

        });
        break;
      default:
        break;
    }
  }

  async updateHerbPlotDB(){
    this.formEditHerbPlot.markAllAsTouched();
    if (this.formEditHerbPlot.invalid) {
      this.formEditHerbPlotError = true;
    } else {
      try {
        let plot: Plot = this.formEditHerbPlot.value;
        await this.plotService.update(plot.orderNumber, plot).toPromise();
        this.snackBar.open('ACTUALIZADO: Parcela-recinto con cultivos herbáceos u otros.', 'OK', 
          { duration: 4000, horizontalPosition: 'end' } );
      } catch {
        this.snackBar.open('ERROR al actualizar.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
      } finally {
        this.dialog.closeAll();
      }
    }
  }

  async updateWoodyPlotDB(){
    this.formEditWoodyPlot.markAllAsTouched();
    if (this.formEditWoodyPlot.invalid) {
      this.formEditWoodyPlotError = true;
    } else {
      try {
        let plot: Plot = this.formEditWoodyPlot.value;
        await this.plotService.update(plot.orderNumber, plot).toPromise();
        this.snackBar.open('ACTUALIZADO: Parcela-recinto con cultivos leñosos.', 'OK', 
          { duration: 4000, horizontalPosition: 'end' } );
      } catch {
        this.snackBar.open('ERROR al actualizar.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
      } finally {
        this.dialog.closeAll();
      }
    }
  }

}
