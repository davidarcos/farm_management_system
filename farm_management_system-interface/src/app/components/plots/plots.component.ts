import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { timer } from 'rxjs';
import { take } from 'rxjs/operators';
import { Plot } from 'src/app/models/plot';
import { PlotService } from 'src/app/services/plot.service';
import { DialogPlotsComponent } from './dialogs/dialog-plots/dialog-plots.component';

@Component({
  selector: 'app-plots',
  templateUrl: './plots.component.html',
  styleUrls: ['./plots.component.scss']
})
export class PlotsComponent implements OnInit {

  currentTab = 0;

  displayedColumnsPlots: string[] = ['orderNumber', 'provinceNumber', 'city', 'addition' , 'zone', 
  'polygon', 'plot', 'enclosure', 'cultivatedArea', 'area', 'sigpacUse', 'slope', 'crop', 'options'];

  dataSourcePlots = new MatTableDataSource<Plot>();

  noLoading = false;

  @ViewChild(MatPaginator, {static: true}) paginator!: MatPaginator;
  constructor(
    private plotsService: PlotService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar
    ) {}

  ngOnInit(){
    this.refreshItems('herbaceousPlots',1);
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    switch(tabChangeEvent.index) {
      case 0:
        this.refreshItems('herbaceousPlots',0);
        this.currentTab = 0;
        break;      
      case 1:
        this.refreshItems('woodyPlots',0);
        this.currentTab = 1;
        break;
    }
  }

  openDialog(option: string, param: any) {
    switch(option) {
      case 'edit-herbaceousPlot':
        const updatedHerbaceousPlot: Plot = param;
        const dialogRef_editHerbaceousPlot = this.dialog.open(DialogPlotsComponent, 
          {data: {plot: updatedHerbaceousPlot, optionDialog: option}});
        dialogRef_editHerbaceousPlot.afterClosed().subscribe(result => {
          this.refreshItems('herbaceousPlots',1500);
        });
        break;
      case 'edit-woodyPlot':
        const updatedWoodyPlot: Plot = param;
        const dialogRef_editWoodyPlot = this.dialog.open(DialogPlotsComponent, 
          {data: {plot: updatedWoodyPlot, optionDialog: option}});
        dialogRef_editWoodyPlot.afterClosed().subscribe(result => {
          this.refreshItems('woodyPlots',1500);
        });
        break;
    }
  }

  async refreshItems(table: string, time: number, showMsg: boolean = false) {
    await timer(time).pipe(take(1)).toPromise();
    switch(table) {
      case 'herbaceousPlots':
        this.noLoading = false;
        this.findByCategory('herbaceous');
        break;
      case 'woodyPlots':
        this.noLoading = false;
        this.findByCategory('woody');
        break;
      default: 
        break;
    }

    if(showMsg && !this.noLoading) {
      this.snackBar.open('¡Datos actualizados correctamente!', 'OK', 
      { duration: 2000, horizontalPosition: 'end' } );
    }

  }

  async findByCategory(category: string){
    try {
      this.dataSourcePlots.data = await this.plotsService.findByCategory(category).toPromise();
      this.dataSourcePlots.paginator = this.paginator;
    } catch {
      this.noLoading = true;
    }
  }

}
