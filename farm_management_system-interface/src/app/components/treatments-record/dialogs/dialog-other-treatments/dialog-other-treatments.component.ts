import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { OtherTreatment } from 'src/app/models/otherTreatment';
import { OtherTreatmentService } from 'src/app/services/other-treatment.service';
import { OwnerService } from 'src/app/services/owner.service';

@Component({
  selector: 'app-dialog-other-treatments',
  templateUrl: './dialog-other-treatments.component.html',
  styleUrls: ['./dialog-other-treatments.component.scss']
})
export class DialogOtherTreatmentsComponent implements OnInit {

  addStoragePremiseTreatment = false;
  addStoredProductTreatment = false;
  addOwnTransportTreatment = false;
  editStoragePremiseTreatment = false;
  editStoredProductTreatment = false;
  editOwnTransportTreatment = false;
  deleteStoragePremiseTreatment = false;
  deleteStoredProductTreatment = false;
  deleteOwnTransportTreatment = false;

  public formNewStoragePremiseTreatment!: FormGroup;
  public formNewStoredProductTreatment!: FormGroup;
  public formNewOwnTransportTreatment!: FormGroup;
  public formEditStoragePremiseTreatment!: FormGroup;
  public formEditStoredProductTreatment!: FormGroup;
  public formEditOwnTransportTreatment!: FormGroup;
  public formDeleteStoragePremiseTreatment!: FormGroup;
  public formDeleteStoredProductTreatment!: FormGroup;
  public formDeleteOwnTransportTreatment!: FormGroup;

  formNewStoragePremiseTreatmentError = false;
  formNewStoredProductTreatmentError = false;
  formNewOwnTransportTreatmentError = false;
  formEditStoragePremiseTreatmentError = false;
  formEditStoredProductTreatmentError = false;
  formEditOwnTransportTreatmentError = false;
  nifOwnerError = false;

  treatment = 0;

  @ViewChild(MatPaginator, {static: true}) paginator!: MatPaginator;
  constructor(
    private otherTreatmentService: OtherTreatmentService,
    private ownerService: OwnerService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    @ Inject(MAT_DIALOG_DATA) public data: {treatment: OtherTreatment, optionDialog: string},
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.initForms(this.data.optionDialog);
  }

  async initForms(option: string) {
    switch(option) {
      case 'new-storagePremise':
        this.treatment = 1;
        this.addStoragePremiseTreatment = true;
        this.formNewStoragePremiseTreatment = this.formBuilder.group({
          name: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(20)]),
          volume: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(20)]),
          date: new FormControl('', [Validators.required, Validators.pattern(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/i)]),
          nifOwner: new FormControl('', [Validators.required, Validators.minLength(9), Validators.maxLength(9)]),
          reason_plague: new FormControl('', [Validators.required]),
          tradeName: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(20)]),
          registryNumber: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(12)]),
          amount: new FormControl('', [Validators.required, Validators.minLength(1)]),
          category: new FormControl('En local de almacenamiento')
        });
        break;
      case 'new-storedProduct':
        this.treatment = 2;
        this.addStoredProductTreatment = true;
        this.formNewStoredProductTreatment = this.formBuilder.group({
          name: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(20)]),
          volume: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(20)]),
          date: new FormControl('', [Validators.required, Validators.pattern(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/i)]),
          nifOwner: new FormControl('', [Validators.required, Validators.minLength(9), Validators.maxLength(9)]),
          reason_plague: new FormControl('', [Validators.required]),
          tradeName: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(20)]),
          registryNumber: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(12)]),
          amount: new FormControl('', [Validators.required, Validators.minLength(1)]),
          category: new FormControl('En productos almacenados')
        });
        break;
      case 'new-ownTransport':
        this.treatment = 3;
        this.addOwnTransportTreatment = true;
        this.formNewOwnTransportTreatment = this.formBuilder.group({
          name: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(20)]),
          volume: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(20)]),
          date: new FormControl('', [Validators.required, Validators.pattern(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/i)]),
          nifOwner: new FormControl('', [Validators.required, Validators.minLength(9), Validators.maxLength(9)]),
          reason_plague: new FormControl('', [Validators.required]),
          tradeName: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(20)]),
          registryNumber: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(12)]),
          amount: new FormControl('', [Validators.required, Validators.minLength(1)]),
          category: new FormControl('En medios de transporte propios')
        });
        break;
      case 'edit-storagePremise':
        this.treatment = 1;
        this.editStoragePremiseTreatment = true;
        const updatedStoragePremsieTreatment = this.data.treatment;
        this.formEditStoragePremiseTreatment = this.formBuilder.group({
          id: new FormControl(updatedStoragePremsieTreatment.id),
          category: new FormControl(updatedStoragePremsieTreatment.category),
          name: new FormControl(updatedStoragePremsieTreatment.name),
          volume: new FormControl(updatedStoragePremsieTreatment.volume),
          date: new FormControl(updatedStoragePremsieTreatment.date, [Validators.required, Validators.pattern(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/i)]),
          nifOwner: new FormControl(updatedStoragePremsieTreatment.nifOwner, [Validators.required, Validators.minLength(9), Validators.maxLength(9)]),
          reason_plague: new FormControl(updatedStoragePremsieTreatment.reason_plague, [Validators.required]),
          tradeName: new FormControl(updatedStoragePremsieTreatment.tradeName),
          registryNumber: new FormControl(updatedStoragePremsieTreatment.registryNumber),
          amount: new FormControl(updatedStoragePremsieTreatment.amount, [Validators.required, Validators.minLength(1)])
        });
        break;
      case 'edit-storedProduct':
        this.treatment = 2;
        this.editStoredProductTreatment = true;
        const updatedStoredProduct = this.data.treatment;
        this.formEditStoredProductTreatment = this.formBuilder.group({
          id: new FormControl(updatedStoredProduct.id),
          category: new FormControl(updatedStoredProduct.category),
          name: new FormControl(updatedStoredProduct.name),
          volume: new FormControl(updatedStoredProduct.volume),
          date: new FormControl(updatedStoredProduct.date, [Validators.required, Validators.pattern(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/i)]),
          nifOwner: new FormControl(updatedStoredProduct.nifOwner, [Validators.required, Validators.minLength(9), Validators.maxLength(9)]),
          reason_plague: new FormControl(updatedStoredProduct.reason_plague, [Validators.required]),
          tradeName: new FormControl(updatedStoredProduct.tradeName),
          registryNumber: new FormControl(updatedStoredProduct.registryNumber),
          amount: new FormControl(updatedStoredProduct.amount, [Validators.required, Validators.minLength(1)])
        });
        break;
      case 'edit-ownTransport':
        this.treatment = 3;
        this.editOwnTransportTreatment = true;
        const updatedOwnTransportTreatment = this.data.treatment;
        this.formEditOwnTransportTreatment = this.formBuilder.group({
          id: new FormControl(updatedOwnTransportTreatment.id),
          category: new FormControl(updatedOwnTransportTreatment.category),
          name: new FormControl(updatedOwnTransportTreatment.name),
          volume: new FormControl(updatedOwnTransportTreatment.volume),
          date: new FormControl(updatedOwnTransportTreatment.date, [Validators.required, Validators.pattern(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/i)]),
          nifOwner: new FormControl(updatedOwnTransportTreatment.nifOwner, [Validators.required, Validators.minLength(9), Validators.maxLength(9)]),
          reason_plague: new FormControl(updatedOwnTransportTreatment.reason_plague, [Validators.required]),
          tradeName: new FormControl(updatedOwnTransportTreatment.tradeName),
          registryNumber: new FormControl(updatedOwnTransportTreatment.registryNumber),
          amount: new FormControl(updatedOwnTransportTreatment.amount, [Validators.required, Validators.minLength(1)])
        });
        break;
      case 'delete-storagePremise':
        this.treatment = 1;
        this.deleteStoragePremiseTreatment = true;
        const deletedStoragePremsieTreatment = this.data.treatment;
        this.formDeleteStoragePremiseTreatment = this.formBuilder.group({
          id: new FormControl(deletedStoragePremsieTreatment.id),
          category: new FormControl(deletedStoragePremsieTreatment.category),
          name: new FormControl(deletedStoragePremsieTreatment.name),
          volume: new FormControl(deletedStoragePremsieTreatment.volume),
          date: new FormControl(deletedStoragePremsieTreatment.date),
          nifOwner: new FormControl(deletedStoragePremsieTreatment.nifOwner),
          reason_plague: new FormControl(deletedStoragePremsieTreatment.reason_plague),
          tradeName: new FormControl(deletedStoragePremsieTreatment.tradeName),
          registryNumber: new FormControl(deletedStoragePremsieTreatment.registryNumber),
          amount: new FormControl(deletedStoragePremsieTreatment.amount)
        });
        break;
      case 'delete-storedProduct':
        this.treatment = 2;
        this.deleteStoredProductTreatment = true;
        const deletedStoredProduct = this.data.treatment;
        this.formDeleteStoredProductTreatment = this.formBuilder.group({
          id: new FormControl(deletedStoredProduct.id),
          category: new FormControl(deletedStoredProduct.category),
          name: new FormControl(deletedStoredProduct.name),
          volume: new FormControl(deletedStoredProduct.volume),
          date: new FormControl(deletedStoredProduct.date),
          nifOwner: new FormControl(deletedStoredProduct.nifOwner),
          reason_plague: new FormControl(deletedStoredProduct.reason_plague),
          tradeName: new FormControl(deletedStoredProduct.tradeName),
          registryNumber: new FormControl(deletedStoredProduct.registryNumber),
          amount: new FormControl(deletedStoredProduct.amount)
        });
        break;
      case 'delete-ownTransport':
        this.treatment = 3;
        this.deleteOwnTransportTreatment = true;
        const deletedOwnTransportTreatment = this.data.treatment;
        this.formDeleteOwnTransportTreatment = this.formBuilder.group({
          id: new FormControl(deletedOwnTransportTreatment.id),
          category: new FormControl(deletedOwnTransportTreatment.category),
          name: new FormControl(deletedOwnTransportTreatment.name),
          volume: new FormControl(deletedOwnTransportTreatment.volume),
          date: new FormControl(deletedOwnTransportTreatment.date),
          nifOwner: new FormControl(deletedOwnTransportTreatment.nifOwner),
          reason_plague: new FormControl(deletedOwnTransportTreatment.reason_plague),
          tradeName: new FormControl(deletedOwnTransportTreatment.tradeName),
          registryNumber: new FormControl(deletedOwnTransportTreatment.registryNumber),
          amount: new FormControl(deletedOwnTransportTreatment.amount)
        });
        break;
      default:
        break;
    }
  }
  async saveOtherTreatmentDB() {
    if(this.treatment == 1) {
      this.formNewStoragePremiseTreatment.markAllAsTouched();
      if(this.formNewStoragePremiseTreatment.invalid) {
        this.formNewStoragePremiseTreatmentError = true;
      } else {
        const treatment: OtherTreatment = this.formNewStoragePremiseTreatment.value;
        const nifOwner = treatment.nifOwner as string;
        try {
          const owners = await this.ownerService.findByNif(nifOwner).toPromise();
          if(owners.length < 1) {
            this.nifOwnerError = true;
            this.formNewStoragePremiseTreatmentError = true;
          } else {
            const myDate = new Date();
            treatment.id =  new Date(myDate).getTime().toString();
            await this.otherTreatmentService.insert(treatment).toPromise();
            this.snackBar.open('AÑADIDO: Registro de un tratamiento en locales de almacenamiento.', 'OK', 
              { duration: 4000, horizontalPosition: 'end' } );
            this.dialog.closeAll();
          }
        } catch {
          this.snackBar.open('ERROR al añadir.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
        }
      }
    } else if(this.treatment == 2) {
      this.formNewStoredProductTreatment.markAllAsTouched();
      if(this.formNewStoredProductTreatment.invalid) {
        this.formNewStoredProductTreatmentError = true;
      } else {
        const treatment: OtherTreatment = this.formNewStoredProductTreatment.value;
        const nifOwner = treatment.nifOwner as string;
        try {
          const owners = await this.ownerService.findByNif(nifOwner).toPromise();
          if(owners.length < 1) {
            this.nifOwnerError = true;
            this.formNewStoragePremiseTreatmentError = true;
          } else {
            const myDate = new Date();
            treatment.id =  new Date(myDate).getTime().toString();
            await this.otherTreatmentService.insert(treatment).toPromise();
            this.snackBar.open('AÑADIDO: Registro de un tratamiento en productos almacenados.', 'OK', 
              { duration: 4000, horizontalPosition: 'end' } );
            this.dialog.closeAll();
          }
        } catch {
          this.snackBar.open('ERROR al añadir.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
        }
      }
    } else if(this.treatment == 3) {
      this.formNewOwnTransportTreatment.markAllAsTouched();
      if(this.formNewOwnTransportTreatment.invalid) {
        this.formNewOwnTransportTreatmentError = true;
      } else {
        const treatment: OtherTreatment = this.formNewOwnTransportTreatment.value;
        const nifOwner = treatment.nifOwner as string;
        try {
          const owners = await this.ownerService.findByNif(nifOwner).toPromise();
          if(owners.length < 1) {
            this.nifOwnerError = true;
            this.formNewStoragePremiseTreatmentError = true;
          } else {
            const myDate = new Date();
            treatment.id =  new Date(myDate).getTime().toString();
            await this.otherTreatmentService.insert(treatment).toPromise();
            this.snackBar.open('AÑADIDO: Registro de un tratamiento en medios de transporte propios.', 'OK', 
              { duration: 4000, horizontalPosition: 'end' } );
            this.dialog.closeAll();
          }
        } catch {
          this.snackBar.open('ERROR al añadir.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
        }
      }
    } else {
      this.snackBar.open('ERROR al añadir.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
      this.dialog.closeAll();
    }
  }
  
  async updateOtherTreatmentDB() {
    if(this.treatment == 1) {
      this.formEditStoragePremiseTreatment.markAllAsTouched();
      if(this.formEditStoragePremiseTreatment.invalid) {
        this.formEditStoragePremiseTreatmentError = true;
      } else {
        const treatment: OtherTreatment = this.formEditStoragePremiseTreatment.value;
        try {
          await this.otherTreatmentService.update(treatment.id, treatment).toPromise();
          this.snackBar.open('ACTUALIZAR: Registro de un tratamiento en locales de almacenamiento.', 'OK', 
            { duration: 4000, horizontalPosition: 'end' } );
        } catch {
          this.snackBar.open('ERROR al actualizar.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
        } finally {
          this.dialog.closeAll();
        }
      }
    } else if(this.treatment == 2) {
      this.formEditStoredProductTreatment.markAllAsTouched();
      if(this.formEditStoredProductTreatment.invalid) {
        this.formEditStoredProductTreatmentError = true;
      } else {
        const treatment: OtherTreatment = this.formEditStoredProductTreatment.value;
        try {
          await this.otherTreatmentService.update(treatment.id, treatment).toPromise();
          this.snackBar.open('ACTUALIZADO: Registro de un tratamiento en productos almacenados.', 'OK', 
            { duration: 4000, horizontalPosition: 'end' } );
        } catch {
          this.snackBar.open('ERROR al actualizar.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
        } finally {
          this.dialog.closeAll();
        }
      }
    } else if(this.treatment == 3) {
      this.formEditOwnTransportTreatment.markAllAsTouched();
      if(this.formEditOwnTransportTreatment.invalid) {
        this.formEditOwnTransportTreatmentError = true;
      } else {
        const treatment: OtherTreatment = this.formEditOwnTransportTreatment.value;
        try {
          await this.otherTreatmentService.update(treatment.id, treatment).toPromise();
          this.snackBar.open('ACTUALIZADO: Registro de un tratamiento en medios de transporte propios.', 'OK', 
            { duration: 4000, horizontalPosition: 'end' } );
        } catch {
          this.snackBar.open('ERROR al actualizar.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
        } finally {
          this.dialog.closeAll();
        }
      }
    } else {
      this.snackBar.open('ERROR al actualizar.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
      this.dialog.closeAll();
    }
  }

  async deleteOtherTreatmentDB() {
    try {
      await this.otherTreatmentService.delete(this.data.treatment.id).toPromise();
      this.snackBar.open('ELIMINADO: Registro de un tratamiento.', 'OK', 
      { duration: 4000, horizontalPosition: 'end' } );
    } catch {
      this.snackBar.open('ERROR al eliminar.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
    } finally {
      this.dialog.closeAll(); 
    }
  }

}
