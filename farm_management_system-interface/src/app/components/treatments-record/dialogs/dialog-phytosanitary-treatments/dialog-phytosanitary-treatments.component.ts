import { SelectionModel } from '@angular/cdk/collections';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { PhytosanitaryTreatment } from 'src/app/models/phytosanitaryTreatment';
import { Plot } from 'src/app/models/plot';
import { PhytosanitaryTreatmentService } from 'src/app/services/phytosanitary-treatment.service';
import { PlotService } from 'src/app/services/plot.service';
import { OwnerService } from 'src/app/services/owner.service';
import { AdviserService } from 'src/app/services/adviser.service';

@Component({
  selector: 'app-dialog-phytosanitary-treatments',
  templateUrl: './dialog-phytosanitary-treatments.component.html',
  styleUrls: ['./dialog-phytosanitary-treatments.component.scss']
})
export class DialogPhytosanitaryTreatmentsComponent implements OnInit {

  addPhytosanitaryTreatment = false;
  editPhytosanitaryTreatment = false;
  deletePhytosanitaryTreatment = false;

  public formNewPhytosanitaryTreatment!: FormGroup;
  public formEditPhytosanitaryTreatment!: FormGroup;
  public formDeletePhytosanitaryTreatment!: FormGroup;

  formNewPhytosanitaryTreatmentError = false;
  errorForm = false;
  formEditPhytosanitaryTreatmentError = false;
  plotSelectedError = false;
  nifOwnerError = false;
  nifAdviserError = false;

  displayedColumnsPlots: string[] = ['select', 'orderNumber', 'crop', 'provinceNumber', 'city', 'addition' , 'zone', 
  'polygon', 'plot', 'enclosure', 'cultivatedArea'];

  dataSourceAllPlots = new MatTableDataSource<Plot>();
  selection = new SelectionModel<Plot>(true, []);
  
  @ViewChild(MatPaginator, {static: true}) paginator!: MatPaginator;
  constructor(
    private phytosanitaryTreatmentService: PhytosanitaryTreatmentService,
    private ownerService: OwnerService,
    private adviserService: AdviserService,
    private plotService: PlotService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    @ Inject(MAT_DIALOG_DATA) public data: {phytosanitaryTreatment: PhytosanitaryTreatment, optionDialog: string},
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.initForms(this.data.optionDialog);
  }

  async initForms(option: string) {
    switch(option) {
      case 'new-phytosanitaryTreatment':
        this.addPhytosanitaryTreatment = true;
        this.formNewPhytosanitaryTreatment = this.formBuilder.group({
          crop: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(30)]),
          pest: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(30)]),
          justification: new FormControl('', [Validators.required]),
          name: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(20)]),
          registryNumber: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(20)]),
          activeMatter: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(40)]),
          lotNumber: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(20)]),
          amount: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(20)]),
          measurementUnit: new FormControl('', [Validators.required]),
          nonChemicalAlternatives: new FormControl('', [Validators.required]),
          date: new FormControl('', [Validators.required, Validators.pattern(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/i)]),
          effectiveness: new FormControl('', [Validators.required]),
          nifOwner: new FormControl('', [Validators.required, Validators.minLength(9), Validators.maxLength(9)]),
          nifAdviser: new FormControl('', [Validators.required, Validators.minLength(9), Validators.maxLength(9)]),
          treatmentTeam: new FormControl('', [Validators.minLength(1), Validators.maxLength(7)]),
          romaNumber: new FormControl('', [Validators.minLength(1), Validators.maxLength(12)]),
          comments: new FormControl(''),
        });
        this.dataSourceAllPlots.data = await this.plotService.findAll().toPromise();
        break;
      case 'edit-phytosanitaryTreatment':
        this.editPhytosanitaryTreatment = true;
        const updatedPT = this.data.phytosanitaryTreatment;
        this.formEditPhytosanitaryTreatment = this.formBuilder.group({
          id: new FormControl(updatedPT.id),
          crop: new FormControl(updatedPT.crop),
          pest: new FormControl(updatedPT.pest),
          justification: new FormControl(updatedPT.justification),
          name: new FormControl(updatedPT.name),
          registryNumber: new FormControl(updatedPT.registryNumber),
          activeMatter: new FormControl(updatedPT.activeMatter),
          lotNumber: new FormControl(updatedPT.lotNumber),
          amount: new FormControl(updatedPT.amount, [Validators.required, Validators.minLength(1), Validators.maxLength(20)]),
          measurementUnit: new FormControl(updatedPT.measurementUnit, [Validators.required]),
          nonChemicalAlternatives: new FormControl(updatedPT.nonChemicalAlternatives),
          date: new FormControl(updatedPT.date, [Validators.required, Validators.pattern(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/i)]),
          effectiveness: new FormControl(updatedPT.effectiveness, [Validators.required]),
          nifOwner: new FormControl(updatedPT.nifOwner, [Validators.required, Validators.minLength(9), Validators.maxLength(9)]),
          nifAdviser: new FormControl(updatedPT.nifAdviser, [Validators.required, Validators.minLength(9), Validators.maxLength(9)]),
          treatmentTeam: new FormControl(updatedPT.treatmentTeam, [Validators.required, Validators.minLength(1), Validators.maxLength(7)]),
          romaNumber: new FormControl(updatedPT.romaNumber, [Validators.required, Validators.minLength(1), Validators.maxLength(12)]),
          comments: new FormControl(updatedPT.comments),
        });
        break;
      case 'delete-phytosanitaryTreatment':
        this.deletePhytosanitaryTreatment = true;
        const deletedPT = this.data.phytosanitaryTreatment;
        this.formDeletePhytosanitaryTreatment = this.formBuilder.group({
          id: new FormControl(deletedPT.id),
          crop: new FormControl(deletedPT.crop),
          pest: new FormControl(deletedPT.pest),
          justification: new FormControl(deletedPT.justification),
          name: new FormControl(deletedPT.name),
          registryNumber: new FormControl(deletedPT.registryNumber),
          activeMatter: new FormControl(deletedPT.activeMatter),
          lotNumber: new FormControl(deletedPT.lotNumber),
          nonChemicalAlternatives: new FormControl(deletedPT.nonChemicalAlternatives),
        });
        break;
      default:
        break;
    }
  }

  async savePhytosanitaryTreatmentDB() {
    this.formNewPhytosanitaryTreatmentError = false;
    this.errorForm = false;
    this.nifOwnerError = false;
    this.nifAdviserError = false;

    try {
      if(this.formNewPhytosanitaryTreatment.invalid) {
        if(
            this.formNewPhytosanitaryTreatment.get('crop')?.invalid ||
            this.formNewPhytosanitaryTreatment.get('pest')?.invalid ||
            this.formNewPhytosanitaryTreatment.get('justification')?.invalid ||
            this.formNewPhytosanitaryTreatment.get('date')?.invalid ||
            this.formNewPhytosanitaryTreatment.get('effectiveness')?.invalid ||
            this.formNewPhytosanitaryTreatment.get('nifOwner')?.invalid ||
            this.formNewPhytosanitaryTreatment.get('nifAdviser')?.invalid ||
            this.formNewPhytosanitaryTreatment.get('treatmentTeam')?.invalid
          ) {
            this.formNewPhytosanitaryTreatment.markAllAsTouched();
          } else {
            if(
              this.formNewPhytosanitaryTreatment.get('nonChemicalAlternatives')?.value != 'N/A' &&
              this.formNewPhytosanitaryTreatment.get('name')?.invalid &&
              this.formNewPhytosanitaryTreatment.get('registryNumber')?.invalid &&
              this.formNewPhytosanitaryTreatment.get('activeMatter')?.invalid &&
              this.formNewPhytosanitaryTreatment.get('lotNumber')?.invalid &&
              this.formNewPhytosanitaryTreatment.get('amount')?.invalid &&
              this.formNewPhytosanitaryTreatment.get('measurementUnit')?.invalid
            ) {
              if(this.selection.isEmpty()) {
                this.plotSelectedError = true;
              } else {
                const phytosanitaryTreatment: PhytosanitaryTreatment = this.formNewPhytosanitaryTreatment.value;
                const nifOwner = phytosanitaryTreatment.nifOwner as string;
                const nifAdivser = phytosanitaryTreatment.nifAdviser as string;
                const owners = await this.ownerService.findByNif(nifOwner).toPromise();
                const adviser = await this.adviserService.findByNif(nifAdivser).toPromise();
                if(owners.length < 1) {
                  this.nifOwnerError = true;
                  if(adviser.length < 1) {
                    this.nifAdviserError = true;
                  }
                  this.formNewPhytosanitaryTreatmentError = true;
                } else {
                  if(adviser.length < 1) {
                    this.nifAdviserError = true;
                    this.formNewPhytosanitaryTreatmentError = true;
                  } else {
                    const myDate = new Date();
                    phytosanitaryTreatment.id =  new Date(myDate).getTime().toString();
                    phytosanitaryTreatment.plots = this.selection.selected;
                    await this.phytosanitaryTreatmentService.insert(phytosanitaryTreatment).toPromise();
                    this.snackBar.open('AÑADIDO: Registro de tratamientos fitosanitarios.', 'OK', 
                    { duration: 4000, horizontalPosition: 'end' } );
                    this.dialog.closeAll();
                  }
                }
              }
            } else {
              this.errorForm = true;
              this.formNewPhytosanitaryTreatment.markAllAsTouched();
            }
          }
        this.formNewPhytosanitaryTreatmentError = true;
      } else {
        if(this.formNewPhytosanitaryTreatment.get('nonChemicalAlternatives')?.value != 'N/A') {
          this.errorForm = true;
        } else {
          if(this.selection.isEmpty()) {
            this.plotSelectedError = true;
          } else {
            const phytosanitaryTreatment: PhytosanitaryTreatment = this.formNewPhytosanitaryTreatment.value;
            const nifOwner = phytosanitaryTreatment.nifOwner as string;
            const nifAdivser = phytosanitaryTreatment.nifAdviser as string;
            const owners = await this.ownerService.findByNif(nifOwner).toPromise();
            const adviser = await this.adviserService.findByNif(nifAdivser).toPromise();
            if(owners.length < 1) {
              this.nifOwnerError = true;
              if(adviser.length < 1) {
                this.nifAdviserError = true;
              }
              this.formNewPhytosanitaryTreatmentError = true;
            } else {
              if(adviser.length < 1) {
                this.nifAdviserError = true;
                this.formNewPhytosanitaryTreatmentError = true;
              } else {
                phytosanitaryTreatment.nonChemicalAlternatives = "";
                const myDate = new Date();
                phytosanitaryTreatment.id =  new Date(myDate).getTime().toString();
                phytosanitaryTreatment.plots = this.selection.selected;
                await this.phytosanitaryTreatmentService.insert(phytosanitaryTreatment).toPromise();
                this.snackBar.open('AÑADIDO: Registro de tratamientos fitosanitarios.', 'OK', 
                { duration: 4000, horizontalPosition: 'end' } );
                this.dialog.closeAll();
              }
            }
          }
        }
      }
    } catch {
      this.snackBar.open('ERROR al añadir.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
    } 
  }

  async updatePhytosanitaryTreatmentDB() {
    this.formEditPhytosanitaryTreatment.markAllAsTouched();
    if (this.formEditPhytosanitaryTreatment.invalid) {
      this.formEditPhytosanitaryTreatmentError = true;
    } else {
      try {
        let phytosanitaryTreatment: PhytosanitaryTreatment = this.formEditPhytosanitaryTreatment.value;
        phytosanitaryTreatment.plots = this.data.phytosanitaryTreatment.plots;
        await this.phytosanitaryTreatmentService.update(phytosanitaryTreatment.id, phytosanitaryTreatment).toPromise();
        this.snackBar.open('ACTUALIZADO: Registro de tratamientos fitosanitarios.', 'OK', 
          { duration: 4000, horizontalPosition: 'end' } );
      } catch {
        this.snackBar.open('ERROR al actualizar.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
      } finally {
        this.dialog.closeAll();
      }
    }
  }

  async deletePhytosanitaryTreatmentDB() {
    try {
      await this.phytosanitaryTreatmentService.delete(this.data.phytosanitaryTreatment.id).toPromise();
      this.snackBar.open('ELIMINADO: Registro de tratamientos fitosanitarios.', 'OK', 
      { duration: 4000, horizontalPosition: 'end' } );
    } catch {
      this.snackBar.open('ERROR al eliminar.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
    } finally {
      this.dialog.closeAll(); 
    }
  }


  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSourceAllPlots.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSourceAllPlots.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: Plot): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.orderNumber}`;
  }
    
}
