import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { timer } from 'rxjs';
import { take } from 'rxjs/operators';
import { OtherTreatment } from 'src/app/models/otherTreatment';
import { PhytosanitaryTreatment } from 'src/app/models/phytosanitaryTreatment';
import { OtherTreatmentService } from 'src/app/services/other-treatment.service';
import { PhytosanitaryTreatmentService } from 'src/app/services/phytosanitary-treatment.service';
import { DialogOtherTreatmentsComponent } from './dialogs/dialog-other-treatments/dialog-other-treatments.component';
import { DialogPhytosanitaryTreatmentsComponent } from './dialogs/dialog-phytosanitary-treatments/dialog-phytosanitary-treatments.component';

@Component({
  selector: 'app-treatments-record',
  templateUrl: './treatments-record.component.html',
  styleUrls: ['./treatments-record.component.scss']
})
export class TreatmentsRecordComponent implements OnInit {

  displayedColumnsPhytoTreatments: string[] = ['id', 'name', 'registryNumber', 'activeMatter', 
  'lotNumber', 'amount', 'crop', 'pest', 'date', 'justification', 'nonChemicalAlternatives', 'options'];
  displayedColumnsOtherTreatments: string[] = ['id', 'tradeName', 'registryNumber', 'amount', 'name', 
  'volume', 'date', 'nifOwner', 'reason_plague', 'options'];

  dataSourcePhytoTreatments = new MatTableDataSource<PhytosanitaryTreatment>();
  dataSourceOtherTreatments = new MatTableDataSource<OtherTreatment>();

  lengthPhytoTreatments = 0;
  lengthOtherTreatments = 0;

  noLoading = false;

  currentTab = 0;

  @ViewChild(MatPaginator, {static: true}) paginator!: MatPaginator;

  constructor(
    private phytoTreatmentService: PhytosanitaryTreatmentService ,
    private otherTreatmentService: OtherTreatmentService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(){
    this.refreshItems('phytoTreatments',1);
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    switch(tabChangeEvent.index) {
      case 0:
        this.refreshItems('phytoTreatments',0);
        this.currentTab = 0;
        break;
      case 1:
        this.refreshItems('storagePremises',0);
        this.currentTab = 1;
        break;
      case 2:
        this.refreshItems('storedProducts',0);
        this.currentTab = 2;
        break;
      case 3:
        this.refreshItems('ownTransports',0);
        this.currentTab = 3;
        break;
    }
  }

  openDialog(option: string, param: any) {
    switch(option) {
      case 'new-phytosanitaryTreatment':
        const dialogRef_newFarmPhytosanitaryTreatment = this.dialog.open(DialogPhytosanitaryTreatmentsComponent,
          {data: {phytosanitaryTreatment: null, optionDialog: option}});
          dialogRef_newFarmPhytosanitaryTreatment.afterClosed().subscribe(result => {
          this.refreshItems('phytoTreatments',1500);
        });
        break;
      case 'edit-phytosanitaryTreatment':
        const updatedPhytosanitaryTreatment: PhytosanitaryTreatment = param;
        const dialogRef_editPhytosanitaryTreatment = this.dialog.open(DialogPhytosanitaryTreatmentsComponent, 
          {data: {phytosanitaryTreatment: updatedPhytosanitaryTreatment, optionDialog: option}});
        dialogRef_editPhytosanitaryTreatment.afterClosed().subscribe(result => {
          this.refreshItems('phytoTreatments',1500);
        });
        break;
      case 'delete-phytosanitaryTreatment':
        const deletedPhytosanitaryTreatment: PhytosanitaryTreatment = param;
        const dialogRef_deletePhytosanitaryTreatment = this.dialog.open(DialogPhytosanitaryTreatmentsComponent, 
          {data: {phytosanitaryTreatment: deletedPhytosanitaryTreatment, optionDialog: option}});
        dialogRef_deletePhytosanitaryTreatment.afterClosed().subscribe(result => {
          this.refreshItems('phytoTreatments',1500);
        });
        break;
      case 'new-storagePremise':
        const dialogRef_newStoragePremise = this.dialog.open(DialogOtherTreatmentsComponent,
          {data: {treatment: null, optionDialog: option}});
        dialogRef_newStoragePremise.afterClosed().subscribe(result => {
          this.refreshItems('storagePremises',1500);
        });
        break;
      case 'new-storedProduct':
        const dialogRef_newStoredProduct = this.dialog.open(DialogOtherTreatmentsComponent,
          {data: {treatment: null, optionDialog: option}});
        dialogRef_newStoredProduct.afterClosed().subscribe(result => {
          this.refreshItems('storedProducts',1500);
        });
        break;
      case 'new-ownTransport':
        const dialogRef_newOwnTransport = this.dialog.open(DialogOtherTreatmentsComponent,
          {data: {treatment: null, optionDialog: option}});
        dialogRef_newOwnTransport.afterClosed().subscribe(result => {
          this.refreshItems('ownTransports',1500);
        });
        break;
      case 'edit-storagePremise':
        const updatedStoragePremiseTreatment: OtherTreatment = param;
        const dialogRef_editStoragePremise = this.dialog.open(DialogOtherTreatmentsComponent,
          {data: {treatment: updatedStoragePremiseTreatment, optionDialog: option}});
        dialogRef_editStoragePremise.afterClosed().subscribe(result => {
          this.refreshItems('storagePremises',1500);
        });
        break;
      case 'edit-storedProduct':
        const updatedStoredProductTreatment: OtherTreatment = param;
        const dialogRef_editStoredProduct = this.dialog.open(DialogOtherTreatmentsComponent,
          {data: {treatment: updatedStoredProductTreatment, optionDialog: option}});
        dialogRef_editStoredProduct.afterClosed().subscribe(result => {
          this.refreshItems('storedProducts',1500);
        });
        break;
      case 'edit-ownTransport':
        const updatedOwnProductTreatment: OtherTreatment = param;
        const dialogRef_editOwnTransport = this.dialog.open(DialogOtherTreatmentsComponent,
          {data: {treatment: updatedOwnProductTreatment, optionDialog: option}});
        dialogRef_editOwnTransport.afterClosed().subscribe(result => {
          this.refreshItems('ownTransports',1500);
        });
        break;
      case 'delete-storagePremise':
        const deletedStoragePremiseTreatment: OtherTreatment = param;
        const dialogRef_deleteStoragePremise = this.dialog.open(DialogOtherTreatmentsComponent,
          {data: {treatment: deletedStoragePremiseTreatment, optionDialog: option}});
        dialogRef_deleteStoragePremise.afterClosed().subscribe(result => {
          this.refreshItems('storagePremises',1500);
        });
        break;
      case 'delete-storedProduct':
        const deletedStoredProductTreatment: OtherTreatment = param;
        const dialogRef_deleteStoredProduct = this.dialog.open(DialogOtherTreatmentsComponent,
          {data: {treatment: deletedStoredProductTreatment, optionDialog: option}});
        dialogRef_deleteStoredProduct.afterClosed().subscribe(result => {
          this.refreshItems('storedProducts',1500);
        });
        break;
      case 'delete-ownTransport':
        const deletedOwnProductTreatment: OtherTreatment = param;
        const dialogRef_deleteOwnTransport = this.dialog.open(DialogOtherTreatmentsComponent,
          {data: {treatment: deletedOwnProductTreatment, optionDialog: option}});
        dialogRef_deleteOwnTransport.afterClosed().subscribe(result => {
          this.refreshItems('ownTransports',1500);
        });
        break;
    }
  }

  async refreshItems(table: string, time: number, showMsg: boolean = false) {
    await timer(time).pipe(take(1)).toPromise();
    switch(table) {
      case 'phytoTreatments':
        this.noLoading = false;
        this.findAllPhytoTreatments();
        break;
      case 'storagePremises':
        this.noLoading = false;
        this.findOtherTreatmentByCategory('En local de almacenamiento');
        break;
      case 'storedProducts':
        this.noLoading = false;
        this.findOtherTreatmentByCategory('En productos almacenados');
        break;
      case 'ownTransports':
        this.noLoading = false;
        this.findOtherTreatmentByCategory('En medios de transporte propios');
        break;
      default: 
        break;
    }

    if(showMsg && !this.noLoading) {
      this.snackBar.open('¡Datos actualizados correctamente!', 'OK', 
      { duration: 2000, horizontalPosition: 'end' } );
    }

  }

  async findAllPhytoTreatments(){
    try {
      this.dataSourcePhytoTreatments.data = await this.phytoTreatmentService.findAll().toPromise();
      this.dataSourcePhytoTreatments.paginator = this.paginator;
    } catch {
      this.noLoading = true;
    }
  }

  async findOtherTreatmentByCategory(category: string){
    try {
      this.dataSourceOtherTreatments.data = await this.otherTreatmentService.findByCategory(category).toPromise();
      this.dataSourceOtherTreatments.paginator = this.paginator;
    } catch {
      this.noLoading = true;
    }
  }

}
