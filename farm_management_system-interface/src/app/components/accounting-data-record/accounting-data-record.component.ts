import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { timer } from 'rxjs';
import { take } from 'rxjs/operators';
import { Fertilizer } from 'src/app/models/fertilizer';
import { MarketedHarvest } from 'src/app/models/marketedHarvest';
import { PhytosanitaryProduct } from 'src/app/models/phytosanitaryProduct';
import { Sapling } from 'src/app/models/sapling';
import { SeedPlant } from 'src/app/models/seedPlant';
import { FertilizersService } from 'src/app/services/fertilizer.service';
import { MarketedHarvestService } from 'src/app/services/marketed-harvest.service';
import { PhytosanitaryProductService } from 'src/app/services/phytosanitary-product.service';
import { SaplingService } from 'src/app/services/sapling.service';
import { SeedPlantService } from 'src/app/services/seed-plant.service';
import { DialogFertilizersComponent } from './dialogs/dialog-fertilizers/dialog-fertilizers.component';
import { DialogMarketedHarvestsComponent } from './dialogs/dialog-marketed-harvests/dialog-marketed-harvests.component';
import { DialogPhytosanitayProductsComponent } from './dialogs/dialog-phytosanitay-products/dialog-phytosanitay-products.component';
import { DialogSaplingsComponent } from './dialogs/dialog-saplings/dialog-saplings.component';
import { DialogSeedsPlantsComponent } from './dialogs/dialog-seeds-plants/dialog-seeds-plants.component';


@Component({
  selector: 'app-accounting-data-record',
  templateUrl: './accounting-data-record.component.html',
  styleUrls: ['./accounting-data-record.component.scss']
})
export class AccountingDataRecordComponent implements OnInit {

  displayedColumnsSeedPlant: string[] = ['cropType', 'crop', 'variety', 'lot', 'category', 'amount', 'datePurchase', 
  'invoiceNumberPurchase', 'amountPurchase', 'providerNif', 'providerFullName', 'options'];
  displayedColumnsSapling: string[] = ['cropType', 'crop', 'variety', 'pattern', 'lot', 'datePurchase', 
  'invoiceNumberPurchase', 'amountPurchase', 'providerNif', 'providerFullName', 'options'];
  displayedColumnsFertilizer: string[] = ['product', 'productType', 'datePurchase', 
  'invoiceNumberPurchase', 'amountPurchase', 'providerNif', 'providerFullName', 'options'];
  displayedColumnsPhytosanitaryProduct: string[] = ['tradeName', 'registryNumber', 'activeMatter', 'lot',
  'datePurchase', 'invoiceNumberPurchase', 'amountPurchase', 'providerNif', 'providerFullName', 'options'];
  displayedColumnsMarketedHarvest: string[] = ['cropType', 'crop', 'qualification', 'lot', 'date', 
  'invoiceNumber', 'amount', 'handoutNumber', 'purchaserNif', 'purchaserFullName', 'options'];
  
  dataSourceSeedPlant = new MatTableDataSource<SeedPlant>();
  dataSourceSapling = new MatTableDataSource<Sapling>();
  dataSourceFertilizers= new MatTableDataSource<Fertilizer>();
  dataSourcePhytosanitaryProducts= new MatTableDataSource<PhytosanitaryProduct>();
  dataSourceMarketedHarvest = new MatTableDataSource<MarketedHarvest>();

  noLoading = false;

  @ViewChild(MatPaginator, {static: true}) paginator!: MatPaginator;
  constructor(
    private seedPlantService: SeedPlantService,
    private saplingService: SaplingService,
    private fertilizerService: FertilizersService,
    private phytosanitaryProductService: PhytosanitaryProductService,
    private marketedHarvestService: MarketedHarvestService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(){
    this.refreshItems('seedsPlants',1);
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    switch(tabChangeEvent.index) {
      case 0:
        this.refreshItems('seedsPlants',0);
        break;      
      case 1:
        this.refreshItems('saplings',0);
        break;
      case 2:
        this.refreshItems('fertilizers',0);
        break;
      case 3:
        this.refreshItems('phytosanitaryProducts',0);
        break;
      case 4:
        this.refreshItems('marketedHarvests',0);
        break;
    }
    
  }

  openDialog(option: string, param: any) {
    switch(option) {
      case 'new-seedPlant':
        const dialogRef_newSeedPlant = this.dialog.open(DialogSeedsPlantsComponent,
          {data: {seedPlant: null, optionDialog: option}});
        dialogRef_newSeedPlant.afterClosed().subscribe(result => {
          this.refreshItems('seedsPlants',1500);
        });
        break;
      case 'edit-seedPlant':
        const updatedSeedPlant: SeedPlant = param;
        const dialogRef_editSeedPlant = this.dialog.open(DialogSeedsPlantsComponent, 
          {data: {seedPlant: updatedSeedPlant, optionDialog: option}});
        dialogRef_editSeedPlant.afterClosed().subscribe(result => {
          this.refreshItems('seedsPlants',1500);
        });
        break;
      case 'delete-seedPlant':
        const deletedSeedPlant: SeedPlant = param;
        const dialogRef_deleteSeedPlant = this.dialog.open(DialogSeedsPlantsComponent, 
          {data: {seedPlant: deletedSeedPlant, optionDialog: option}});
        dialogRef_deleteSeedPlant.afterClosed().subscribe(result => {
          this.refreshItems('seedsPlants',1500);
        });
        break;
      case 'new-sapling':
        const dialogRef_newSapling = this.dialog.open(DialogSaplingsComponent,
          {data: {sapling: null, optionDialog: option}});
        dialogRef_newSapling.afterClosed().subscribe(result => {
          this.refreshItems('saplings',1500);
        });
        break;
      case 'edit-sapling':
        const updatedSapling: Sapling = param;
        const dialogRef_editSapling = this.dialog.open(DialogSaplingsComponent, 
          {data: {sapling: updatedSapling, optionDialog: option}});
        dialogRef_editSapling.afterClosed().subscribe(result => {
          this.refreshItems('saplings',1500);
        });
        break;
      case 'delete-sapling':
        const deletedSapling: Sapling = param;
        const dialogRef_deleteSapling = this.dialog.open(DialogSaplingsComponent, 
          {data: {sapling: deletedSapling, optionDialog: option}});
        dialogRef_deleteSapling.afterClosed().subscribe(result => {
          this.refreshItems('saplings',1500);
        });
        break;
      case 'new-fertilizer':
        const dialogRef_newFertilizer = this.dialog.open(DialogFertilizersComponent,
          {data: {fertilizer: null, optionDialog: option}});
        dialogRef_newFertilizer.afterClosed().subscribe(result => {
          this.refreshItems('fertilizers',1500);
        });
        break;
      case 'edit-fertilizer':
        const updatedFertilizer: Fertilizer = param;
        const dialogRef_editFertilizer = this.dialog.open(DialogFertilizersComponent, 
          {data: {fertilizer: updatedFertilizer, optionDialog: option}});
        dialogRef_editFertilizer.afterClosed().subscribe(result => {
          this.refreshItems('fertilizers',1500);
        });
        break;
      case 'delete-fertilizer':
        const deletedFertilizer: Fertilizer = param;
        const dialogRef_deleteFertilizer = this.dialog.open(DialogFertilizersComponent, 
          {data: {fertilizer: deletedFertilizer, optionDialog: option}});
        dialogRef_deleteFertilizer.afterClosed().subscribe(result => {
          this.refreshItems('fertilizers',1500);
        });
        break;
      case 'new-phytosanitaryProduct':
        const dialogRef_newPhytosanitaryProduct = this.dialog.open(DialogPhytosanitayProductsComponent,
          {data: {phytosanitaryProduct: null, optionDialog: option}});
        dialogRef_newPhytosanitaryProduct.afterClosed().subscribe(result => {
          this.refreshItems('phytosanitaryProducts',1500);
        });
        break;
      case 'edit-phytosanitaryProduct':
        const updatedPhytosanitaryProduct: PhytosanitaryProduct = param;
        const dialogRef_editPhytosanitaryProduct = this.dialog.open(DialogPhytosanitayProductsComponent, 
          {data: {phytosanitaryProduct: updatedPhytosanitaryProduct, optionDialog: option}});
        dialogRef_editPhytosanitaryProduct.afterClosed().subscribe(result => {
          this.refreshItems('phytosanitaryProducts',1500);
        });
        break;
      case 'delete-phytosanitaryProduct':
        const deletedPhytosanitaryProduct: PhytosanitaryProduct = param;
        const dialogRef_deletePhytosanitaryProduct = this.dialog.open(DialogPhytosanitayProductsComponent, 
          {data: {phytosanitaryProduct: deletedPhytosanitaryProduct, optionDialog: option}});
        dialogRef_deletePhytosanitaryProduct.afterClosed().subscribe(result => {
          this.refreshItems('phytosanitaryProducts',1500);
        });
        break;
      case 'new-marketedHarvest':
        const dialogRef_newMarketedHarvest = this.dialog.open(DialogMarketedHarvestsComponent,
          {data: {marketedHarvest: null, optionDialog: option}});
        dialogRef_newMarketedHarvest.afterClosed().subscribe(result => {
          this.refreshItems('marketedHarvests',1500);
        });
        break;
      case 'edit-marketedHarvest':
        const updatedMarketedHarvest: MarketedHarvest = param;
        const dialogRef_editMarketedHarvest = this.dialog.open(DialogMarketedHarvestsComponent, 
          {data: {marketedHarvest: updatedMarketedHarvest, optionDialog: option}});
        dialogRef_editMarketedHarvest.afterClosed().subscribe(result => {
          this.refreshItems('marketedHarvests',1500);
        });
        break;
      case 'delete-marketedHarvest':
        const deletedMarketedHarvest: MarketedHarvest = param;
        const dialogRef_deleteMarketedHarvest = this.dialog.open(DialogMarketedHarvestsComponent, 
          {data: {marketedHarvest: deletedMarketedHarvest, optionDialog: option}});
        dialogRef_deleteMarketedHarvest.afterClosed().subscribe(result => {
          this.refreshItems('marketedHarvests',1500);
        });
          break;
      default:
        break;
    }
  }

  async refreshItems(table: string, time: number, showMsg: boolean = false) {
    await timer(time).pipe(take(1)).toPromise();
    switch(table) {
      case 'seedsPlants':
        this.noLoading = false;
        this.findAllSeedsPlants();
        break;
      case 'saplings':
        this.noLoading = false;
        this.findAllSaplings();
        break;
      case 'fertilizers':
        this.noLoading = false;
        this.findAllFertilizers();
        break;
      case 'phytosanitaryProducts':
        this.noLoading = false;
        this.findAllPhytosanitaryProducts();
        break;
      case 'marketedHarvests':
        this.noLoading = false;
        this.findAllMarketedHarvests();
        break;
      default: 
        break;
    }

    if(showMsg && !this.noLoading) {
      this.snackBar.open('¡Datos actualizados correctamente!', 'OK', 
      { duration: 2000, horizontalPosition: 'end' } );
    }

  }

  async findAllSeedsPlants(){
    try {
      this.dataSourceSeedPlant.data = await this.seedPlantService.findAll().toPromise();
      this.dataSourceSeedPlant.paginator = this.paginator;
    } catch {
      this.noLoading = true;
    }
  }

  async findAllSaplings(){
    try {
      this.dataSourceSapling.data = await this.saplingService.findAll().toPromise();
      this.dataSourceSapling.paginator = this.paginator;
    } catch {
      this.noLoading = true;
    }
  }

  async findAllFertilizers(){
    try {
      this.dataSourceFertilizers.data = await this.fertilizerService.findAll().toPromise();
      this.dataSourceFertilizers.paginator = this.paginator;
    } catch {
      this.noLoading = true;
    }
  }

  async findAllPhytosanitaryProducts(){
    try {
      this.dataSourcePhytosanitaryProducts.data = await this.phytosanitaryProductService.findAll().toPromise();
      this.dataSourcePhytosanitaryProducts.paginator = this.paginator;
    } catch {
      this.noLoading = true;
    }
  }

  async findAllMarketedHarvests(){
    try {
      this.dataSourceMarketedHarvest.data = await this.marketedHarvestService.findAll().toPromise();
      this.dataSourceMarketedHarvest.paginator = this.paginator;
    } catch {
      this.noLoading = true;
    }
  }

}
