import { SelectionModel } from '@angular/cdk/collections';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { MarketedHarvest } from 'src/app/models/marketedHarvest';
import { Plot } from 'src/app/models/plot';
import { MarketedHarvestService } from 'src/app/services/marketed-harvest.service';
import { OwnerService } from 'src/app/services/owner.service';
import { PlotService } from 'src/app/services/plot.service';

@Component({
  selector: 'app-dialog-marketed-harvests',
  templateUrl: './dialog-marketed-harvests.component.html',
  styleUrls: ['./dialog-marketed-harvests.component.scss']
})
export class DialogMarketedHarvestsComponent implements OnInit {

  addMarketedHarvest = false;
  editMarketedHarvest = false;
  deleteMarketedHarvest = false;

  public formNewMarketedHarvest!: FormGroup;
  public formEditMarketedHarvest!: FormGroup;
  public formDeleteMarketedHarvest!: FormGroup;

  formNewMarketedHarvestError = false;
  formEditMarketedHarvestError = false;
  plotSelectedError = false;
  nifOwnerError = false;

  displayedColumnsPlots: string[] = ['select', 'crop', 'provinceNumber', 'cityNumber', 
  'addition' , 'zone', 'polygon', 'plot', 'enclosure'];
  dataSourceAllPlots = new MatTableDataSource<Plot>();
  selection = new SelectionModel<Plot>(true, []);
  
  @ViewChild(MatPaginator, {static: true}) paginator!: MatPaginator;
  constructor(
    private marketedHarvestService: MarketedHarvestService,
    private plotService: PlotService,
    private ownerService: OwnerService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    @ Inject(MAT_DIALOG_DATA) public data: {marketedHarvest: MarketedHarvest, optionDialog: string},
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.initForms(this.data.optionDialog);
  }

  async initForms(option: string) {
    switch(option) {
      case 'new-marketedHarvest':
        this.addMarketedHarvest = true;
        this.formNewMarketedHarvest = this.formBuilder.group({
          cropType: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(15)]),
          cropTypeNumber: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(4)]),
          crop: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(15)]),
          cropNumber: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(4)]),
          qualification: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(2)]),
          lot: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(10)]),
          comments: new FormControl(''),
          date: new FormControl('', [Validators.required, Validators.pattern(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/i)]),
          invoiceNumber: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(20)]),
          amount: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(10)]),
          handoutNumber: new FormControl('', Validators.maxLength(10)),
          purchaserNif: new FormControl('', [Validators.required, Validators.minLength(9), Validators.maxLength(9), Validators.pattern(/^[0-9]{8}[ABCDEFGHIJKLMNOPQRSTUVWXYZ]$/i)]),
          purchaserFullName: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(30)]),
          purchaserAddress: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(40)]),
          province: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(15)]),
          provinceNumber: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(3)]),
          city: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(15)]),
          cityNumber: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(3)])
        });
        this.dataSourceAllPlots.data = await this.plotService.findAll().toPromise();
        break;
      case 'edit-marketedHarvest':
        this.editMarketedHarvest = true;
        const updatedMarketedHarvest = this.data.marketedHarvest;
        this.formEditMarketedHarvest = this.formBuilder.group({
          cropType: new FormControl(updatedMarketedHarvest.cropType),
          cropTypeNumber: new FormControl(updatedMarketedHarvest.cropTypeNumber),
          crop: new FormControl(updatedMarketedHarvest.crop),
          cropNumber: new FormControl(updatedMarketedHarvest.cropNumber),
          qualification: new FormControl(updatedMarketedHarvest.qualification, [Validators.required, Validators.minLength(1), Validators.maxLength(2)]),
          lot: new FormControl(updatedMarketedHarvest.lot),
          comments: new FormControl(updatedMarketedHarvest.comments),
          date: new FormControl(updatedMarketedHarvest.date),
          invoiceNumber: new FormControl(updatedMarketedHarvest.invoiceNumber),
          amount: new FormControl(updatedMarketedHarvest.amount, [Validators.required, Validators.minLength(1), Validators.maxLength(10)]),
          handoutNumber: new FormControl(updatedMarketedHarvest.handoutNumber),
          purchaserNif: new FormControl(updatedMarketedHarvest.purchaserNif),
          purchaserFullName: new FormControl(updatedMarketedHarvest.purchaserFullName),
          purchaserAddress: new FormControl(updatedMarketedHarvest.purchaserAddress, [Validators.required, Validators.minLength(1), Validators.maxLength(40)]),
          province: new FormControl(updatedMarketedHarvest.province),
          provinceNumber: new FormControl(updatedMarketedHarvest.provinceNumber),
          city: new FormControl(updatedMarketedHarvest.city),
          cityNumber: new FormControl(updatedMarketedHarvest.cityNumber)
        });
        break;
      case 'delete-marketedHarvest':
        this.deleteMarketedHarvest = true;
        const deletedMarketedHarvest = this.data.marketedHarvest;
        this.formDeleteMarketedHarvest = this.formBuilder.group({
          cropType: new FormControl(deletedMarketedHarvest.cropType),
          cropTypeNumber: new FormControl(deletedMarketedHarvest.cropTypeNumber),
          crop: new FormControl(deletedMarketedHarvest.crop),
          cropNumber: new FormControl(deletedMarketedHarvest.cropNumber),
          qualification: new FormControl(deletedMarketedHarvest.qualification),
          lot: new FormControl(deletedMarketedHarvest.lot),
          comments: new FormControl(deletedMarketedHarvest.comments),
          date: new FormControl(deletedMarketedHarvest.date),
          invoiceNumber: new FormControl(deletedMarketedHarvest.invoiceNumber),
          amount: new FormControl(deletedMarketedHarvest.amount),
          handoutNumber: new FormControl(deletedMarketedHarvest.handoutNumber),
          purchaserNif: new FormControl(deletedMarketedHarvest.purchaserNif),
          purchaserFullName: new FormControl(deletedMarketedHarvest.purchaserFullName),
          purchaserAddress: new FormControl(deletedMarketedHarvest.purchaserAddress),
          province: new FormControl(deletedMarketedHarvest.province),
          provinceNumber: new FormControl(deletedMarketedHarvest.provinceNumber),
          city: new FormControl(deletedMarketedHarvest.city),
          cityNumber: new FormControl(deletedMarketedHarvest.cityNumber)
        });
        break;
      default:
        break;
    }
  }

  async saveMarketedHarvestDB() {
    this.formNewMarketedHarvest.markAllAsTouched();
    try {
      if(this.formNewMarketedHarvest.invalid) {
        this.formNewMarketedHarvestError = true;
      } else {
        if(this.selection.isEmpty()) {
          this.plotSelectedError = true;
        } else {
          let marketedHarvest: MarketedHarvest = this.formNewMarketedHarvest.value;
          const nifOwner = marketedHarvest.purchaserNif as string;
          const owners = await this.ownerService.findByNif(nifOwner).toPromise();
          if(owners.length < 1) {
            this.nifOwnerError = true;
            this.formNewMarketedHarvestError = true;
          } else {
            marketedHarvest.plots = this.selection.selected;
            await this.marketedHarvestService.insert(marketedHarvest).toPromise();
            this.snackBar.open('AÑADIDO: Registro de venta de cosecha comercializada.', 'OK', 
            { duration: 4000, horizontalPosition: 'end' } );
            this.dialog.closeAll();
          }
        }
      }
    } catch {
      this.snackBar.open('ERROR al añadir.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
    } 
  }

  async updateMarketedHarvestDB() {
    this.formEditMarketedHarvest.markAllAsTouched();
    if (this.formEditMarketedHarvest.invalid) {
      this.formEditMarketedHarvestError = true;
    } else {
      try {
        let marketedHarvest: MarketedHarvest = this.formEditMarketedHarvest.value;
        marketedHarvest.plots = this.data.marketedHarvest.plots;
        await this.marketedHarvestService.update(marketedHarvest.invoiceNumber, marketedHarvest).toPromise();
        this.snackBar.open('ACTUALIZADO: Registro de venta de cosecha comercializada.', 'OK', 
        { duration: 4000, horizontalPosition: 'end' } );
      } catch {
        this.snackBar.open('ERROR al actualizar.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
      } finally {
        this.dialog.closeAll();
      }
    }
  }

  async deleteMarketedHarvestDB() {
    try {
      await this.marketedHarvestService.delete(this.data.marketedHarvest.invoiceNumber).toPromise();
      this.snackBar.open('ELIMINADO: Registro de venta de cosecha comercializada.', 'OK', 
      { duration: 4000, horizontalPosition: 'end' } );
    } catch {
      this.snackBar.open('ERROR al eliminar.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
    } finally {
      this.dialog.closeAll(); 
    }
  }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSourceAllPlots.data.length;
      return numSelected === numRows;
    }
  
    /** Selects all rows if they are not all selected; otherwise clear selection. */
    masterToggle() {
      this.isAllSelected() ?
          this.selection.clear() :
          this.dataSourceAllPlots.data.forEach(row => this.selection.select(row));
    }
  
    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: Plot): string {
      if (!row) {
        return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
      }
      return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.orderNumber}`;
    }

}
