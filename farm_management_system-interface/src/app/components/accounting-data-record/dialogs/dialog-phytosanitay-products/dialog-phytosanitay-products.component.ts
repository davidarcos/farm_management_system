import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PhytosanitaryProduct } from 'src/app/models/phytosanitaryProduct';
import { OwnerService } from 'src/app/services/owner.service';
import { PhytosanitaryProductService } from 'src/app/services/phytosanitary-product.service';

@Component({
  selector: 'app-dialog-phytosanitay-products',
  templateUrl: './dialog-phytosanitay-products.component.html',
  styleUrls: ['./dialog-phytosanitay-products.component.scss']
})
export class DialogPhytosanitayProductsComponent implements OnInit {

  addPhytosanitaryProduct = false;
  editPhytosanitaryProduct = false;
  deletePhytosanitaryProduct = false;

  public formNewPhytosanitaryProduct!: FormGroup;
  public formEditPhytosanitaryProduct!: FormGroup;
  public formDeletePhytosanitaryProduct!: FormGroup;

  formNewPhytosanitaryProductError = false;
  formEditPhytosanitaryProductError = false;
  nifOwnerError = false;

  constructor(
    private phytosanitaryProductService: PhytosanitaryProductService,
    private ownerService: OwnerService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    @ Inject(MAT_DIALOG_DATA) public data: {phytosanitaryProduct: PhytosanitaryProduct, optionDialog: string},
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.initForms(this.data.optionDialog);
  }

  async initForms(option: string) {
    switch(option) {
      case 'new-phytosanitaryProduct':
        this.addPhytosanitaryProduct = true;
        this.formNewPhytosanitaryProduct = this.formBuilder.group({
          tradeName: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(20)]),
          registryNumber: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(15)]),
          activeMatter: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(35)]),
          lot: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(10)]),
          datePurchase: new FormControl('', [Validators.required, Validators.pattern(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/i)]),
          invoiceNumberPurchase: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(20)]),
          amountPurchase: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(15)]),
          providerNif: new FormControl('', [Validators.required, Validators.minLength(9), Validators.maxLength(9), Validators.pattern(/^[0-9]{8}[ABCDEFGHIJKLMNOPQRSTUVWXYZ]$/i)]),
          providerFullName: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(30)])
        });
        break;
      case 'edit-phytosanitaryProduct':
        this.editPhytosanitaryProduct = true;
        const updatedPhytosanitaryProduct = this.data.phytosanitaryProduct;
        this.formEditPhytosanitaryProduct = this.formBuilder.group({
          tradeName: new FormControl(updatedPhytosanitaryProduct.tradeName),
          registryNumber: new FormControl(updatedPhytosanitaryProduct.registryNumber),
          activeMatter: new FormControl(updatedPhytosanitaryProduct.activeMatter),
          lot: new FormControl(updatedPhytosanitaryProduct.lot),
          datePurchase: new FormControl(updatedPhytosanitaryProduct.datePurchase),
          invoiceNumberPurchase: new FormControl(updatedPhytosanitaryProduct.invoiceNumberPurchase),
          amountPurchase: new FormControl(updatedPhytosanitaryProduct.amountPurchase, [Validators.required, Validators.minLength(1), Validators.maxLength(15)]),
          providerNif: new FormControl(updatedPhytosanitaryProduct.providerNif),
          providerFullName: new FormControl(updatedPhytosanitaryProduct.providerFullName)
        });
        break;
      case 'delete-phytosanitaryProduct':
        this.deletePhytosanitaryProduct = true;
        const deletedPhytosanitaryProduct = this.data.phytosanitaryProduct;
        this.formDeletePhytosanitaryProduct = this.formBuilder.group({
          tradeName: new FormControl(deletedPhytosanitaryProduct.tradeName),
          registryNumber: new FormControl(deletedPhytosanitaryProduct.registryNumber),
          activeMatter: new FormControl(deletedPhytosanitaryProduct.activeMatter),
          lot: new FormControl(deletedPhytosanitaryProduct.lot),
          datePurchase: new FormControl(deletedPhytosanitaryProduct.datePurchase),
          invoiceNumberPurchase: new FormControl(deletedPhytosanitaryProduct.invoiceNumberPurchase),
          amountPurchase: new FormControl(deletedPhytosanitaryProduct.amountPurchase),
          providerNif: new FormControl(deletedPhytosanitaryProduct.providerNif),
          providerFullName: new FormControl(deletedPhytosanitaryProduct.providerFullName)
        });
        break;
      default:
        break;
    }
  }

  async savePhytosanitaryProductDB() {
    this.formNewPhytosanitaryProduct.markAllAsTouched();
    try {
      if(this.formNewPhytosanitaryProduct.invalid) {
        this.formNewPhytosanitaryProductError = true;
      } else {
        const phytosanitaryProduct: PhytosanitaryProduct = this.formNewPhytosanitaryProduct.value;
        const nifOwner = phytosanitaryProduct.providerNif as string;
        const owners = await this.ownerService.findByNif(nifOwner).toPromise();
        if(owners.length < 1) {
          this.nifOwnerError = true;
          this.formNewPhytosanitaryProductError = true;
        } else {
          await this.phytosanitaryProductService.insert(phytosanitaryProduct).toPromise();
          this.snackBar.open('AÑADIDO: Registro de compra de productos fitosanitarios.', 'OK', 
          { duration: 4000, horizontalPosition: 'end' } );
          this.dialog.closeAll();
        }
      }
    } catch {
      this.snackBar.open('ERROR al añadir.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
    } 
  }

  async updatePhytosanitaryProductDB() {
    this.formEditPhytosanitaryProduct.markAllAsTouched();
    if (this.formEditPhytosanitaryProduct.invalid) {
      this.formEditPhytosanitaryProductError = true;
    } else {
      try {
        let phytosanitaryProduct: PhytosanitaryProduct = this.formEditPhytosanitaryProduct.value;
        await this.phytosanitaryProductService.update(phytosanitaryProduct.invoiceNumberPurchase, phytosanitaryProduct).toPromise();
        this.snackBar.open('ACTUALIZADO: Registro de productos fitosanitarios.', 'OK', 
          { duration: 4000, horizontalPosition: 'end' } );
      } catch {
        this.snackBar.open('ERROR al actualizar.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
      } finally {
        this.dialog.closeAll();
      }
    }
  }

  async deletePhytosanitaryProductDB() {
    try {
      await this.phytosanitaryProductService.delete(this.data.phytosanitaryProduct.invoiceNumberPurchase).toPromise();
      this.snackBar.open('ELIMINADO: Registro de productos fitosanitarios.', 'OK', 
      { duration: 4000, horizontalPosition: 'end' } );
    } catch {
      this.snackBar.open('ERROR al eliminar.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
    } finally {
      this.dialog.closeAll(); 
    }
  }

}
