import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Sapling } from 'src/app/models/sapling';
import { OwnerService } from 'src/app/services/owner.service';
import { SaplingService } from 'src/app/services/sapling.service';

@Component({
  selector: 'app-dialog-saplings',
  templateUrl: './dialog-saplings.component.html',
  styleUrls: ['./dialog-saplings.component.scss']
})
export class DialogSaplingsComponent implements OnInit {

  addSapling = false;
  editSapling = false;
  deleteSapling = false;

  public formNewSapling!: FormGroup;
  public formEditSapling!: FormGroup;
  public formDeleteSapling!: FormGroup;

  formNewSaplingError = false;
  formEditSaplingError = false;
  nifOwnerError = false;

  @ViewChild(MatPaginator, {static: true}) paginator!: MatPaginator;
  constructor(
    private saplingService: SaplingService,
    private ownerService: OwnerService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    @ Inject(MAT_DIALOG_DATA) public data: {sapling: Sapling, optionDialog: string},
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.initForms(this.data.optionDialog);
  }

  async initForms(option: string) {
    switch(option) {
      case 'new-sapling':
        this.addSapling = true;
        this.formNewSapling = this.formBuilder.group({
          cropType: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(15)]),
          cropTypeNumber: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(4)]),
          crop: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(15)]),
          cropNumber: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(4)]),
          variety: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(15)]),
          varietyNumber: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(6)]),
          pattern: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(20)]),
          lot: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(10)]),
          datePurchase: new FormControl('', [Validators.required, Validators.pattern(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/i)]),
          invoiceNumberPurchase: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(20)]),
          amountPurchase: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(15)]),
          providerNif: new FormControl('', [Validators.required, Validators.minLength(9), Validators.maxLength(9), Validators.pattern(/^[0-9]{8}[ABCDEFGHIJKLMNOPQRSTUVWXYZ]$/i)]),
          providerFullName: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(30)])
        });
        break;
      case 'edit-sapling':
        this.editSapling = true;
        const updatedSapling = this.data.sapling;
        this.formEditSapling = this.formBuilder.group({
          cropType: new FormControl(updatedSapling.cropType),
          cropTypeNumber: new FormControl(updatedSapling.cropTypeNumber),
          crop: new FormControl(updatedSapling.crop),
          cropNumber: new FormControl(updatedSapling.cropNumber),
          variety: new FormControl(updatedSapling.variety),
          varietyNumber: new FormControl(updatedSapling.varietyNumber),
          pattern: new FormControl(updatedSapling.pattern),
          lot: new FormControl(updatedSapling.lot),
          datePurchase: new FormControl(updatedSapling.datePurchase),
          invoiceNumberPurchase: new FormControl(updatedSapling.invoiceNumberPurchase),
          amountPurchase: new FormControl(updatedSapling.amountPurchase, [Validators.required, Validators.minLength(1), Validators.maxLength(15)]),
          providerNif: new FormControl(updatedSapling.providerNif),
          providerFullName: new FormControl(updatedSapling.providerFullName)
        });
        break;
      case 'delete-sapling':
        this.deleteSapling = true;
        const deletedSapling = this.data.sapling;
        this.formDeleteSapling = this.formBuilder.group({
          cropType: new FormControl(deletedSapling.cropType),
          cropTypeNumber: new FormControl(deletedSapling.cropTypeNumber),
          crop: new FormControl(deletedSapling.crop),
          cropNumber: new FormControl(deletedSapling.cropNumber),
          variety: new FormControl(deletedSapling.variety),
          varietyNumber: new FormControl(deletedSapling.varietyNumber),
          pattern: new FormControl(deletedSapling.pattern),
          lot: new FormControl(deletedSapling.lot),
          datePurchase: new FormControl(deletedSapling.datePurchase),
          invoiceNumberPurchase: new FormControl(deletedSapling.invoiceNumberPurchase),
          amountPurchase: new FormControl(deletedSapling.amountPurchase),
          providerNif: new FormControl(deletedSapling.providerNif),
          providerFullName: new FormControl(deletedSapling.providerFullName)
        });
        break;
      default:
        break;
    }
  }

  async saveSaplingDB() {
    this.formNewSapling.markAllAsTouched();
    try {
      if(this.formNewSapling.invalid) {
        this.formNewSaplingError = true;
      } else {
        const sapling: Sapling = this.formNewSapling.value;
        const nifOwner = sapling.providerNif as string;
        const owners = await this.ownerService.findByNif(nifOwner).toPromise();
        if(owners.length < 1) {
          this.nifOwnerError = true;
          this.formNewSaplingError = true;
        } else {
          await this.saplingService.insert(sapling).toPromise();
          this.snackBar.open('AÑADIDO: Registro de compra de platones.', 'OK', 
          { duration: 4000, horizontalPosition: 'end' } );
          this.dialog.closeAll();
        }
      }
    } catch {
      this.snackBar.open('ERROR al añadir.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
    } 
  }

  async updateSaplingDB() {
    this.formEditSapling.markAllAsTouched();
    if (this.formEditSapling.invalid) {
      this.formEditSaplingError = true;
    } else {
      try {
        let sapling: Sapling = this.formEditSapling.value;
        await this.saplingService.update(sapling.invoiceNumberPurchase, sapling).toPromise();
        this.snackBar.open('ACTUALIZADO: Registro de compra de plantones.', 'OK', 
          { duration: 4000, horizontalPosition: 'end' } );
      } catch {
        this.snackBar.open('ERROR al actualizar.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
      } finally {
        this.dialog.closeAll();
      }
    }
  }

  async deleteSaplingDB() {
    try {
      await this.saplingService.delete(this.data.sapling.invoiceNumberPurchase).toPromise();
      this.snackBar.open('ELIMINADO: Registro de compra de plantones.', 'OK', 
      { duration: 4000, horizontalPosition: 'end' } );
    } catch {
      this.snackBar.open('ERROR al eliminar.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
    } finally {
      this.dialog.closeAll(); 
    }
  }

}
