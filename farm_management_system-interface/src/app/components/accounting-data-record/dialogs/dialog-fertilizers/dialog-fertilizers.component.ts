import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Fertilizer } from 'src/app/models/fertilizer';
import { FertilizersService } from 'src/app/services/fertilizer.service';
import { OwnerService } from 'src/app/services/owner.service';

@Component({
  selector: 'app-dialog-fertilizers',
  templateUrl: './dialog-fertilizers.component.html',
  styleUrls: ['./dialog-fertilizers.component.scss']
})
export class DialogFertilizersComponent implements OnInit {

  addFertilizer = false;
  editFertilizer = false;
  deleteFertilizer = false;

  public formNewFertilizer!: FormGroup;
  public formEditFertilizer!: FormGroup;
  public formDeleteFertilizer!: FormGroup;

  formNewFertilizerError = false;
  formEditFertilizerError = false;
  nifOwnerError = false;

  @ViewChild(MatPaginator, {static: true}) paginator!: MatPaginator;
  constructor(
    private fertilizerService: FertilizersService,
    private ownerService: OwnerService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    @ Inject(MAT_DIALOG_DATA) public data: {fertilizer: Fertilizer, optionDialog: string},
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.initForms(this.data.optionDialog);
  }

  async initForms(option: string) {
    switch(option) {
      case 'new-fertilizer':
        this.addFertilizer = true;
        this.formNewFertilizer = this.formBuilder.group({
          product: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(25)]),
          productType: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(25)]),
          datePurchase: new FormControl('', [Validators.required, Validators.pattern(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/i)]),
          invoiceNumberPurchase: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(20)]),
          amountPurchase: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(15)]),
          providerNif: new FormControl('', [Validators.required, Validators.minLength(9), Validators.maxLength(9), Validators.pattern(/^[0-9]{8}[ABCDEFGHIJKLMNOPQRSTUVWXYZ]$/i)]),
          providerFullName: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(30)])
        });
        break;
      case 'edit-fertilizer':
        this.editFertilizer = true;
        const updatedFertilizer = this.data.fertilizer;
        this.formEditFertilizer = this.formBuilder.group({
          product: new FormControl(updatedFertilizer.product),
          productType: new FormControl(updatedFertilizer.productType),
          datePurchase: new FormControl(updatedFertilizer.datePurchase),
          invoiceNumberPurchase: new FormControl(updatedFertilizer.invoiceNumberPurchase),
          amountPurchase: new FormControl(updatedFertilizer.amountPurchase, [Validators.required, Validators.minLength(1), Validators.maxLength(15)]),
          providerNif: new FormControl(updatedFertilizer.providerNif),
          providerFullName: new FormControl(updatedFertilizer.providerFullName)
        });
        break;
      case 'delete-fertilizer':
        this.deleteFertilizer = true;
        const deletedFertilizer = this.data.fertilizer;
        this.formDeleteFertilizer = this.formBuilder.group({
          product: new FormControl(deletedFertilizer.product),
          productType: new FormControl(deletedFertilizer.productType),
          datePurchase: new FormControl(deletedFertilizer.datePurchase),
          invoiceNumberPurchase: new FormControl(deletedFertilizer.invoiceNumberPurchase),
          amountPurchase: new FormControl(deletedFertilizer.amountPurchase),
          providerNif: new FormControl(deletedFertilizer.providerNif),
          providerFullName: new FormControl(deletedFertilizer.providerFullName)
        });
        break;
      default:
        break;
    }
  }

  async saveFertilizerDB() {
    this.formNewFertilizer.markAllAsTouched();
    try {
      if(this.formNewFertilizer.invalid) {
        this.formNewFertilizerError = true;
      } else {
        const fertilizer: Fertilizer = this.formNewFertilizer.value;
        const nifOwner = fertilizer.providerNif as string;
        const owners = await this.ownerService.findByNif(nifOwner).toPromise();
        if(owners.length < 1) {
          this.nifOwnerError = true;
          this.formNewFertilizerError = true;
        } else {
          await this.fertilizerService.insert(fertilizer).toPromise();
          this.snackBar.open('AÑADIDO: Registro de compra de abonos y fertilizantes.', 'OK', 
          { duration: 4000, horizontalPosition: 'end' } );
          this.dialog.closeAll();
        }
      }
    } catch {
      this.snackBar.open('ERROR al añadir.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
    } 
  }

  async updateFertilizerDB() {
    this.formEditFertilizer.markAllAsTouched();
    if (this.formEditFertilizer.invalid) {
      this.formEditFertilizerError = true;
    } else {
      try {
        let fertilizer: Fertilizer = this.formEditFertilizer.value;
        await this.fertilizerService.update(fertilizer.invoiceNumberPurchase, fertilizer).toPromise();
        this.snackBar.open('ACTUALIZADO: Registro de compra de abonos y fertilizantes.', 'OK', 
          { duration: 4000, horizontalPosition: 'end' } );
      } catch {
        this.snackBar.open('ERROR al actualizar.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
      } finally {
        this.dialog.closeAll();
      }
    }
  }

  async deleteFertilizerDB() {
    try {
      await this.fertilizerService.delete(this.data.fertilizer.invoiceNumberPurchase).toPromise();
      this.snackBar.open('ELIMINADO: Registro de compra de abonos y fertilizantes.', 'OK', 
      { duration: 4000, horizontalPosition: 'end' } );
    } catch {
      this.snackBar.open('ERROR al eliminar.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
    } finally {
      this.dialog.closeAll(); 
    }
  }

}
