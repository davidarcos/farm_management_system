import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SeedPlant } from 'src/app/models/seedPlant';
import { OwnerService } from 'src/app/services/owner.service';
import { SeedPlantService } from 'src/app/services/seed-plant.service';

@Component({
  selector: 'app-dialogs-seeds-plants',
  templateUrl: './dialog-seeds-plants.component.html',
  styleUrls: ['./dialog-seeds-plants.component.scss']
})
export class DialogSeedsPlantsComponent implements OnInit {

  addSeedPlant = false;
  editSeedPlant = false;
  deleteSeedPlant = false;

  public formNewSeedPlant!: FormGroup;
  public formEditSeedPlant!: FormGroup;
  public formDeleteSeedPlant!: FormGroup;

  formNewSeedPlantError = false;
  formEditSeedPlantError = false;
  nifOwnerError = false;

  @ViewChild(MatPaginator, {static: true}) paginator!: MatPaginator;
  constructor(
    private seedPlantService: SeedPlantService,
    private ownerService: OwnerService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    @ Inject(MAT_DIALOG_DATA) public data: {seedPlant: SeedPlant, optionDialog: string},
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.initForms(this.data.optionDialog);
  }

  async initForms(option: string) {
    switch(option) {
      case 'new-seedPlant':
        this.addSeedPlant = true;
        this.formNewSeedPlant = this.formBuilder.group({
          cropType: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(15)]),
          cropTypeNumber: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(4)]),
          crop: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(15)]),
          cropNumber: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(4)]),
          variety: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(15)]),
          varietyNumber: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(6)]),
          category: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(15)]),
          lot: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(10)]),
          amount: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(10)]),
          datePurchase: new FormControl('', [Validators.required, Validators.pattern(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/i)]),
          invoiceNumberPurchase: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(20)]),
          amountPurchase: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(15)]),
          providerNif: new FormControl('', [Validators.required, Validators.minLength(9), Validators.maxLength(9), Validators.pattern(/^[0-9]{8}[ABCDEFGHIJKLMNOPQRSTUVWXYZ]$/i)]),
          providerFullName: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(30)])
        });
        break;
      case 'edit-seedPlant':
        this.editSeedPlant = true;
        const updatedSeedPlant = this.data.seedPlant;
        this.formEditSeedPlant = this.formBuilder.group({
          cropType: new FormControl(updatedSeedPlant.cropType),
          cropTypeNumber: new FormControl(updatedSeedPlant.cropTypeNumber),
          crop: new FormControl(updatedSeedPlant.crop),
          cropNumber: new FormControl(updatedSeedPlant.cropNumber),
          variety: new FormControl(updatedSeedPlant.variety),
          varietyNumber: new FormControl(updatedSeedPlant.varietyNumber),
          category: new FormControl(updatedSeedPlant.category, [Validators.required, Validators.minLength(1), Validators.maxLength(15)]),
          lot: new FormControl(updatedSeedPlant.lot, [Validators.required, Validators.minLength(1), Validators.maxLength(10)]),
          amount: new FormControl(updatedSeedPlant.amount, [Validators.required, Validators.minLength(1), Validators.maxLength(10)]),
          datePurchase: new FormControl(updatedSeedPlant.datePurchase),
          invoiceNumberPurchase: new FormControl(updatedSeedPlant.invoiceNumberPurchase),
          amountPurchase: new FormControl(updatedSeedPlant.amountPurchase, [Validators.required, Validators.minLength(1), Validators.maxLength(15)]),
          providerNif: new FormControl(updatedSeedPlant.providerNif, [Validators.required, Validators.minLength(9), Validators.maxLength(9), Validators.pattern(/^[0-9]{8}[ABCDEFGHIJKLMNOPQRSTUVWXYZ]$/i)]),
          providerFullName: new FormControl(updatedSeedPlant.providerFullName, [Validators.required, Validators.minLength(1), Validators.maxLength(30)])
        });
        break;
      case 'delete-seedPlant':
        this.deleteSeedPlant = true;
        const deletedSeedPlant = this.data.seedPlant;
        this.formDeleteSeedPlant = this.formBuilder.group({
          cropType: new FormControl(deletedSeedPlant.cropType),
          cropTypeNumber: new FormControl(deletedSeedPlant.cropTypeNumber),
          crop: new FormControl(deletedSeedPlant.crop),
          cropNumber: new FormControl(deletedSeedPlant.cropNumber),
          variety: new FormControl(deletedSeedPlant.variety),
          varietyNumber: new FormControl(deletedSeedPlant.varietyNumber),
          category: new FormControl(deletedSeedPlant.category),
          lot: new FormControl(deletedSeedPlant.lot),
          amount: new FormControl(deletedSeedPlant.amount),
          datePurchase: new FormControl(deletedSeedPlant.datePurchase),
          invoiceNumberPurchase: new FormControl(deletedSeedPlant.invoiceNumberPurchase),
          amountPurchase: new FormControl(deletedSeedPlant.amountPurchase),
          providerNif: new FormControl(deletedSeedPlant.providerNif),
          providerFullName: new FormControl(deletedSeedPlant.providerFullName)
        });
        break;
      default:
        break;
    }
  }

  async saveSeedPlantDB() {
    this.formNewSeedPlant.markAllAsTouched();
    try {
      if(this.formNewSeedPlant.invalid) {
        this.formNewSeedPlantError = true;
      } else {
        const seedPlant: SeedPlant = this.formNewSeedPlant.value;
        const nifOwner = seedPlant.providerNif as string;
        const owners = await this.ownerService.findByNif(nifOwner).toPromise();
        if(owners.length < 1) {
          this.nifOwnerError = true;
          this.formNewSeedPlantError = true;
        } else {
          await this.seedPlantService.insert(seedPlant).toPromise();
          this.snackBar.open('AÑADIDO: Registro de compra de una semilla o planta.', 'OK', 
          { duration: 4000, horizontalPosition: 'end' } );
          this.dialog.closeAll();
        }
      }
    } catch {
      this.snackBar.open('ERROR al añadir.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
    } 
  }

  async updateSeedPlantDB() {
    this.formEditSeedPlant.markAllAsTouched();
    if (this.formEditSeedPlant.invalid) {
      this.formEditSeedPlantError = true;
    } else {
      try {
        let seedPlant: SeedPlant = this.formEditSeedPlant.value;
        await this.seedPlantService.update(seedPlant.invoiceNumberPurchase, seedPlant).toPromise();
        this.snackBar.open('ACTUALIZADO: Registro de compra de una semilla o planta.', 'OK', 
          { duration: 4000, horizontalPosition: 'end' } );
      } catch {
        this.snackBar.open('ERROR al actualizar.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
      } finally {
        this.dialog.closeAll();
      }
    }
  }

  async deleteSeedPlantDB() {
    try {
      await this.seedPlantService.delete(this.data.seedPlant.invoiceNumberPurchase).toPromise();
      this.snackBar.open('ELIMINADO: Registro de compra de una semilla o planta.', 'OK', 
      { duration: 4000, horizontalPosition: 'end' } );
    } catch {
      this.snackBar.open('ERROR al eliminar.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
    } finally {
      this.dialog.closeAll(); 
    }
  }

}
