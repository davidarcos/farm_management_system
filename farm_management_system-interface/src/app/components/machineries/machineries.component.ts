import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { timer } from 'rxjs';
import { take } from 'rxjs/operators';
import { Machinery } from 'src/app/models/machinery';
import { MachineryService } from 'src/app/services/machinery.service';
import { DialogMachineryComponent } from './dialogs/dialog-machinery/dialog-machinery.component';

@Component({
  selector: 'app-machineries',
  templateUrl: './machineries.component.html',
  styleUrls: ['./machineries.component.scss']
})
export class MachineriesComponent implements OnInit {

  currentTab = 0;

  displayedColumnsMachineries: string[] = ['id', 'type', 'brand', 'model', 
  'romaNumber', 'romaDate1', 'romaDate', 'lastInscDate', 'resultInsc', 'options'];

  dataSourceMachineries = new MatTableDataSource<Machinery>();

  lengthMachineries = 0;

  noLoading = false;

  @ViewChild(MatPaginator, {static: true}) paginator!: MatPaginator;
  constructor(
    private machineryService: MachineryService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar
    ) {}

  ngOnInit(){
    this.refreshItems('farmMachineries',1);
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    switch(tabChangeEvent.index) {
      case 0:
        this.refreshItems('farmMachineries',0);
        this.currentTab = 0;
        break;      
      case 1:
        this.refreshItems('phytoMachineries',0);
        this.currentTab = 1;
        break;
      case 2:
        this.refreshItems('fertilizerMachineries',0);
        this.currentTab = 2;
        break;
    }
  }

  openDialog(option: string, param: any) {
    switch(option) {
      case 'new-farmMachinery':
        const dialogRef_newFarmMachineries = this.dialog.open(DialogMachineryComponent,
          {data: {machinery: null, optionDialog: option}});
        dialogRef_newFarmMachineries.afterClosed().subscribe(result => {
          this.refreshItems('farmMachineries',1500);
        });
        break;
      case 'new-phytoMachinery':
        const dialogRef_newPhytoMachineries = this.dialog.open(DialogMachineryComponent,
          {data: {machinery: null, optionDialog: option}});
        dialogRef_newPhytoMachineries.afterClosed().subscribe(result => {
          this.refreshItems('phytoMachineries',1500);
        });
        break;
      case 'new-fertilizerMachinery':
        const dialogRef_newFertilizerMachineries = this.dialog.open(DialogMachineryComponent,
          {data: {machinery: null, optionDialog: option}});
        dialogRef_newFertilizerMachineries.afterClosed().subscribe(result => {
          this.refreshItems('fertilizerMachineries',1500);
        });
        break;
      case 'edit-machinery':
        const updatedMachinery: Machinery = param;
        const dialogRef_editMachinery = this.dialog.open(DialogMachineryComponent, 
          {data: {machinery: updatedMachinery, optionDialog: option}});
        dialogRef_editMachinery.afterClosed().subscribe(result => {
          if (this.currentTab === 0) this.refreshItems('farmMachineries',1500);
          if (this.currentTab === 1) this.refreshItems('phytoMachineries',1500);
          if (this.currentTab === 2) this.refreshItems('fertilizerMachineries',1500);
        });
        break;
      case 'delete-machinery':
        const deletedMachinery: Machinery = param;
        const dialogRef_deleteMachinery = this.dialog.open(DialogMachineryComponent, 
          {data: {machinery: deletedMachinery, optionDialog: option}});
        dialogRef_deleteMachinery.afterClosed().subscribe(result => {
          if (this.currentTab === 0) this.refreshItems('farmMachineries',1500);
          if (this.currentTab === 1) this.refreshItems('phytoMachineries',1500);
          if (this.currentTab === 2) this.refreshItems('fertilizerMachineries',1500);
        });
        break;
    }
  }

  async refreshItems(table: string, time: number, showMsg: boolean = false) {
    await timer(time).pipe(take(1)).toPromise();
    switch(table) {
      case 'farmMachineries':
        this.noLoading = false;
        this.findByCategory('Maquinaria de la explotación');
        break;
      case 'phytoMachineries':
        this.noLoading = false;
        this.findByCategory('Equipos de aplicación de productos fitosantiarios');
        break;
      case 'fertilizerMachineries':
        this.noLoading = false;
        this.findByCategory('Equipos de distribución de fertilizantes');
        break;
      default: 
        break;
    }

    if(showMsg && !this.noLoading) {
      this.snackBar.open('¡Datos actualizados correctamente!', 'OK', 
      { duration: 2000, horizontalPosition: 'end' } );
    }

  }

  async findByCategory(category: string){
    try {
      this.dataSourceMachineries.data = await this.machineryService.findByCategory(category).toPromise();
      this.dataSourceMachineries.paginator = this.paginator;
    } catch {
      this.noLoading = true;
    }
  }

}
