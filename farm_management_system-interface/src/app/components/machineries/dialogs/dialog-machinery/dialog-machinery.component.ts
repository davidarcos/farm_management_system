import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Machinery } from 'src/app/models/machinery';
import { MachineryService } from 'src/app/services/machinery.service';

@Component({
  selector: 'app-dialog-machinery',
  templateUrl: './dialog-machinery.component.html',
  styleUrls: ['./dialog-machinery.component.scss']
})
export class DialogMachineryComponent implements OnInit {

  addMachinery = false;
  editMachinery = false;
  deleteMachinery = false;

  public formNewMachinery!: FormGroup;
  public formEditMachinery!: FormGroup;
  public formDeleteMachinery!: FormGroup;

  formNewMachineryError = false;
  formEditMachineryError = false;

  displayedColumnsMachineries: string[] = ['id', 'type', 'brand', 'model', 
  'romaNumber', 'romaDate1', 'romaDate', 'lastInscDate', 'resultInsc', 'options'];
  dataSourceMachineries = new MatTableDataSource<Machinery>();

  @ViewChild(MatPaginator, {static: true}) paginator!: MatPaginator;
  constructor(
    private machineryService: MachineryService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    @ Inject(MAT_DIALOG_DATA) public data: {machinery: Machinery, optionDialog: string},
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.initForms(this.data.optionDialog);
  }

  async initForms(option: string) {
    switch(option) {
      case 'new-farmMachinery':
        this.addMachinery = true;
        this.formNewMachinery = this.formBuilder.group({
          id: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(20)]),
          category: new FormControl('Maquinaria de la explotación'),
          type: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(30)]),
          brand: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(30)]),
          model: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(30)]),
          romaNumber: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(20)]),
          romaDate1: new FormControl('', [Validators.required, Validators.pattern(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/i)]),
          romaDate: new FormControl('', [Validators.required, Validators.pattern(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/i)]),
          lastInscDate: new FormControl('',[Validators.required, Validators.pattern(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/i)]),
          resultInsc: new FormControl('',[Validators.required])
        });
        break;
      case 'new-phytoMachinery':
        this.addMachinery = true;
        this.formNewMachinery = this.formBuilder.group({
          id: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(20)]),
          category: new FormControl('Equipos de aplicación de productos fitosantiarios'),
          type: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(30)]),
          brand: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(30)]),
          model: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(30)]),
          romaNumber: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(20)]),
          romaDate1: new FormControl('', [Validators.required, Validators.pattern(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/i)]),
          romaDate: new FormControl('', [Validators.required, Validators.pattern(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/i)]),
          lastInscDate: new FormControl('',[Validators.required, Validators.pattern(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/i)]),
          resultInsc: new FormControl('',[Validators.required])
        });
        break;
      case 'new-fertilizerMachinery':
        this.addMachinery = true;
        this.formNewMachinery = this.formBuilder.group({
          id: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(20)]),
          category: new FormControl('Equipos de distribución de fertilizantes'),
          type: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(30)]),
          brand: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(30)]),
          model: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(30)]),
          romaNumber: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(20)]),
          romaDate1: new FormControl('', [Validators.required, Validators.pattern(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/i)]),
          romaDate: new FormControl('', [Validators.required, Validators.pattern(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/i)]),
          lastInscDate: new FormControl('',[Validators.required, Validators.pattern(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/i)]),
          resultInsc: new FormControl('',[Validators.required])
        });
        break;
      case 'edit-machinery':
        this.editMachinery = true;
        const updatedMachinery = this.data.machinery;
        this.formEditMachinery = this.formBuilder.group({
          id: new FormControl(updatedMachinery.id),
          category: new FormControl(updatedMachinery.category),
          type: new FormControl(updatedMachinery.type),
          brand: new FormControl(updatedMachinery.brand),
          model: new FormControl(updatedMachinery.model),
          romaNumber: new FormControl(updatedMachinery.romaNumber),
          romaDate1: new FormControl(updatedMachinery.romaDate1),
          romaDate: new FormControl(updatedMachinery.romaDate),
          lastInscDate: new FormControl(updatedMachinery.lastInscDate, [Validators.required, Validators.pattern(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/i)]),
          resultInsc: new FormControl(updatedMachinery.resultInsc, [Validators.required])
        });
        break;
      case 'delete-machinery':
        this.deleteMachinery = true;
        const deletedMachinery = this.data.machinery;
        this.formDeleteMachinery = this.formBuilder.group({
          id: new FormControl(deletedMachinery.id),
          category: new FormControl(deletedMachinery.category),
          type: new FormControl(deletedMachinery.type),
          brand: new FormControl(deletedMachinery.brand),
          model: new FormControl(deletedMachinery.model),
          romaNumber: new FormControl(deletedMachinery.romaNumber),
          romaDate1: new FormControl(deletedMachinery.romaDate1),
          romaDate: new FormControl(deletedMachinery.romaDate),
          lastInscDate: new FormControl(deletedMachinery.lastInscDate),
          resultInsc: new FormControl(deletedMachinery.resultInsc)
        });
        break;
      default:
        break;
    }
  }

  async saveMachineryDB() {
    this.formNewMachinery.markAllAsTouched();
    try {
      if(this.formNewMachinery.invalid) {
        this.formNewMachineryError = true;
      } else {
        const machinery: Machinery = this.formNewMachinery.value;
        await this.machineryService.insert(machinery).toPromise();
        this.snackBar.open('AÑADIDO: Maquinaria de la explotación.', 'OK', 
        { duration: 4000, horizontalPosition: 'end' } );
        this.dialog.closeAll();
      }
    } catch {
      this.snackBar.open('ERROR al añadir.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
    } 
  }

  async updateMachineryDB() {
    this.formEditMachinery.markAllAsTouched();
    if (this.formEditMachinery.invalid) {
      this.formEditMachineryError = true;
    } else {
      try {
        let machinery: Machinery = this.formEditMachinery.value;
        await this.machineryService.update(machinery.id, machinery).toPromise();
        this.snackBar.open('ACTUALIZADO: Maquinaria.', 'OK', 
          { duration: 4000, horizontalPosition: 'end' } );
      } catch {
        this.snackBar.open('ERROR al actualizar.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
      } finally {
        this.dialog.closeAll();
      }
    }
  }

  async deleteMachineryDB() {
    try {
      await this.machineryService.delete(this.data.machinery.id).toPromise();
      this.snackBar.open('ELIMINADO: Maquinaria.', 'OK', 
      { duration: 4000, horizontalPosition: 'end' } );
    } catch {
      this.snackBar.open('ERROR al eliminar.', 'OK', { duration: 4000, horizontalPosition: 'end' } );
    } finally {
      this.dialog.closeAll(); 
    }
  }

}
