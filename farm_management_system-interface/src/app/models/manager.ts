export class Manager {
    registrationID!: string;
    campaign?: string;
    regepaCode?: string;
    nif?: string;
    lastName?: string;
    name?: string;
    province?: string;
    city?: string;
    zipCode?: number;
    address?: string;
    phoneNumber?: number;
    mobileNumber?: number;
    email?: string;
}
