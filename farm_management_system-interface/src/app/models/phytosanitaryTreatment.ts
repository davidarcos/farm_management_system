import { Plot } from "./plot";

export class PhytosanitaryTreatment {
    id!: string;

    crop?: string;
    pest?: string;
    justification?: string;

    name?: string;
    registryNumber?: number;
    activeMatter?: string;
    lotNumber?: number;
    amount?: string;
    measurementUnit?: string;

    nonChemicalAlternatives?: string;

    date?: string;
    effectiveness?: string;
    nifOwner?: string;
    nifAdviser?: string;
    treatmentTeam?: number;
    romaNumber?: number;
    comments?: string;

    plots!: Array<Plot>;
}
