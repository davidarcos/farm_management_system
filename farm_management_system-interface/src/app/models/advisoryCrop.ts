export class AdvisoryCrop {
    id!: number;
    type?: string;
    crop?: string;
    section?: string;
}
