import { Plot } from "./plot";

export class MarketedHarvest {
    cropType?: string;
	cropTypeNumber?: number;
	crop?: string;
	cropNumber?: number;
	qualification?: number;
	lot?: number;
	comments?: string;
	plots?: Array<Plot>;
	date?: string;
	invoiceNumber!: string;
	amount?: number;
	handoutNumber?: number;
	purchaserNif?: string;
	purchaserFullName?: string;
	purchaserAddress?: string;
	province?: string;
	provinceNumber?: number;
	city?: string;
	cityNumber?: number;
}
