export class Plot {
    orderNumber!: number;
    category?: string;
    cropType?: string;
    cropTypeNumber?: string;
    crop?: string;
    cropNumber?: string;
    variety?: string;
    varietyNumber?: string;
    varietyComments?: string;

    isGenModified?: string;

    varietyType?: string;
    mainPattern?: string;
    sizeX?: string;
    sizeY?: string;
    year?: string;

    province?: string;
    provinceNumber?: string;
    city?: string;
    cityNumber?: string;
    addition?: string;
    zone?: string;
    polygon?: string;
    plot?: string;
    enclosure?: string;
    cultivatedArea?: string;
    area?: string;
    sigpacUse?: string;
    slope?: string;

    isVulnerableArea?: string;
    isRedNatura?: string;
    isSecurityDistance?: string;
    securityDistance?: string;
    waterExtPointsType?: string;
    measures?: string;

    exploitationType?: string;
    irrigationSystem?: string;
    protectionType?: string;
    comments?: string;

    certificationType?: string;
    gipAdviceSystem?: string;

}
