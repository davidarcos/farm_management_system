export class Fertilizer {
    product?: string;
    productType?: string;
    datePurchase?: string;
    invoiceNumberPurchase!: string;
    amountPurchase?: number;
    providerNif?: string;
    providerFullName?: string;
}
