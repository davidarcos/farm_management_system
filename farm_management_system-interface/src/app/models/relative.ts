export class Relative {
    nif!: string;
    lastName?: string;
    name?: string;
    orderNumber?: number;
    carnetType?: string;
    ropoNumber?: string;
    relationship?: string;
}
