export class Sapling {
    cropType?: string;
    cropTypeNumber?: number;
    crop?: string;
    cropNumber?: number;
    variety?: string;
    varietyNumber?: string;
    pattern?: string;
    lot?: number;
    datePurchase?: string;
    invoiceNumberPurchase!: string;
    amountPurchase?: number;
    providerNif?: string;
    providerFullName?: string;
}
