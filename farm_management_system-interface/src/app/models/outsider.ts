export class Outsider {
    nif!: string;
    lastName?: string;
    name?: string;
    orderNumber?: number;
    carnetType?: string;
    ropoNumber?: string;
}
