export class PhytosanitaryProduct {
    tradeName?: string;
    registryNumber?: number;
    activeMatter?: string;
    lot?: number;
    datePurchase?: string;
    invoiceNumberPurchase!: string;
    amountPurchase?: number;
    providerNif?: string;
    providerFullName?: string;
}
