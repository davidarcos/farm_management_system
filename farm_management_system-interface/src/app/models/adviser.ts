import { AdvisoryCrop } from "./advisoryCrop";

export class Adviser {
    nif!: string;
    lastName?: string;
    name?: string;
    orderNumber?: number;
    ropoNumber?: string;
    fungusInstAdvice?: string;
    crops?: Array<AdvisoryCrop>;
}
