export class OtherTreatment {
    id!: string;
    name?: string;
    volume?: string;
    date?: string;
    nifOwner?: string;
    reason_plague?: string;
    tradeName?: string;
    registryNumber?: number;
    amount?: string;
    category?: string;
}
