export class Machinery {
    id!: number;
    category?: string;
    type?: string;
    brand?: string;
    model?: string;
    romaNumber?: number;
    romaDate1?: string;
    romaDate?: string;
    lastInscDate?: string;
    resultInsc?: string;
}
