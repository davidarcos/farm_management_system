export class SeedPlant {
    cropType?: string;
    cropTypeNumber?: number;
    crop?: string;
    cropNumber?: number;
    variety?: string;
    varietyNumber?: string;
    category?: string;
    lot?: number;
    amount?: number;
    datePurchase?: string;
    invoiceNumberPurchase!: string;
    amountPurchase?: number;
    providerNif?: string;
    providerFullName?: string;
}
