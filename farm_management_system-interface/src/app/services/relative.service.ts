import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Relative } from '../models/relative';

@Injectable({
  providedIn: 'root'
})
export class RelativeService {
  
  url = "http://localhost:8888/generalData/relatives/"
  constructor(
    private http: HttpClient
  ) { }

  findAll() {
    return this.http.get<Relative[]>(this.url);
  }

  insert(relative: Relative): Observable<Relative> {
    return this.http.post<Relative>(this.url, relative);
  }

  update(id: string, relative: Relative) {
    const urlUpdate = `${this.url}${id}`;
    return this.http.put<Relative>(urlUpdate, relative);
  }

  delete(id: string) {
    const urlDelete = `${this.url}${id}`;
    return this.http.delete(urlDelete);
  }
  
}
