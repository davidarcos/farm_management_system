import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SeedPlant } from '../models/seedPlant';

@Injectable({
  providedIn: 'root'
})
export class SeedPlantService {

  url = "http://localhost:8888/purchases/seeds-plants/"
  constructor(
    private http: HttpClient
  ) { }

  findAll() {
    return this.http.get<SeedPlant[]>(this.url);
  }

  insert(seedPlant: SeedPlant): Observable<SeedPlant> {
    return this.http.post<SeedPlant>(this.url, seedPlant);
  }

  update(id: string, seedPlant: SeedPlant) {
    const urlUpdate = `${this.url}${id}`;
    return this.http.put<SeedPlant>(urlUpdate, seedPlant);
  }

  delete(id: string) {
    const urlDelete = `${this.url}${id}`;
    return this.http.delete(urlDelete);
  }

}
