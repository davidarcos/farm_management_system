import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PhytosanitaryTreatment } from '../models/phytosanitaryTreatment';

@Injectable({
  providedIn: 'root'
})
export class PhytosanitaryTreatmentService {
  url = "http://localhost:8888/treatments/phytosanitaries/"
  constructor(
    private http: HttpClient
  ) { }

  findAll() {
    return this.http.get<PhytosanitaryTreatment[]>(this.url);
  }

  findById(id: string): Observable<PhytosanitaryTreatment>{
    const urlFindById = `${this.url}${id}`;
    return this.http.get<PhytosanitaryTreatment>(urlFindById);
  }

  insert(phytoTreatment: PhytosanitaryTreatment): Observable<PhytosanitaryTreatment> {
    return this.http.post<PhytosanitaryTreatment>(this.url, phytoTreatment);
  }

  update(id: string, phytoTreatment: PhytosanitaryTreatment) {
    const urlUpdate = `${this.url}${id}`;
    return this.http.put<PhytosanitaryTreatment>(urlUpdate, phytoTreatment);
  }

  delete(id: string) {
    const urlDelete = `${this.url}${id}`;
    return this.http.delete(urlDelete);
  }

}
