import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Machinery } from '../models/machinery';

@Injectable({
  providedIn: 'root'
})
export class MachineryService {

  url = "http://localhost:8888/machineries/"
  urlCategory = "categories/"
  constructor(
    private http: HttpClient
  ) { }

  findByCategory(category: string) {
    const urlFindByCategory= `${this.url}${this.urlCategory}${category}`;
    return this.http.get<Machinery[]>(urlFindByCategory);
  }

  insert(machinery: Machinery): Observable<Machinery> {
    return this.http.post<Machinery>(this.url, machinery);
  }

  update(id: number, machinery: Machinery) {
    const urlUpdate = `${this.url}${id}`;
    return this.http.put<Machinery>(urlUpdate, machinery);
  }

  delete(id: number) {
    const urlDelete = `${this.url}${id}`;
    return this.http.delete(urlDelete);
  }

}
