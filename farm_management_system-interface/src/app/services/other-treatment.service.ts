import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { OtherTreatment } from '../models/otherTreatment';

@Injectable({
  providedIn: 'root'
})
export class OtherTreatmentService {

  url = "http://localhost:8888/treatments/"
  urlCategory = "category/"

  constructor(
    private http: HttpClient
  ) { }

  findByCategory(category: string) {
    const urlFindByCategory= `${this.url}${this.urlCategory}${category}`;
    return this.http.get<OtherTreatment[]>(urlFindByCategory);
  }

  insert(otherTreatment: OtherTreatment): Observable<OtherTreatment> {
    return this.http.post<OtherTreatment>(this.url, otherTreatment);
  }

  update(id: string, otherTreatment: OtherTreatment) {
    const urlUpdate = `${this.url}${id}`;
    return this.http.put<OtherTreatment>(urlUpdate, otherTreatment);
  }

  delete(id: string) {
    const urlDelete = `${this.url}${id}`;
    return this.http.delete(urlDelete);
  }

}
