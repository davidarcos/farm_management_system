import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Manager } from '../models/manager';

@Injectable({
  providedIn: 'root'
})
export class ManagerService {
  url = "http://localhost:8888/generalData/owners/managers/"
  constructor(
    private http: HttpClient
  ) { }

  findById(id: string): Observable<Manager>{
    const urlFindById = `${this.url}${id}`;
    return this.http.get<Manager>(urlFindById);
  }

  insert(manager: Manager): Observable<Manager> {
    return this.http.post<Manager>(this.url, manager);
  }

  update(id: string, manager: Manager) {
    const urlUpdate = `${this.url}${id}`;
    return this.http.put<Manager>(urlUpdate, manager);
  }

  delete(id: string) {
    const urlDelete = `${this.url}${id}`;
    return this.http.delete(urlDelete);
  }
  
}
