import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Fertilizer } from '../models/fertilizer';

@Injectable({
  providedIn: 'root'
})
export class FertilizersService {

  url = "http://localhost:8888/purchases/fertilizers/"
  constructor(
    private http: HttpClient
  ) { }

  findAll() {
    return this.http.get<Fertilizer[]>(this.url);
  }

  insert(fertilizer: Fertilizer): Observable<Fertilizer> {
    return this.http.post<Fertilizer>(this.url, fertilizer);
  }

  update(id: string, fertilizer: Fertilizer) {
    const urlUpdate = `${this.url}${id}`;
    return this.http.put<Fertilizer>(urlUpdate, fertilizer);
  }

  delete(id: string) {
    const urlDelete = `${this.url}${id}`;
    return this.http.delete(urlDelete);
  }
}
