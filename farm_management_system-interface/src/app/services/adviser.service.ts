import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Adviser } from '../models/adviser';

@Injectable({
  providedIn: 'root'
})
export class AdviserService {
  url = "http://localhost:8888/generalData/advisers/"
  urlNif = "nif/"

  constructor(
    private http: HttpClient
  ) { }

  findAll() {
    return this.http.get<Adviser[]>(this.url);
  }

  findByNif(nif: string) {
    const urlCheckNif = `${this.url}${this.urlNif}${nif}`;
    return this.http.get<Adviser[]>(urlCheckNif);
  }

  insert(adviser: Adviser): Observable<Adviser> {
    return this.http.post<Adviser>(this.url, adviser);
  }

  update(id: string, adviser: Adviser) {
    const urlUpdate = `${this.url}${id}`;
    return this.http.put<Adviser>(urlUpdate, adviser);
  }

  delete(id: string) {
    const urlDelete = `${this.url}${id}`;
    return this.http.delete(urlDelete);
  }

}
