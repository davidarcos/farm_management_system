import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Owner } from '../models/owner';

@Injectable({
  providedIn: 'root'
})
export class OwnerService {
  url = "http://localhost:8888/generalData/owners/"
  urlNif = "nif/"
  urlRegistrationID = "registrationID/"
  urlCampaign = "campaign/"
  urlRegepaCode = "regepaCode/"

  constructor(
    private http: HttpClient
  ) { }

  findAll() {
    return this.http.get<Owner[]>(this.url);
  }

  findByNif(nif: string) {
    const urlCheckNif = `${this.url}${this.urlNif}${nif}`;
    return this.http.get<Owner[]>(urlCheckNif);
  }

  findByRegistrationID(registrationID: string) {
    const urlRegID = `${this.url}${this.urlRegistrationID}${registrationID}`;
    return this.http.get<Owner[]>(urlRegID);
  }

  findByCampaign(campaign: string) {
    const urlCampaign = `${this.url}${this.urlCampaign}${campaign}`;
    return this.http.get<Owner[]>(urlCampaign);
  }

  findByRegepaCode(regepaCode: string) {
    const urlRegepaCode= `${this.url}${this.urlRegepaCode}${regepaCode}`;
    return this.http.get<Owner[]>(urlRegepaCode);
  }

  insert(owner: Owner): Observable<Owner> {
    return this.http.post<Owner>(this.url, owner);
  }

  update(id: string, owner: Owner) {
    const urlUpdate = `${this.url}${id}`;
    return this.http.put<Owner>(urlUpdate, owner);
  }

  delete(id: string) {
    const urlDelete = `${this.url}${id}`;
    return this.http.delete(urlDelete);
  }

}
