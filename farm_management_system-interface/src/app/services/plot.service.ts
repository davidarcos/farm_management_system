import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Plot } from '../models/plot';

@Injectable({
  providedIn: 'root'
})
export class PlotService {
  
  url = "http://localhost:8888/plots/"
  urlCategory = "categories/"
  constructor(
    private http: HttpClient
  ) { }


  findAll() {
    return this.http.get<Plot[]>(this.url);
  }

  findByCategory(category: string) {
    const urlFindByCategory= `${this.url}${this.urlCategory}${category}`;
    return this.http.get<Plot[]>(urlFindByCategory);
  }

  update(id: number, plot: Plot) {
    const urlUpdate = `${this.url}${id}`;
    return this.http.put<Plot>(urlUpdate, plot);
  }

}
