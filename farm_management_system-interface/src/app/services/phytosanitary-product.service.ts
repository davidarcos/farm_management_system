import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PhytosanitaryProduct } from '../models/phytosanitaryProduct';

@Injectable({
  providedIn: 'root'
})
export class PhytosanitaryProductService {

  url = "http://localhost:8888/purchases/phytosanitaryProducts/"
  constructor(
    private http: HttpClient
  ) { }

  findAll() {
    return this.http.get<PhytosanitaryProduct[]>(this.url);
  }

  insert(phytosanitaryProduct: PhytosanitaryProduct): Observable<PhytosanitaryProduct> {
    return this.http.post<PhytosanitaryProduct>(this.url, phytosanitaryProduct);
  }

  update(id: string, phytosanitaryProduct: PhytosanitaryProduct) {
    const urlUpdate = `${this.url}${id}`;
    return this.http.put<PhytosanitaryProduct>(urlUpdate, phytosanitaryProduct);
  }

  delete(id: string) {
    const urlDelete = `${this.url}${id}`;
    return this.http.delete(urlDelete);
  }

}
