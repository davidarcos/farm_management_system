import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Outsider } from '../models/outsider';
import { Relative } from '../models/relative';

@Injectable({
  providedIn: 'root'
})
export class OutsiderService {
  url = "http://localhost:8888/generalData/outsiders/"
  constructor(
    private http: HttpClient
  ) { }

  findAll() {
    return this.http.get<Outsider[]>(this.url);
  }

  insert(outsider: Outsider): Observable<Outsider> {
    return this.http.post<Outsider>(this.url, outsider);
  }

  update(id: string, outsider: Outsider) {
    const urlUpdate = `${this.url}${id}`;
    return this.http.put<Outsider>(urlUpdate, outsider);
  }

  delete(id: string) {
    const urlDelete = `${this.url}${id}`;
    return this.http.delete(urlDelete);
  }
  
}
