import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Sapling } from '../models/sapling';

@Injectable({
  providedIn: 'root'
})
export class SaplingService {

  url = "http://localhost:8888/purchases/saplings/"
  constructor(
    private http: HttpClient
  ) { }

  findAll() {
    return this.http.get<Sapling[]>(this.url);
  }

  insert(sapling: Sapling): Observable<Sapling> {
    return this.http.post<Sapling>(this.url, sapling);
  }

  update(id: string, sapling: Sapling) {
    const urlUpdate = `${this.url}${id}`;
    return this.http.put<Sapling>(urlUpdate, sapling);
  }

  delete(id: string) {
    const urlDelete = `${this.url}${id}`;
    return this.http.delete(urlDelete);
  }
  
}
