import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AdvisoryCrop } from '../models/advisoryCrop';

@Injectable({
  providedIn: 'root'
})
export class AdvisoryCropService {
  url = "http://localhost:8888/generalData/advisers/advisoryCrop/"
  constructor(
    private http: HttpClient
  ) { }

  findAll() {
    return this.http.get<AdvisoryCrop[]>(this.url);
  }

  findAllBySection(section: string) {
    const findBySection = `${this.url}${section}`;
    return this.http.get<AdvisoryCrop[]>(findBySection);
  }

}
