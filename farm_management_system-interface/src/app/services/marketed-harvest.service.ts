import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MarketedHarvest } from '../models/marketedHarvest';

@Injectable({
  providedIn: 'root'
})
export class MarketedHarvestService {

  url = "http://localhost:8888/sales/marketedHarvest/"
  constructor(
    private http: HttpClient
  ) { }

  findAll() {
    return this.http.get<MarketedHarvest[]>(this.url);
  }

  insert(marketedHarvest: MarketedHarvest): Observable<MarketedHarvest> {
    return this.http.post<MarketedHarvest>(this.url, marketedHarvest);
  }

  update(id: string, marketedHarvest: MarketedHarvest) {
    const urlUpdate = `${this.url}${id}`;
    return this.http.put<MarketedHarvest>(urlUpdate, marketedHarvest);
  }

  delete(id: string) {
    const urlDelete = `${this.url}${id}`;
    return this.http.delete(urlDelete);
  }
}
