import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[appOnlyNumbers]'
})
export class OnlyNumbersDirective {

  @HostListener('paste', ['$event'])
  onPaste(e: ClipboardEvent) {
    const data = e.clipboardData!.getData('text/plain');
    if(isNaN(parseInt(data,10))) {
        e.preventDefault();
    }
  }

  @HostListener('keydown', ['$event']) 
  onKeyDown(e: KeyboardEvent) {
    if(
      ((e.key == "c" || e.key == "C") && e.metaKey == true) || ((e.key == "c" || e.key == "C") && e.ctrlKey == true) || // Ctrl + C
      ((e.key == "v" || e.key == "V") && e.metaKey == true) || ((e.key == "v" || e.key == "V") && e.ctrlKey == true) || // Ctrl + V
      ((e.key == "x" || e.key == "X") && e.metaKey == true) || ((e.key == "x" || e.key == "X") && e.ctrlKey == true) || // Ctrl + X
      ((e.key == "a" || e.key == "A") && e.metaKey == true) || ((e.key == "a" || e.key == "A") && e.ctrlKey == true) || // Ctrl + A

      (e.key == "ArrowRight") || (e.key == "ArrowLeft") || // Go right and left
      (e.key == "Backspace") || (e.key == "Delete") || (e.key == "Tab") || (e.key == "Enter") || // Backspace, delete, tab, enter
      
      (e.key == "0") || (e.key == "1") || (e.key == "2") || (e.key == "3") || (e.key == "4") || //Numbers
      (e.key == "5") || (e.key == "6") || (e.key == "7") || (e.key == "8") || (e.key == "9")
    ) {
      return;
    }

    e.preventDefault();

  }

}
