import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[appOnlyLetters]'
})
export class OnlyLettersDirective {

  @HostListener('paste', ['$event'])
  onPaste(e: ClipboardEvent) {
    const data = e.clipboardData!.getData('text/plain');
    let regex = /[a-zA-Z\u0600-\u06FF]/g;
    let m;
    let matches = 0;

    while((m = regex.exec(data)) != null) {
      if(m.index == regex.lastIndex) {
        regex.lastIndex++;
      }
      m.forEach((match, groupIndex) => {
        matches++;
      });
    }

    if(matches === data.length) {
      return;
    } else {
      e.preventDefault();
    }
  }

  @HostListener('keydown', ['$event']) 
  onKeyDown(e: KeyboardEvent) {
    
    if(
      (e.key == "0") || (e.key == "1") || (e.key == "2") || (e.key == "3") || (e.key == "4") || //Numbers
      (e.key == "5") || (e.key == "6") || (e.key == "7") || (e.key == "8") || (e.key == "9") ||
      (e.key == "*") || (e.key == "+") || (e.key == "/") || (e.key == ",") || (e.key == ";") || //Special chars
      (e.key == "¡") || (e.key == "|") || (e.key == "!") || (e.key == '"') || (e.key == "·") ||
      (e.key == "#") || (e.key == "~") || (e.key == "$") || (e.key == "%") || (e.key == "&") ||
      (e.key == "(") || (e.key == ")") || (e.key == "=") || (e.key == "?") || (e.key == "¿") ||
      (e.key == "[") || (e.key == "]") || (e.key == "{") || (e.key == "}")
    ) {
      e.preventDefault();
    }

    return;
  }

}
