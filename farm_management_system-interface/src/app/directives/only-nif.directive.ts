import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[appOnlyNif]'
})
export class OnlyNifDirective {

  @HostListener('paste', ['$event'])
  onPaste(e: ClipboardEvent) {
    const data = e.clipboardData!.getData('text/plain');
    let regex = /[a-zA-Z0-9\u0600-\u06FF]/g;
    let m;
    let matches = 0;

    while((m = regex.exec(data)) != null) {
      if(m.index == regex.lastIndex) {
        regex.lastIndex++;
      }
      m.forEach((match, groupIndex) => {
        matches++;
      });
    }

    if(matches === data.length) {
      return;
    } else {
      e.preventDefault();
    }
  }

  @HostListener('keydown', ['$event']) 
  onKeyDown(e: KeyboardEvent) {
    if(
      ((e.key == "c" || e.key == "C") && e.metaKey == true) || ((e.key == "c" || e.key == "C") && e.ctrlKey == true) || // Ctrl + C
      ((e.key == "v" || e.key == "V") && e.metaKey == true) || ((e.key == "v" || e.key == "V") && e.ctrlKey == true) || // Ctrl + V
      ((e.key == "x" || e.key == "X") && e.metaKey == true) || ((e.key == "x" || e.key == "X") && e.ctrlKey == true) || // Ctrl + X
      ((e.key == "a" || e.key == "A") && e.metaKey == true) || ((e.key == "a" || e.key == "A") && e.ctrlKey == true) || // Ctrl + A

      (e.key == "ArrowRight") || (e.key == "ArrowLeft") || // Go right and left
      (e.key == "Backspace") || (e.key == "Delete") || (e.key == "Tab") || (e.key == "Enter") || // Backspace, delete, tab, enter

      (e.key == "0") || (e.key == "1") || (e.key == "2") || (e.key == "3") || (e.key == "4") || // Numbers
      (e.key == "5") || (e.key == "6") || (e.key == "7") || (e.key == "8") || (e.key == "9") ||

      (e.key == "A") || (e.key == "B") || (e.key == "C") || (e.key == "D") || (e.key == "E") || // Letters
      (e.key == "F") || (e.key == "G") || (e.key == "H") || (e.key == "I") || (e.key == "J") ||
      (e.key == "K") || (e.key == "L") || (e.key == "M") || (e.key == "N") || (e.key == "O") ||
      (e.key == "P") || (e.key == "Q") || (e.key == "R") || (e.key == "S") || (e.key == "T") ||
      (e.key == "U") || (e.key == "V") || (e.key == "W") || (e.key == "X") || (e.key == "W") ||
      (e.key == "Z") || (e.key == "a") || (e.key == "b") || (e.key == "c") || (e.key == "d") ||
      (e.key == "e") || (e.key == "f") || (e.key == "g") || (e.key == "h") || (e.key == "i") ||
      (e.key == "j") || (e.key == "k") || (e.key == "l") || (e.key == "m") || (e.key == "n") ||
      (e.key == "o") || (e.key == "p") || (e.key == "q") || (e.key == "r") || (e.key == "s") || 
      (e.key == "t") || (e.key == "u") || (e.key == "v") || (e.key == "w") || (e.key == "x") ||
      (e.key == "y") || (e.key == "z")
    ) {
      return;
    }

    e.preventDefault();
    
  }

}
