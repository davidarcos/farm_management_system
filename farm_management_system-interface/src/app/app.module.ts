import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RootNavComponent } from './root-nav/root-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { GeneralDataComponent } from './components/general-data/general-data.component';
import { MachineriesComponent } from './components/machineries/machineries.component';
import { PlotsComponent } from './components/plots/plots.component';
import { TreatmentsRecordComponent } from './components/treatments-record/treatments-record.component';
import { AccountingDataRecordComponent } from './components/accounting-data-record/accounting-data-record.component';
import { HomeComponent } from './components/home/home.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MatTabsModule } from '@angular/material/tabs';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { OwnerService } from './services/owner.service';
import { HttpClientModule } from '@angular/common/http';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { OnlyNumbersDirective } from './directives/only-numbers.directive';
import { OnlyNifDirective } from './directives/only-nif.directive';
import { OnlyLettersDirective } from './directives/only-letters.directive';
import { DialogOwnersComponent } from './components/general-data/dialogs/dialog-owners/dialog-owners.component';
import { ManagerService } from './services/manager.service';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { RelativeService } from './services/relative.service';
import { DialogRelativesComponent } from './components/general-data/dialogs/dialog-relatives/dialog-relatives.component';
import { DialogOutsidersComponent } from './components/general-data/dialogs/dialog-outsiders/dialog-outsiders.component';
import { OutsiderService } from './services/outsider.service';
import { DialogAdviserComponent } from './components/general-data/dialogs/dialog-adviser/dialog-adviser.component';
import { AdviserService } from './services/adviser.service';
import { AdvisoryCropService } from './services/advisory-crop.service';
import { MachineryService } from './services/machinery.service';
import { DialogMachineryComponent } from './components/machineries/dialogs/dialog-machinery/dialog-machinery.component';
import { PhytosanitaryTreatmentService } from './services/phytosanitary-treatment.service';
import { DialogPhytosanitaryTreatmentsComponent } from './components/treatments-record/dialogs/dialog-phytosanitary-treatments/dialog-phytosanitary-treatments.component';
import { DialogOtherTreatmentsComponent } from './components/treatments-record/dialogs/dialog-other-treatments/dialog-other-treatments.component';
import { DialogPlotsComponent } from './components/plots/dialogs/dialog-plots/dialog-plots.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { DialogSeedsPlantsComponent } from './components/accounting-data-record/dialogs/dialog-seeds-plants/dialog-seeds-plants.component';
import { DialogSaplingsComponent } from './components/accounting-data-record/dialogs/dialog-saplings/dialog-saplings.component';
import { DialogFertilizersComponent } from './components/accounting-data-record/dialogs/dialog-fertilizers/dialog-fertilizers.component';
import { DialogPhytosanitayProductsComponent } from './components/accounting-data-record/dialogs/dialog-phytosanitay-products/dialog-phytosanitay-products.component';
import { DialogMarketedHarvestsComponent } from './components/accounting-data-record/dialogs/dialog-marketed-harvests/dialog-marketed-harvests.component';


@NgModule({
  declarations: [
    AppComponent,
    RootNavComponent,
    GeneralDataComponent,
    MachineriesComponent,
    PlotsComponent,
    TreatmentsRecordComponent,
    AccountingDataRecordComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    OnlyNumbersDirective,
    OnlyNifDirective,
    OnlyLettersDirective,
    DialogOwnersComponent,
    DialogRelativesComponent,
    DialogOutsidersComponent,
    DialogAdviserComponent,
    DialogMachineryComponent,
    DialogPhytosanitaryTreatmentsComponent,
    DialogOtherTreatmentsComponent,
    DialogPlotsComponent,
    DialogSeedsPlantsComponent,
    DialogSaplingsComponent,
    DialogFertilizersComponent,
    DialogPhytosanitayProductsComponent,
    DialogMarketedHarvestsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatTabsModule,
    MatPaginatorModule,
    MatTableModule,
    HttpClientModule,
    MatDialogModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatIconModule,
    MatSnackBarModule,
    FormsModule,
    MatSlideToggleModule,
    MatCheckboxModule,
    MatChipsModule
  ],
  providers: [
    OwnerService,
    ManagerService,
    RelativeService,
    OutsiderService,
    AdviserService,
    AdvisoryCropService,
    MachineryService,
    PhytosanitaryTreatmentService
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    DialogOwnersComponent,
    DialogRelativesComponent,
    DialogOutsidersComponent,
    DialogAdviserComponent,
    DialogMachineryComponent,
    DialogPhytosanitaryTreatmentsComponent,
    DialogPlotsComponent,
    DialogSeedsPlantsComponent,
    DialogFertilizersComponent,
    DialogPhytosanitayProductsComponent,
    DialogMarketedHarvestsComponent
  ]
})
export class AppModule { }
