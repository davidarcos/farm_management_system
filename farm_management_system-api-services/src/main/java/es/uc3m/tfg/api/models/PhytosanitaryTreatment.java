package es.uc3m.tfg.api.models;

import java.io.Serializable;
import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "PhytosanitaryTreatments")
public class PhytosanitaryTreatment implements Serializable {

	private static final long serialVersionUID = 472939024728423L;

	@Id
	private String id;
	
	/* Crops and pest data */

	private String crop;

	private String pest;

	private String justification;

	/* Phytosanitary Treatments */

	private String name;

	private Integer registryNumber;

	private String activeMatter;

	private Integer lotNumber;

	private String amount;

	private String measurementUnit;

	/* Non-chemical alternatives */

	private String nonChemicalAlternatives;

	/* Other data */

	private String date;

	private String effectiveness;

	private String nifOwner;

	private String nifAdviser;

	private Integer treatmentTeam;

	private Integer romaNumber;

	private String comments;

	/* SigPac reference */

	private List<Plot> plots;

	
	/* Getters and Setters */
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getCrop() {
		return crop;
	}

	public void setCrop(String crop) {
		this.crop = crop;
	}

	public String getPest() {
		return pest;
	}

	public void setPest(String pest) {
		this.pest = pest;
	}

	public String getJustification() {
		return justification;
	}

	public void setJustification(String justification) {
		this.justification = justification;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getRegistryNumber() {
		return registryNumber;
	}

	public void setRegistryNumber(Integer registryNumber) {
		this.registryNumber = registryNumber;
	}

	public String getActiveMatter() {
		return activeMatter;
	}

	public void setActiveMatter(String activeMatter) {
		this.activeMatter = activeMatter;
	}

	public Integer getLotNumber() {
		return lotNumber;
	}

	public void setLotNumber(Integer lotNumber) {
		this.lotNumber = lotNumber;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getMeasurementUnit() {
		return measurementUnit;
	}

	public void setMeasurementUnit(String measurementUnit) {
		this.measurementUnit = measurementUnit;
	}

	public String getNonChemicalAlternatives() {
		return nonChemicalAlternatives;
	}

	public void setNonChemicalAlternatives(String nonChemicalAlternatives) {
		this.nonChemicalAlternatives = nonChemicalAlternatives;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getEffectiveness() {
		return effectiveness;
	}

	public void setEffectiveness(String effectiveness) {
		this.effectiveness = effectiveness;
	}

	public String getNifOwner() {
		return nifOwner;
	}

	public void setNifOwner(String nifOwner) {
		this.nifOwner = nifOwner;
	}

	public String getNifAdviser() {
		return nifAdviser;
	}

	public void setNifAdviser(String nifAdviser) {
		this.nifAdviser = nifAdviser;
	}

	public Integer getTreatmentTeam() {
		return treatmentTeam;
	}

	public void setTreatmentTeam(Integer treatmentTeam) {
		this.treatmentTeam = treatmentTeam;
	}

	public Integer getRomaNumber() {
		return romaNumber;
	}

	public void setRomaNumber(Integer romaNumber) {
		this.romaNumber = romaNumber;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public List<Plot> getPlots() {
		return plots;
	}

	public void setPlots(List<Plot> plots) {
		this.plots = plots;
	}

}
