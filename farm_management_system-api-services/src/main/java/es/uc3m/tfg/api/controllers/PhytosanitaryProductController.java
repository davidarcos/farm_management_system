package es.uc3m.tfg.api.controllers;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import es.uc3m.tfg.api.models.PhytosanitaryProduct;
import es.uc3m.tfg.api.repositories.IPhytosanitaryProductDAO;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.POST, RequestMethod.GET, RequestMethod.PUT,
		RequestMethod.DELETE })
@RequestMapping("/purchases/phytosanitaryProducts")
public class PhytosanitaryProductController {

	@Autowired
	private IPhytosanitaryProductDAO iPhytosanitaryProductDAO;

	@GetMapping("/")
	public List<PhytosanitaryProduct> findAll() {
		return iPhytosanitaryProductDAO.findAll();
	}

	@GetMapping("/{id}")
	public Optional<PhytosanitaryProduct> findById(@PathVariable String id) {
		return iPhytosanitaryProductDAO.findById(id);
	}

	@PostMapping("/")
	public PhytosanitaryProduct insert(@Validated @RequestBody PhytosanitaryProduct pytosanitaryProduct) {
		return iPhytosanitaryProductDAO.insert(pytosanitaryProduct);
	}

	@PutMapping("/{id}")
	public PhytosanitaryProduct update(@PathVariable String id,
			@Validated @RequestBody PhytosanitaryProduct pytosanitaryProduct) {
		return iPhytosanitaryProductDAO.save(pytosanitaryProduct);
	}

	@DeleteMapping("/")
	public void deleteAll() {
		iPhytosanitaryProductDAO.deleteAll();
	}

	@DeleteMapping("/{id}")
	public void deleteById(@PathVariable String id) {
		iPhytosanitaryProductDAO.deleteById(id);
	}

}
