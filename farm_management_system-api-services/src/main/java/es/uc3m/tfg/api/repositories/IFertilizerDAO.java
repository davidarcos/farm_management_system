package es.uc3m.tfg.api.repositories;

import java.io.Serializable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import es.uc3m.tfg.api.models.Fertilizer;

@Repository
public interface IFertilizerDAO extends MongoRepository<Fertilizer, Serializable> {

}
