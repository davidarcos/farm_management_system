package es.uc3m.tfg.api.models;

import java.io.Serializable;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Fertilizers")
public class Fertilizer implements Serializable {

	private static final long serialVersionUID = 472939024728423L;

	private String product;

	private String productType;

	private String datePurchase;
	
	@Id
	private String invoiceNumberPurchase;
	
	private Integer amountPurchase;
	
	private String providerNif;
	
	private String providerFullName;

	/* Getters and Setters */

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getDatePurchase() {
		return datePurchase;
	}

	public void setDatePurchase(String datePurchase) {
		this.datePurchase = datePurchase;
	}

	public String getInvoiceNumberPurchase() {
		return invoiceNumberPurchase;
	}

	public void setInvoiceNumberPurchase(String invoiceNumberPurchase) {
		this.invoiceNumberPurchase = invoiceNumberPurchase;
	}

	public Integer getAmountPurchase() {
		return amountPurchase;
	}

	public void setAmountPurchase(Integer amountPurchase) {
		this.amountPurchase = amountPurchase;
	}

	public String getProviderNif() {
		return providerNif;
	}

	public void setProviderNif(String providerNif) {
		this.providerNif = providerNif;
	}

	public String getProviderFullName() {
		return providerFullName;
	}

	public void setProviderFullName(String providerFullName) {
		this.providerFullName = providerFullName;
	}


}
