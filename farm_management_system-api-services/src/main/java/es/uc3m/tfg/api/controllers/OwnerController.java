package es.uc3m.tfg.api.controllers;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import es.uc3m.tfg.api.models.Owner;
import es.uc3m.tfg.api.repositories.IOwnerDAO;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.POST, RequestMethod.GET, RequestMethod.PUT,
		RequestMethod.DELETE })
@RequestMapping("/generalData/owners")
public class OwnerController {

	@Autowired
	private IOwnerDAO iOwnerDAO;

	@GetMapping("/")
	public List<Owner> findAll() {
		return iOwnerDAO.findAll();
	}

	@GetMapping("/{registrationID}")
	public Optional<Owner> findById(@PathVariable String registrationID) {
		return iOwnerDAO.findById(registrationID);
	}
	
	@GetMapping("/nif/{nif}")
	public List<Owner> findByNif(@PathVariable String nif) {
		return iOwnerDAO.findByNif(nif);
	}
	
	@GetMapping("/registrationID/{registrationID}")
	public List<Owner> findByRegistrationID(@PathVariable String registrationID) {
		return iOwnerDAO.findByRegistrationID(registrationID);
	}	
	
	@GetMapping("/campaign/{campaign}")
	public List<Owner> findByCampaign(@PathVariable String campaign) {
		return iOwnerDAO.findByCampaign(campaign);
	}	
	
	@GetMapping("/regepaCode/{regepaCode}")
	public List<Owner> findByRegepaCode(@PathVariable String regepaCode) {
		return iOwnerDAO.findByRegepaCode(regepaCode);
	}
	
	@PostMapping("/")
	public Owner insert(@Validated @RequestBody Owner owner) {
		return iOwnerDAO.insert(owner);
	}

	@PutMapping("/{registrationID}")
	public Owner update(@PathVariable String registrationID, @Validated @RequestBody Owner owner) {
		return iOwnerDAO.save(owner);
	}

	@DeleteMapping("/")
	public void deleteAll() {
		iOwnerDAO.deleteAll();
	}

	@DeleteMapping("/{registrationID}")
	public void deleteById(@PathVariable String registrationID) {
		iOwnerDAO.deleteById(registrationID);
	}

}
