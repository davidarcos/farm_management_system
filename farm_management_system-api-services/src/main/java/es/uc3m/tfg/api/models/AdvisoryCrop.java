package es.uc3m.tfg.api.models;

import java.io.Serializable;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "AdvisoryCrops")
public class AdvisoryCrop implements Serializable {

	private static final long serialVersionUID = 472939024728423L;

	@Id
	private Integer id;

	private String type;

	private String crop;

	private String section;

	/* Getters and Setters */

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCrop() {
		return crop;
	}

	public void setCrop(String crop) {
		this.crop = crop;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

}
