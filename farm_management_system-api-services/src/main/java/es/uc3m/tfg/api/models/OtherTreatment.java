package es.uc3m.tfg.api.models;

import java.io.Serializable;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection="OtherTreatments")
public class OtherTreatment implements Serializable {

	private static final long serialVersionUID = 472939024728423L;

	
	@Id	
	private String id;
	
	private String name;
	
	private String volume;
	
	private String date;
	
	private String nifOwner;
	
	private String reason_plague;
	
	private String tradeName;
	
	private Integer registryNumber;
	
	private String amount;

	private String category;

	
	/*Getters and Setters*/
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getNifOwner() {
		return nifOwner;
	}

	public void setNifOwner(String nifOwner) {
		this.nifOwner = nifOwner;
	}

	public String getReason_plague() {
		return reason_plague;
	}

	public void setReason_plague(String reason_plague) {
		this.reason_plague = reason_plague;
	}

	public String getTradeName() {
		return tradeName;
	}

	public void setTradeName(String tradeName) {
		this.tradeName = tradeName;
	}

	public Integer getRegistryNumber() {
		return registryNumber;
	}

	public void setRegistryNumber(Integer registryNumber) {
		this.registryNumber = registryNumber;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
			
}