package es.uc3m.tfg.api.controllers;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import es.uc3m.tfg.api.models.OtherTreatment;
import es.uc3m.tfg.api.repositories.IOtherTreatmentDAO;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.POST, RequestMethod.GET, RequestMethod.PUT,
		RequestMethod.DELETE })
@RequestMapping("/treatments/")
public class OtherTreatmentController {

	@Autowired
	private IOtherTreatmentDAO iOtherTreatmentDAO;

	@GetMapping("/")
	public List<OtherTreatment> findAll() {
		return iOtherTreatmentDAO.findAll();
	}

	@GetMapping("/category/{category}")
	public List<OtherTreatment> findAllByCategory(@PathVariable String category) {
		return iOtherTreatmentDAO.findAllByCategory(category);
	}

	@GetMapping("/{id}")
	public Optional<OtherTreatment> findById(@PathVariable String id) {
		return iOtherTreatmentDAO.findById(id);
	}

	@PostMapping("/")
	public OtherTreatment insert(@Validated @RequestBody OtherTreatment treatment) {
		return iOtherTreatmentDAO.insert(treatment);
	}

	@PutMapping("/{id}")
	public OtherTreatment update(@PathVariable String id, @Validated @RequestBody OtherTreatment treatment) {
		return iOtherTreatmentDAO.save(treatment);
	}

	@DeleteMapping("/")
	public void deleteAll() {
		iOtherTreatmentDAO.deleteAll();
	}

	@DeleteMapping("/{id}")
	public void deleteById(@PathVariable String id) {
		iOtherTreatmentDAO.deleteById(id);
	}

}
