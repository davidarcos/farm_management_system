package es.uc3m.tfg.api.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import es.uc3m.tfg.api.models.AdvisoryCrop;
import es.uc3m.tfg.api.repositories.IAdvisoryCropDAO;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.POST, RequestMethod.GET })
@RequestMapping("/generalData/advisers/advisoryCrop")
public class AdvisoryCropController {

	@Autowired
	private IAdvisoryCropDAO iAdvisoryCropDAO;

	@GetMapping("/")
	public List<AdvisoryCrop> findAll() {
		return iAdvisoryCropDAO.findAll();
	}

	@GetMapping("/{section}")
	public List<AdvisoryCrop> findAllBySection(@PathVariable String section) {
		return iAdvisoryCropDAO.findAllBySection(section);
	}

	@PostMapping("/")
	public AdvisoryCrop insert(@Validated @RequestBody AdvisoryCrop advisoryCrop) {
		return iAdvisoryCropDAO.insert(advisoryCrop);
	}

}
