package es.uc3m.tfg.api.models;

import java.io.Serializable;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "PhytosanitaryProducts")
public class PhytosanitaryProduct implements Serializable {

	private static final long serialVersionUID = 472939024728423L;

	private String tradeName;

	private Integer registryNumber;

	private String activeMatter;

	private Integer lot;

	private String datePurchase;
	
	@Id
	private String invoiceNumberPurchase;
	
	private Integer amountPurchase;
	
	private String providerNif;
	
	private String providerFullName;
	

	/* Getters and Setters */

	public String getTradeName() {
		return tradeName;
	}

	public void setTradeName(String tradeName) {
		this.tradeName = tradeName;
	}

	public Integer getRegistryNumber() {
		return registryNumber;
	}

	public void setRegistryNumber(Integer registryNumber) {
		this.registryNumber = registryNumber;
	}

	public String getActiveMatter() {
		return activeMatter;
	}

	public void setActiveMatter(String activeMatter) {
		this.activeMatter = activeMatter;
	}

	public Integer getLot() {
		return lot;
	}

	public void setLot(Integer lot) {
		this.lot = lot;
	}

	public String getDatePurchase() {
		return datePurchase;
	}

	public void setDatePurchase(String datePurchase) {
		this.datePurchase = datePurchase;
	}

	public String getInvoiceNumberPurchase() {
		return invoiceNumberPurchase;
	}

	public void setInvoiceNumberPurchase(String invoiceNumberPurchase) {
		this.invoiceNumberPurchase = invoiceNumberPurchase;
	}

	public Integer getAmountPurchase() {
		return amountPurchase;
	}

	public void setAmountPurchase(Integer amountPurchase) {
		this.amountPurchase = amountPurchase;
	}

	public String getProviderNif() {
		return providerNif;
	}

	public void setProviderNif(String providerNif) {
		this.providerNif = providerNif;
	}

	public String getProviderFullName() {
		return providerFullName;
	}

	public void setProviderFullName(String providerFullName) {
		this.providerFullName = providerFullName;
	}

}
