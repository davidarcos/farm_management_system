package es.uc3m.tfg.api.controllers;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import es.uc3m.tfg.api.models.SeedPlant;
import es.uc3m.tfg.api.repositories.ISeedPlantDAO;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.POST, RequestMethod.GET, RequestMethod.PUT,
		RequestMethod.DELETE })
@RequestMapping("/purchases/seeds-plants")
public class SeedPlantController {

	@Autowired
	private ISeedPlantDAO iSeedPlantDAO;

	@GetMapping("/")
	public List<SeedPlant> findAll() {
		return iSeedPlantDAO.findAll();
	}

	@GetMapping("/{id}")
	public Optional<SeedPlant> findById(@PathVariable String id) {
		return iSeedPlantDAO.findById(id);
	}

	@PostMapping("/")
	public SeedPlant insert(@Validated @RequestBody SeedPlant seed_plant) {
		return iSeedPlantDAO.insert(seed_plant);
	}

	@PutMapping("/{id}")
	public SeedPlant update(@PathVariable String id, @Validated @RequestBody SeedPlant seed_plant) {
		return iSeedPlantDAO.save(seed_plant);
	}

	@DeleteMapping("/")
	public void deleteAll() {
		iSeedPlantDAO.deleteAll();
	}

	@DeleteMapping("/{id}")
	public void deleteById(@PathVariable String id) {
		iSeedPlantDAO.deleteById(id);
	}

}
