package es.uc3m.tfg.api.controllers;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import es.uc3m.tfg.api.models.MarketedHarvest;
import es.uc3m.tfg.api.repositories.IMarketedHarvestDAO;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.POST, RequestMethod.GET, RequestMethod.PUT,
		RequestMethod.DELETE })
@RequestMapping("/sales/marketedHarvest")
public class MarketedHarvestController {

	@Autowired
	private IMarketedHarvestDAO iMarketedHarvestDAO;

	@GetMapping("/")
	public List<MarketedHarvest> findAll() {
		return iMarketedHarvestDAO.findAll();
	}

	@GetMapping("/{id}")
	public Optional<MarketedHarvest> findById(@PathVariable String id) {
		return iMarketedHarvestDAO.findById(id);
	}

	@PostMapping("/")
	public MarketedHarvest insert(@Validated @RequestBody MarketedHarvest marketedHarvest) {
		return iMarketedHarvestDAO.insert(marketedHarvest);
	}

	@PutMapping("/{id}")
	public MarketedHarvest update(@PathVariable String id, @Validated @RequestBody MarketedHarvest marketedHarvest) {
		return iMarketedHarvestDAO.save(marketedHarvest);
	}

	@DeleteMapping("/")
	public void deleteAll() {
		iMarketedHarvestDAO.deleteAll();
	}

	@DeleteMapping("/{id}")
	public void deleteById(@PathVariable String id) {
		iMarketedHarvestDAO.deleteById(id);
	}

}
