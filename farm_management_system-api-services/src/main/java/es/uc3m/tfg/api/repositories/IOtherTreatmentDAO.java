package es.uc3m.tfg.api.repositories;

import java.io.Serializable;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import es.uc3m.tfg.api.models.OtherTreatment;

@Repository
public interface IOtherTreatmentDAO extends MongoRepository<OtherTreatment, Serializable> {

	List<OtherTreatment> findAllByCategory(@Param("category") String category);
}
