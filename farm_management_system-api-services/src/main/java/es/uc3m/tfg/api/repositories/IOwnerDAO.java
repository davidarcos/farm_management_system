package es.uc3m.tfg.api.repositories;

import java.io.Serializable;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import es.uc3m.tfg.api.models.Owner;

@Repository
public interface IOwnerDAO extends MongoRepository<Owner, Serializable> {
	
	List<Owner> findByNif(@Param("nif") String nif);
	List<Owner> findByRegistrationID(@Param("registrationID") String registrationID);
	List<Owner> findByCampaign(@Param("campaign") String campaign);
	List<Owner> findByRegepaCode(@Param("regepaCode") String regepaCode);
}
