package es.uc3m.tfg.api.repositories;

import java.io.Serializable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import es.uc3m.tfg.api.models.Sapling;

@Repository
public interface ISaplingDAO extends MongoRepository<Sapling, Serializable> {

}
