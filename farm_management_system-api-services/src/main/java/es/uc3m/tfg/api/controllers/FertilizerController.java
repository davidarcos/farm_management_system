package es.uc3m.tfg.api.controllers;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import es.uc3m.tfg.api.models.Fertilizer;
import es.uc3m.tfg.api.repositories.IFertilizerDAO;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.POST, RequestMethod.GET, RequestMethod.PUT,
		RequestMethod.DELETE })
@RequestMapping("/purchases/fertilizers")
public class FertilizerController {

	@Autowired
	private IFertilizerDAO iFertilizerDAO;

	@GetMapping("/")
	public List<Fertilizer> findAll() {
		return iFertilizerDAO.findAll();
	}

	@GetMapping("/{id}")
	public Optional<Fertilizer> findById(@PathVariable String id) {
		return iFertilizerDAO.findById(id);
	}

	@PostMapping("/")
	public Fertilizer insert(@Validated @RequestBody Fertilizer fertilizer) {
		return iFertilizerDAO.insert(fertilizer);
	}

	@PutMapping("/{id}")
	public Fertilizer update(@PathVariable String id, @Validated @RequestBody Fertilizer fertilizer) {
		return iFertilizerDAO.save(fertilizer);
	}

	@DeleteMapping("/")
	public void deleteAll() {
		iFertilizerDAO.deleteAll();
	}

	@DeleteMapping("/{id}")
	public void deleteById(@PathVariable String id) {
		iFertilizerDAO.deleteById(id);
	}

}
