package es.uc3m.tfg.api.models;

import java.io.Serializable;
import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "MarketedHarvest")
public class MarketedHarvest implements Serializable {

	private static final long serialVersionUID = 472939024728423L;

	private String cropType;

	private Integer cropTypeNumber;

	private String crop;

	private Integer cropNumber;

	private Integer qualification;

	private Integer lot;

	private String comments;

	private List<Plot> plots;

	private String date;

	@Id
	private String invoiceNumber;

	private Integer amount;

	private Integer handoutNumber;

	private String purchaserNif;

	private String purchaserFullName;

	private String purchaserAddress;

	private String province;

	private Integer provinceNumber;

	private String city;

	private Integer cityNumber;

	
	/* Getters and Setters */

	public String getCropType() {
		return cropType;
	}

	public void setCropType(String cropType) {
		this.cropType = cropType;
	}

	public Integer getCropTypeNumber() {
		return cropTypeNumber;
	}

	public void setCropTypeNumber(Integer cropTypeNumber) {
		this.cropTypeNumber = cropTypeNumber;
	}

	public String getCrop() {
		return crop;
	}

	public void setCrop(String crop) {
		this.crop = crop;
	}

	public Integer getCropNumber() {
		return cropNumber;
	}

	public void setCropNumber(Integer cropNumber) {
		this.cropNumber = cropNumber;
	}

	public Integer getQualification() {
		return qualification;
	}

	public void setQualification(Integer qualification) {
		this.qualification = qualification;
	}

	public Integer getLot() {
		return lot;
	}

	public void setLot(Integer lot) {
		this.lot = lot;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public List<Plot> getPlots() {
		return plots;
	}

	public void setPlots(List<Plot> plots) {
		this.plots = plots;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public Integer getHandoutNumber() {
		return handoutNumber;
	}

	public void setHandoutNumber(Integer handoutNumber) {
		this.handoutNumber = handoutNumber;
	}

	public String getPurchaserNif() {
		return purchaserNif;
	}

	public void setPurchaserNif(String purchaserNif) {
		this.purchaserNif = purchaserNif;
	}

	public String getPurchaserFullName() {
		return purchaserFullName;
	}

	public void setPurchaserFullName(String purchaserFullName) {
		this.purchaserFullName = purchaserFullName;
	}

	public String getPurchaserAddress() {
		return purchaserAddress;
	}

	public void setPurchaserAddress(String purchaserAddress) {
		this.purchaserAddress = purchaserAddress;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public Integer getProvinceNumber() {
		return provinceNumber;
	}

	public void setProvinceNumber(Integer provinceNumber) {
		this.provinceNumber = provinceNumber;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Integer getCityNumber() {
		return cityNumber;
	}

	public void setCityNumber(Integer cityNumber) {
		this.cityNumber = cityNumber;
	}


}
