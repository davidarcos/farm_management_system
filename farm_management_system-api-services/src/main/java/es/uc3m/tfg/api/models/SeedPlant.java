package es.uc3m.tfg.api.models;

import java.io.Serializable;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Seeds-Plants")
public class SeedPlant implements Serializable {

	private static final long serialVersionUID = 472939024728423L;

	private String cropType;

	private Integer cropTypeNumber;

	private String crop;

	private Integer cropNumber;

	private String variety;

	private String varietyNumber;

	private String category;
	
	private Integer lot;

	private Integer amount;

	private String datePurchase;
	
	@Id
	private String invoiceNumberPurchase;
	
	private Integer amountPurchase;
	
	private String providerNif;
	
	private String providerFullName;

	
	/* Getters and Setters */
	
	public String getCropType() {
		return cropType;
	}

	public void setCropType(String cropType) {
		this.cropType = cropType;
	}

	public Integer getCropTypeNumber() {
		return cropTypeNumber;
	}

	public void setCropTypeNumber(Integer cropTypeNumber) {
		this.cropTypeNumber = cropTypeNumber;
	}

	public String getCrop() {
		return crop;
	}

	public void setCrop(String crop) {
		this.crop = crop;
	}

	public Integer getCropNumber() {
		return cropNumber;
	}

	public void setCropNumber(Integer cropNumber) {
		this.cropNumber = cropNumber;
	}

	public String getVariety() {
		return variety;
	}

	public void setVariety(String variety) {
		this.variety = variety;
	}

	public String getVarietyNumber() {
		return varietyNumber;
	}

	public void setVarietyNumber(String varietyNumber) {
		this.varietyNumber = varietyNumber;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Integer getLot() {
		return lot;
	}

	public void setLot(Integer lot) {
		this.lot = lot;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public String getDatePurchase() {
		return datePurchase;
	}

	public void setDatePurchase(String datePurchase) {
		this.datePurchase = datePurchase;
	}

	public String getInvoiceNumberPurchase() {
		return invoiceNumberPurchase;
	}

	public void setInvoiceNumberPurchase(String invoiceNumberPurchase) {
		this.invoiceNumberPurchase = invoiceNumberPurchase;
	}

	public Integer getAmountPurchase() {
		return amountPurchase;
	}

	public void setAmountPurchase(Integer amountPurchase) {
		this.amountPurchase = amountPurchase;
	}

	public String getProviderNif() {
		return providerNif;
	}

	public void setProviderNif(String providerNif) {
		this.providerNif = providerNif;
	}

	public String getProviderFullName() {
		return providerFullName;
	}

	public void setProviderFullName(String providerFullName) {
		this.providerFullName = providerFullName;
	}


}
