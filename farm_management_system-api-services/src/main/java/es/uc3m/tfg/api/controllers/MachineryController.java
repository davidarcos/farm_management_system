package es.uc3m.tfg.api.controllers;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import es.uc3m.tfg.api.models.Machinery;
import es.uc3m.tfg.api.repositories.IMachineryDAO;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.POST, RequestMethod.GET, RequestMethod.PUT,
		RequestMethod.DELETE })
@RequestMapping("/machineries")
public class MachineryController {

	@Autowired
	private IMachineryDAO iMachineryDAO;

	@GetMapping("/")
	public List<Machinery> findAll() {
		return iMachineryDAO.findAll();
	}

	@GetMapping("/categories/{category}")
	public List<Machinery> findAllByCategory(@PathVariable String category) {
		return iMachineryDAO.findAllByCategory(category);
	}

	@GetMapping("/{id}")
	public Optional<Machinery> findById(@PathVariable Long id) {
		return iMachineryDAO.findById(id);
	}

	@PostMapping("/")
	public Machinery insert(@Validated @RequestBody Machinery machinery) {
		return iMachineryDAO.insert(machinery);
	}

	@PutMapping("/{id}")
	public Machinery update(@PathVariable Long id, @Validated @RequestBody Machinery machinery) {
		return iMachineryDAO.save(machinery);
	}

	@DeleteMapping("/")
	public void deleteAll() {
		iMachineryDAO.deleteAll();
	}

	@DeleteMapping("/{id}")
	public void deleteById(@PathVariable Long id) {
		iMachineryDAO.deleteById(id);
	}

}
