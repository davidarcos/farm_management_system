package es.uc3m.tfg.api.controllers;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import es.uc3m.tfg.api.models.Adviser;
import es.uc3m.tfg.api.repositories.IAdviserDAO;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.POST, RequestMethod.GET, RequestMethod.PUT,
		RequestMethod.DELETE })
@RequestMapping("/generalData/advisers")
public class AdviserController {

	@Autowired
	private IAdviserDAO iAdviserDAO;

	@GetMapping("/")
	public List<Adviser> findAll() {
		return iAdviserDAO.findAll();
	}

	@GetMapping("/{id}")
	public Optional<Adviser> findById(@PathVariable String id) {
		return iAdviserDAO.findById(id);
	}

	@GetMapping("/nif/{nif}")
	public List<Adviser> findByNif(@PathVariable String nif) {
		return iAdviserDAO.findByNif(nif);
	}
	
	@PostMapping("/")
	public Adviser insert(@Validated @RequestBody Adviser adviser) {
		return iAdviserDAO.insert(adviser);
	}

	@PutMapping("/{id}")
	public Adviser update(@PathVariable String id, @Validated @RequestBody Adviser adviser) {
		return iAdviserDAO.save(adviser);
	}

	@DeleteMapping("/")
	public void deleteAll() {
		iAdviserDAO.deleteAll();
	}

	@DeleteMapping("/{id}")
	public void deleteById(@PathVariable String id) {
		iAdviserDAO.deleteById(id);
	}

}
