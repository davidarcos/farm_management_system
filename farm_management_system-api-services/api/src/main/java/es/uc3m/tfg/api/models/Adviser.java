package es.uc3m.tfg.api.models;

import java.io.Serializable;
import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Advisers")
public class Adviser implements Serializable {

	private static final long serialVersionUID = 472939024728423L;

	private Integer orderNumber;

	@Id
	private String nif;

	private String lastName;

	private String name;

	private Integer ropoNumber;

	private boolean fungusInstAdvice;

	private List<AdvisoryCrop> crops;

	/* Getters and Setters */

	public Integer getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getNif() {
		return nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getRopoNumber() {
		return ropoNumber;
	}

	public void setRopoNumber(Integer ropoNumber) {
		this.ropoNumber = ropoNumber;
	}

	public boolean getFungusInstAdvice() {
		return fungusInstAdvice;
	}

	public void setFungusInstAdvice(boolean fungusInstAdvice) {
		this.fungusInstAdvice = fungusInstAdvice;
	}

	public List<AdvisoryCrop> getCrops() {
		return crops;
	}

	public void setCrops(List<AdvisoryCrop> crops) {
		this.crops = crops;
	}

}
