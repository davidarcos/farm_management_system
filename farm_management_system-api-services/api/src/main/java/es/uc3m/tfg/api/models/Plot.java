package es.uc3m.tfg.api.models;

import java.io.Serializable;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Plots")
public class Plot implements Serializable {

	private static final long serialVersionUID = 472939024728423L;

	/* Agricultural Plot */

	@Id
	private Integer orderNumber;

	private String category;

	private String cropType;

	private Integer cropTypeNumber;

	private String crop;

	private Integer cropNumber;

	private String variety;

	private Integer varietyNumber;

	private String varietyComments;

	/* Herbaceous crops and others */

	private String isGenModified;

	/* Woody crops */

	private String varietyType;

	private String mainPattern;

	private String sizeX;

	private String sizeY;

	private Integer year;

	/* SigPac reference */

	private String province;

	private Integer provinceNumber;

	private String city;

	private Integer cityNumber;

	private String addition;

	private String zone;

	private String polygon;

	private String plot;
	
	private String enclosure;

	private String cultivatedArea;

	private String area;

	private String sigpacUse;

	private String slope;

	/* Environmental data */

	private String isVulnerableArea;

	private String isRedNatura;

	private String isSecurityDistance;

	private String securityDistance;

	private String waterExtPointsType;

	private String measures;

	/* Agronomic data */

	private String exploitationType;

	private String irrigationSystem;

	private String protectionType;

	private String comments;

	/* Other data */

	private String certificationType;

	private String gipAdviceSystem;

	
	/* Getters and Setters */
	
	public Integer getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCropType() {
		return cropType;
	}

	public void setCropType(String cropType) {
		this.cropType = cropType;
	}

	public Integer getCropTypeNumber() {
		return cropTypeNumber;
	}

	public void setCropTypeNumber(Integer cropTypeNumber) {
		this.cropTypeNumber = cropTypeNumber;
	}

	public String getCrop() {
		return crop;
	}

	public void setCrop(String crop) {
		this.crop = crop;
	}

	public Integer getCropNumber() {
		return cropNumber;
	}

	public void setCropNumber(Integer cropNumber) {
		this.cropNumber = cropNumber;
	}

	public String getVariety() {
		return variety;
	}

	public void setVariety(String variety) {
		this.variety = variety;
	}

	public Integer getVarietyNumber() {
		return varietyNumber;
	}

	public void setVarietyNumber(Integer varietyNumber) {
		this.varietyNumber = varietyNumber;
	}

	public String getVarietyComments() {
		return varietyComments;
	}

	public void setVarietyComments(String varietyComments) {
		this.varietyComments = varietyComments;
	}

	public String getIsGenModified() {
		return isGenModified;
	}

	public void setIsGenModified(String isGenModified) {
		this.isGenModified = isGenModified;
	}

	public String getVarietyType() {
		return varietyType;
	}

	public void setVarietyType(String varietyType) {
		this.varietyType = varietyType;
	}

	public String getMainPattern() {
		return mainPattern;
	}

	public void setMainPattern(String mainPattern) {
		this.mainPattern = mainPattern;
	}

	public String getSizeX() {
		return sizeX;
	}

	public void setSizeX(String sizeX) {
		this.sizeX = sizeX;
	}

	public String getSizeY() {
		return sizeY;
	}

	public void setSizeY(String sizeY) {
		this.sizeY = sizeY;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public Integer getProvinceNumber() {
		return provinceNumber;
	}

	public void setProvinceNumber(Integer provinceNumber) {
		this.provinceNumber = provinceNumber;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Integer getCityNumber() {
		return cityNumber;
	}

	public void setCityNumber(Integer cityNumber) {
		this.cityNumber = cityNumber;
	}

	public String getAddition() {
		return addition;
	}

	public void setAddition(String addition) {
		this.addition = addition;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getPolygon() {
		return polygon;
	}

	public void setPolygon(String polygon) {
		this.polygon = polygon;
	}

	public String getPlot() {
		return plot;
	}

	public void setPlot(String plot) {
		this.plot = plot;
	}

	public String getEnclosure() {
		return enclosure;
	}

	public void setEnclosure(String enclosure) {
		this.enclosure = enclosure;
	}

	public String getCultivatedArea() {
		return cultivatedArea;
	}

	public void setCultivatedArea(String cultivatedArea) {
		this.cultivatedArea = cultivatedArea;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getSigpacUse() {
		return sigpacUse;
	}

	public void setSigpacUse(String sigpacUse) {
		this.sigpacUse = sigpacUse;
	}

	public String getSlope() {
		return slope;
	}

	public void setSlope(String slope) {
		this.slope = slope;
	}

	public String getIsVulnerableArea() {
		return isVulnerableArea;
	}

	public void setIsVulnerableArea(String isVulnerableArea) {
		this.isVulnerableArea = isVulnerableArea;
	}

	public String getIsRedNatura() {
		return isRedNatura;
	}

	public void setIsRedNatura(String isRedNatura) {
		this.isRedNatura = isRedNatura;
	}

	public String getIsSecurityDistance() {
		return isSecurityDistance;
	}

	public void setIsSecurityDistance(String isSecurityDistance) {
		this.isSecurityDistance = isSecurityDistance;
	}

	public String getSecurityDistance() {
		return securityDistance;
	}

	public void setSecurityDistance(String securityDistance) {
		this.securityDistance = securityDistance;
	}

	public String getWaterExtPointsType() {
		return waterExtPointsType;
	}

	public void setWaterExtPointsType(String waterExtPointsType) {
		this.waterExtPointsType = waterExtPointsType;
	}

	public String getMeasures() {
		return measures;
	}

	public void setMeasures(String measures) {
		this.measures = measures;
	}

	public String getExploitationType() {
		return exploitationType;
	}

	public void setExploitationType(String exploitationType) {
		this.exploitationType = exploitationType;
	}

	public String getIrrigationSystem() {
		return irrigationSystem;
	}

	public void setIrrigationSystem(String irrigationSystem) {
		this.irrigationSystem = irrigationSystem;
	}

	public String getProtectionType() {
		return protectionType;
	}

	public void setProtectionType(String protectionType) {
		this.protectionType = protectionType;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getCertificationType() {
		return certificationType;
	}

	public void setCertificationType(String certificationType) {
		this.certificationType = certificationType;
	}

	public String getGipAdviceSystem() {
		return gipAdviceSystem;
	}

	public void setGipAdviceSystem(String gipAdviceSystem) {
		this.gipAdviceSystem = gipAdviceSystem;
	}

	
}
