package es.uc3m.tfg.api.controllers;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import es.uc3m.tfg.api.models.Plot;
import es.uc3m.tfg.api.repositories.IPlotDAO;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.POST, RequestMethod.GET, RequestMethod.PUT })
@RequestMapping("/plots")
public class PlotController {

	@Autowired
	private IPlotDAO iPlotDAO;

	@GetMapping("/")
	public List<Plot> findAll() {
		return iPlotDAO.findAll();
	}

	@GetMapping("/categories/{category}")
	public List<Plot> findAllByCategory(@PathVariable String category) {
		return iPlotDAO.findAllByCategory(category);
	}

	@GetMapping("/{id}")
	public Optional<Plot> findById(@PathVariable Integer id) {
		return iPlotDAO.findById(id);
	}

	@PostMapping("/")
	public Plot insert(@Validated @RequestBody Plot plot) {
		return iPlotDAO.insert(plot);
	}

	@PutMapping("/{id}")
	public Plot update(@PathVariable Integer id, @Validated @RequestBody Plot plot) {
		return iPlotDAO.save(plot);
	}

}
