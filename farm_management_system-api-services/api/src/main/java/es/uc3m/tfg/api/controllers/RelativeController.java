package es.uc3m.tfg.api.controllers;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import es.uc3m.tfg.api.models.Relative;
import es.uc3m.tfg.api.repositories.IRelativeDAO;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.POST, RequestMethod.GET, RequestMethod.PUT,
		RequestMethod.DELETE })
@RequestMapping("/generalData/relatives")
public class RelativeController {

	@Autowired
	private IRelativeDAO iRelativeDAO;

	@GetMapping("/")
	public List<Relative> findAll() {
		return iRelativeDAO.findAll();
	}

	@GetMapping("/{id}")
	public Optional<Relative> findById(@PathVariable String id) {
		return iRelativeDAO.findById(id);
	}

	@PostMapping("/")
	public Relative insert(@Validated @RequestBody Relative relative) {
		return iRelativeDAO.insert(relative);
	}

	@PutMapping("/{id}")
	public Relative update(@PathVariable String id, @Validated @RequestBody Relative relative) {
		return iRelativeDAO.save(relative);
	}

	@DeleteMapping("/")
	public void deleteAll() {
		iRelativeDAO.deleteAll();
	}

	@DeleteMapping("/{id}")
	public void deleteById(@PathVariable String id) {
		iRelativeDAO.deleteById(id);
	}

}
