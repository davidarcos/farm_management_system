package es.uc3m.tfg.api.controllers;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import es.uc3m.tfg.api.models.Sapling;
import es.uc3m.tfg.api.repositories.ISaplingDAO;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.POST, RequestMethod.GET, RequestMethod.PUT,
		RequestMethod.DELETE })
@RequestMapping("/purchases/saplings")
public class SaplingController {

	@Autowired
	private ISaplingDAO iSaplingDAO;

	@GetMapping("/")
	public List<Sapling> findAll() {
		return iSaplingDAO.findAll();
	}

	@GetMapping("/{id}")
	public Optional<Sapling> findById(@PathVariable String id) {
		return iSaplingDAO.findById(id);
	}

	@PostMapping("/")
	public Sapling insert(@Validated @RequestBody Sapling sapling) {
		return iSaplingDAO.insert(sapling);
	}

	@PutMapping("/{id}")
	public Sapling update(@PathVariable String id, @Validated @RequestBody Sapling sapling) {
		return iSaplingDAO.save(sapling);
	}

	@DeleteMapping("/")
	public void deleteAll() {
		iSaplingDAO.deleteAll();
	}

	@DeleteMapping("/{id}")
	public void deleteById(@PathVariable String id) {
		iSaplingDAO.deleteById(id);
	}

}
