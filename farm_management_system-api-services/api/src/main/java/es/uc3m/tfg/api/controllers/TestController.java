package es.uc3m.tfg.api.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import es.uc3m.tfg.api.models.Tester;
import es.uc3m.tfg.api.repositories.IAdviserDAO;
import es.uc3m.tfg.api.repositories.IAdvisoryCropDAO;
import es.uc3m.tfg.api.repositories.IFertilizerDAO;
import es.uc3m.tfg.api.repositories.IMachineryDAO;
import es.uc3m.tfg.api.repositories.IManagerDAO;
import es.uc3m.tfg.api.repositories.IMarketedHarvestDAO;
import es.uc3m.tfg.api.repositories.IOtherTreatmentDAO;
import es.uc3m.tfg.api.repositories.IOutsiderDAO;
import es.uc3m.tfg.api.repositories.IOwnerDAO;
import es.uc3m.tfg.api.repositories.IPhytosanitaryProductDAO;
import es.uc3m.tfg.api.repositories.IPhytosanitaryTreatmentDAO;
import es.uc3m.tfg.api.repositories.IPlotDAO;
import es.uc3m.tfg.api.repositories.IRelativeDAO;
import es.uc3m.tfg.api.repositories.ISaplingDAO;
import es.uc3m.tfg.api.repositories.ISeedPlantDAO;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.POST, RequestMethod.DELETE })
@RequestMapping("/testing")
public class TestController {

	@Autowired
	private IOwnerDAO iOwnerDAO;
	@Autowired
	private IManagerDAO iManagerDAO;
	@Autowired
	private IAdvisoryCropDAO iAdvisoryCropDAO;
	@Autowired
	private IAdviserDAO iAdviserDAO;
	@Autowired
	private IRelativeDAO iRelativeDAO;
	@Autowired
	private IOutsiderDAO iOutsiderDAO;
	@Autowired
	private IMachineryDAO iMachineryDAO;
	@Autowired
	private IPlotDAO iPlotDAO;
	@Autowired
	private IPhytosanitaryTreatmentDAO iPhytosanitaryTreatmentDAO;
	@Autowired
	private IOtherTreatmentDAO iOtherTreatmentDAO;
	@Autowired
	private ISeedPlantDAO iSeedPlantDAO;
	@Autowired
	private ISaplingDAO iSaplingDAO;
	@Autowired
	private IFertilizerDAO iFertilizerDAO;
	@Autowired
	private IPhytosanitaryProductDAO iPhytosanitaryProductDAO;
	@Autowired
	private IMarketedHarvestDAO iMarketedHarvestDAO;

	@PostMapping("/")
	public void insertAll(@Validated @RequestBody Tester tester) {

		iOwnerDAO.insert(tester.getOwners());
		iManagerDAO.insert(tester.getManagers());
		iAdvisoryCropDAO.insert(tester.getAdvisoryCrops());
		iAdviserDAO.insert(tester.getAdvisers());
		iRelativeDAO.insert(tester.getRelatives());
		iOutsiderDAO.insert(tester.getOutsiders());
		iMachineryDAO.insert(tester.getMachineries());
		iPlotDAO.insert(tester.getPlots());
		iPhytosanitaryTreatmentDAO.insert(tester.getPhytosanitaryTreatments());
		iOtherTreatmentDAO.insert(tester.getOtherTreatments());
		iSeedPlantDAO.insert(tester.getSeed_plants());
		iSaplingDAO.insert(tester.getSaplings());
		iFertilizerDAO.insert(tester.getFertilizers());
		iPhytosanitaryProductDAO.insert(tester.getPhytosanitaryProducts());
		iMarketedHarvestDAO.insert(tester.getMarketedHarvests());
	}

	@DeleteMapping("/")
	public void deleteAll() {
		iOwnerDAO.deleteAll();
		iManagerDAO.deleteAll();
		iAdvisoryCropDAO.deleteAll();
		iAdviserDAO.deleteAll();
		iRelativeDAO.deleteAll();
		iOutsiderDAO.deleteAll();
		iMachineryDAO.deleteAll();
		iPlotDAO.deleteAll();
		iPhytosanitaryTreatmentDAO.deleteAll();
		iOtherTreatmentDAO.deleteAll();
		iSeedPlantDAO.deleteAll();
		iSaplingDAO.deleteAll();
		iFertilizerDAO.deleteAll();
		iPhytosanitaryProductDAO.deleteAll();
		iMarketedHarvestDAO.deleteAll();
	}

}
