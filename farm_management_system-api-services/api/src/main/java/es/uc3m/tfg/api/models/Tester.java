package es.uc3m.tfg.api.models;

import java.util.List;

public class Tester {

	List<Owner> owners;
	List<Manager> managers;
	List<AdvisoryCrop> advisoryCrops;
	List<Adviser> advisers;
	List<Relative> relatives;
	List<Outsider> outsiders;
	List<Machinery> machineries;
	List<Plot> plots;
	List<PhytosanitaryTreatment> phytosanitaryTreatments;
	List<OtherTreatment> otherTreatments;
	List<SeedPlant> seed_plants;
	List<Sapling> saplings;
	List<Fertilizer> fertilizers;
	List<PhytosanitaryProduct> phytosanitaryProducts;
	List<MarketedHarvest> marketedHarvests;
	
	
	/* Getters and Setters */
	
	public List<Owner> getOwners() {
		return owners;
	}
	public void setOwners(List<Owner> owners) {
		this.owners = owners;
	}
	public List<Manager> getManagers() {
		return managers;
	}
	public void setManagers(List<Manager> managers) {
		this.managers = managers;
	}
	public List<AdvisoryCrop> getAdvisoryCrops() {
		return advisoryCrops;
	}
	public void setAdvisoryCrops(List<AdvisoryCrop> advisoryCrops) {
		this.advisoryCrops = advisoryCrops;
	}
	public List<Adviser> getAdvisers() {
		return advisers;
	}
	public void setAdvisers(List<Adviser> advisers) {
		this.advisers = advisers;
	}
	public List<Relative> getRelatives() {
		return relatives;
	}
	public void setRelatives(List<Relative> relatives) {
		this.relatives = relatives;
	}
	public List<Outsider> getOutsiders() {
		return outsiders;
	}
	public void setOutsiders(List<Outsider> outsiders) {
		this.outsiders = outsiders;
	}
	public List<Machinery> getMachineries() {
		return machineries;
	}
	public void setMachineries(List<Machinery> machineries) {
		this.machineries = machineries;
	}
	public List<Plot> getPlots() {
		return plots;
	}
	public void setPlots(List<Plot> plots) {
		this.plots = plots;
	}
	public List<PhytosanitaryTreatment> getPhytosanitaryTreatments() {
		return phytosanitaryTreatments;
	}
	public void setPhytosanitaryTreatments(List<PhytosanitaryTreatment> phytosanitaryTreatments) {
		this.phytosanitaryTreatments = phytosanitaryTreatments;
	}
	public List<OtherTreatment> getOtherTreatments() {
		return otherTreatments;
	}
	public void setOtherTreatments(List<OtherTreatment> otherTreatments) {
		this.otherTreatments = otherTreatments;
	}
	public List<SeedPlant> getSeed_plants() {
		return seed_plants;
	}
	public void setSeed_plants(List<SeedPlant> seed_plants) {
		this.seed_plants = seed_plants;
	}
	public List<Sapling> getSaplings() {
		return saplings;
	}
	public void setSaplings(List<Sapling> saplings) {
		this.saplings = saplings;
	}
	public List<Fertilizer> getFertilizers() {
		return fertilizers;
	}
	public void setFertilizers(List<Fertilizer> fertilizers) {
		this.fertilizers = fertilizers;
	}
	public List<PhytosanitaryProduct> getPhytosanitaryProducts() {
		return phytosanitaryProducts;
	}
	public void setPhytosanitaryProducts(List<PhytosanitaryProduct> phytosanitaryProducts) {
		this.phytosanitaryProducts = phytosanitaryProducts;
	}
	public List<MarketedHarvest> getMarketedHarvests() {
		return marketedHarvests;
	}
	public void setMarketedHarvests(List<MarketedHarvest> marketedHarvests) {
		this.marketedHarvests = marketedHarvests;
	}
	

}
