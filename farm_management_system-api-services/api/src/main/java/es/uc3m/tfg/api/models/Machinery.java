package es.uc3m.tfg.api.models;

import java.io.Serializable;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Machinery")
public class Machinery implements Serializable {

	private static final long serialVersionUID = 472939024728423L;

	@Id
	private Integer id;

	private String category;

	private String type;

	private String brand;

	private String model;

	private Integer romaNumber;

	private String romaDate1;

	private String romaDate;

	private String lastInscDate;

	private String resultInsc;

	/* Getters and Setters */

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public Integer getRomaNumber() {
		return romaNumber;
	}

	public void setRomaNumber(Integer romaNumber) {
		this.romaNumber = romaNumber;
	}

	public String getRomaDate1() {
		return romaDate1;
	}

	public void setRomaDate1(String romaDate1) {
		this.romaDate1 = romaDate1;
	}

	public String getRomaDate() {
		return romaDate;
	}

	public void setRomaDate(String romaDate) {
		this.romaDate = romaDate;
	}

	public String getLastInscDate() {
		return lastInscDate;
	}

	public void setLastInscDate(String lastInscDate) {
		this.lastInscDate = lastInscDate;
	}

	public String getResultInsc() {
		return resultInsc;
	}

	public void setResultInsc(String resultInsc) {
		this.resultInsc = resultInsc;
	}

}