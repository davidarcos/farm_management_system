package es.uc3m.tfg.api.controllers;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import es.uc3m.tfg.api.models.Manager;
import es.uc3m.tfg.api.repositories.IManagerDAO;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.POST, RequestMethod.GET, RequestMethod.PUT,
		RequestMethod.DELETE })
@RequestMapping("/generalData/owners/managers")
public class ManagerController {

	@Autowired
	private IManagerDAO iManagerDAO;

	@GetMapping("/")
	public List<Manager> findAll() {
		return iManagerDAO.findAll();
	}

	@GetMapping("/{id}")
	public Optional<Manager> findById(@PathVariable Integer id) {
		return iManagerDAO.findById(id);
	}

	@PostMapping("/")
	public Manager insert(@Validated @RequestBody Manager manager) {
		return iManagerDAO.insert(manager);
	}

	@PutMapping("/{id}")
	public Manager update(@PathVariable Integer id, @Validated @RequestBody Manager manager) {
		return iManagerDAO.save(manager);
	}

	@DeleteMapping("/")
	public void deleteAll() {
		iManagerDAO.deleteAll();
	}

	@DeleteMapping("/{id}")
	public void deleteById(@PathVariable Integer id) {
		iManagerDAO.deleteById(id);
	}

}
