package es.uc3m.tfg.api.controllers;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import es.uc3m.tfg.api.models.Outsider;
import es.uc3m.tfg.api.repositories.IOutsiderDAO;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.POST, RequestMethod.GET, RequestMethod.PUT,
		RequestMethod.DELETE })
@RequestMapping("/generalData/outsiders")
public class OutsiderController {

	@Autowired
	private IOutsiderDAO iOutsiderDAO;

	@GetMapping("/")
	public List<Outsider> findAll() {
		return iOutsiderDAO.findAll();
	}

	@GetMapping("/{id}")
	public Optional<Outsider> findById(@PathVariable String id) {
		return iOutsiderDAO.findById(id);
	}

	@PostMapping("/")
	public Outsider insert(@Validated @RequestBody Outsider outsider) {
		return iOutsiderDAO.insert(outsider);
	}

	@PutMapping("/{id}")
	public Outsider update(@PathVariable String id, @Validated @RequestBody Outsider outsider) {
		return iOutsiderDAO.save(outsider);
	}

	@DeleteMapping("/")
	public void deleteAll() {
		iOutsiderDAO.deleteAll();
	}

	@DeleteMapping("/{id}")
	public void deleteById(@PathVariable String id) {
		iOutsiderDAO.deleteById(id);
	}

}
