package es.uc3m.tfg.api.repositories;

import java.io.Serializable;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import es.uc3m.tfg.api.models.Plot;

@Repository
public interface IPlotDAO extends MongoRepository<Plot, Serializable> {

	List<Plot> findAllByCategory(@Param("category") String category);
}
