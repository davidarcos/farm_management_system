package es.uc3m.tfg.api.models;

import java.io.Serializable;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Relatives")
public class Relative implements Serializable {

	private static final long serialVersionUID = 472939024728423L;

	private Integer orderNumber;

	@Id
	private String nif;

	private String lastName;

	private String name;

	private String carnetType;

	private Integer ropoNumber;

	private String relationship;


	/* Getters and Setters */

	public Integer getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getNif() {
		return nif.toUpperCase();
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getRopoNumber() {
		return ropoNumber;
	}

	public void setRopoNumber(Integer ropoNumber) {
		this.ropoNumber = ropoNumber;
	}

	public String getCarnetType() {
		return carnetType;
	}

	public void setCarnetType(String carnetType) {
		this.carnetType = carnetType;
	}

	public String getRelationship() {
		return relationship;
	}

	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}

}
