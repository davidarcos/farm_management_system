package es.uc3m.tfg.api.controllers;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import es.uc3m.tfg.api.models.PhytosanitaryTreatment;
import es.uc3m.tfg.api.repositories.IPhytosanitaryTreatmentDAO;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.POST, RequestMethod.GET, RequestMethod.PUT,
		RequestMethod.DELETE })
@RequestMapping("/treatments/phytosanitaries")
public class PhytosanitaryTreatmentController {

	@Autowired
	private IPhytosanitaryTreatmentDAO iPhytosanitaryTreatmentDAO;

	@GetMapping("/")
	public List<PhytosanitaryTreatment> findAll() {
		return iPhytosanitaryTreatmentDAO.findAll();
	}

	@GetMapping("/{id}")
	public Optional<PhytosanitaryTreatment> findById(@PathVariable String id) {
		return iPhytosanitaryTreatmentDAO.findById(id);
	}

	@PostMapping("/")
	public PhytosanitaryTreatment insert(@Validated @RequestBody PhytosanitaryTreatment treatment) {
		return iPhytosanitaryTreatmentDAO.insert(treatment);
	}

	@PutMapping("/{id}")
	public PhytosanitaryTreatment update(@PathVariable String id,
			@Validated @RequestBody PhytosanitaryTreatment treatment) {
		return iPhytosanitaryTreatmentDAO.save(treatment);
	}

	@DeleteMapping("/")
	public void deleteAll() {
		iPhytosanitaryTreatmentDAO.deleteAll();
	}

	@DeleteMapping("/{id}")
	public void deleteById(@PathVariable String id) {
		iPhytosanitaryTreatmentDAO.deleteById(id);
	}

}
