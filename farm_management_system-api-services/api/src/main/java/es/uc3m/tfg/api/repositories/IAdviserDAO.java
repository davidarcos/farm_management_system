package es.uc3m.tfg.api.repositories;

import java.io.Serializable;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import es.uc3m.tfg.api.models.Adviser;

@Repository
public interface IAdviserDAO extends MongoRepository<Adviser, Serializable> {
	
	List<Adviser> findByNif(@Param("nif") String nif);

}
