package es.uc3m.tfg.api.repositories;

import java.io.Serializable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import es.uc3m.tfg.api.models.MarketedHarvest;

@Repository
public interface IMarketedHarvestDAO extends MongoRepository<MarketedHarvest, Serializable> {

}
